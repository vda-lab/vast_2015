library(dplyr)
library(ggplot2)
library(lubridate)
library(grid)
library(scales)
library(RColorBrewer)
library(tidyr)
## nice colors  = "aquamarine", "cornflowerblue", "coral", "darkcyan", "brown1", "darkolivegreen1", "darkorchid","deeppink", "gold", "royalblue", "seagreen1"
library(dendsort)
library(vegan)

#GC1
#at the stage sequence view ----
#friday
load("move_fri.RData")
subset_friday <- friday %>%
  filter(from_x == 76 & from_y == 22) %>%
  filter(speed < 0.01)

unique_id <- subset_friday %>%
  group_by(id) %>%
  summarise(arrival = min(from_t)) %>%
  arrange(arrival) %>%            
  select(id) %>%
  t() %>%
  as.vector() %>%
  as.character()
#index individual
subset_friday <- subset_friday %>%
  mutate(index = match(id, unique_id))
#plot
ggplot(subset_friday, aes(x=from_t,y=index)) +
  geom_segment(aes(xend=to_t, yend=index, color = type )) +
  theme(legend.position="bottom", axis.title.x=element_blank()) +
  ggtitle("Movement pattern at the Grinosaurus Stage on Friday")

#annotation
annotation_fri <- subset_friday %>%
  filter(duration >  60*60*2) %>%
  group_by(id) %>%
  summarise(from_t = first(from_t),
            to_t = last(to_t),
            duration = sum(duration),
            type = last(type),
            index = first(index)) %>%
  ungroup() %>%
  group_by(type) %>%
  summarise(last_t = last(to_t),
            index = first(index),
            count = n(),
            duration = mean(duration)) %>%
  ungroup() %>%
  mutate(minutes = duration/60) %>%
  mutate(hours = minutes/60) %>%
  mutate(label = paste(count, " invividuals ", round(hours, digits= 1), " hrs", sep = ""))
  
ggplot(subset_friday, aes(x=from_t,y=index)) +
  geom_segment(aes(xend=to_t, yend=index, color = type )) +
  theme(legend.position="bottom", axis.title.x=element_blank()) +
  ggtitle("Movement pattern at the Grinosaurus Stage on Friday")+
  geom_text(data = annotation_fri, aes(x=last_t, y = index, label= label), vjust = 1, hjust=1, cex= 4)+
  coord_cartesian(xlim = c(ymd_hm("2014-06-06 08:00"), ymd_hm("2014-06-06 18:00"))) +
  scale_x_datetime(breaks = seq(as.POSIXct(strptime("2014-06-06 08:00:00", "%Y-%m-%d %H:%M:%S")), as.POSIXct(strptime("2014-06-06 19:00:00", "%Y-%m-%d %H:%M:%S")), by = 60*60),
                   labels = date_format("%H:%M"))


#Saturday
load("move_sat.RData")
subset_sat <- saturday %>%
  filter(from_x == 76 & from_y == 22) %>%
  filter(speed < 0.01)
unique_id_sat <- subset_sat %>%
  group_by(id) %>%
  summarise(arrival = min(from_t)) %>%
  arrange(arrival) %>%            
  select(id) %>%
  t() %>%
  as.vector() %>%
  as.character()
#index individual
subset_sat <- subset_sat %>%
  mutate(index = match(id, unique_id_sat))
#plot
ggplot(subset_sat, aes(x=from_t,y=index)) +
  geom_segment(aes(xend=to_t, yend=index, color = type )) +
  theme(legend.position="bottom", axis.title.x=element_blank()) +
  ggtitle("Movement pattern at the Grinosaurus Stage on Saturday")

#annotation
annotation_sat <- subset_sat %>%
  group_by(id) %>%
  summarise(from_t = first(from_t),
            to_t = last(to_t),
            duration = sum(duration),
            type = last(type),
            index = first(index)) %>%
  ungroup() %>%
  filter(duration >  60*60*1.5) %>%
  group_by(type) %>%
  summarise(last_t = last(to_t),
            index = first(index),
            count = n(),
            duration = mean(duration)) %>%
  ungroup() %>%
  mutate(minutes = duration/60) %>%
  mutate(hours = minutes/60) %>%
  mutate(label = paste(count, " invividuals ", round(hours, digits= 1), " hrs", sep = ""))

ggplot(subset_sat, aes(x=from_t,y=index)) +
  geom_segment(aes(xend=to_t, yend=index, color = type )) +
  theme(legend.position="bottom", axis.title.x=element_blank()) +
  ggtitle("Movement pattern at the Grinosaurus Stage on Saturday")+
  geom_text(data = annotation_sat, aes(x=last_t, y = index, label= label), vjust = 1, hjust=1, cex= 4)+
  coord_cartesian(xlim = c(ymd_hm("2014-06-07 08:00"), ymd_hm("2014-06-07 18:00"))) +
  scale_x_datetime(breaks = seq(as.POSIXct(strptime("2014-06-07 08:00:00", "%Y-%m-%d %H:%M:%S")), as.POSIXct(strptime("2014-06-07 19:00:00", "%Y-%m-%d %H:%M:%S")), by = 60*60),
                   labels = date_format("%H:%M"))

#Sunday
load("move_sun.RData")
subset_sun <- sunday %>%
  filter(from_x == 76 & from_y == 22) %>%
  filter(speed < 0.01)
unique_id_sun <- subset_sun %>%
  group_by(id) %>%
  summarise(arrival = min(from_t)) %>%
  arrange(arrival) %>%            
  select(id) %>%
  t() %>%
  as.vector() %>%
  as.character()
#index individual
subset_sun <- subset_sun %>%
  mutate(index = match(id, unique_id_sun))
#plot
ggplot(subset_sun, aes(x=from_t,y=index)) +
  geom_segment(aes(xend=to_t, yend=index, color = type )) +
  theme(legend.position="bottom", axis.title.x=element_blank()) +
  ggtitle("Movement pattern at the Creighton Pavilion on Sunday")

#annotation
annotation_sun <- subset_sun %>%
  group_by(id) %>%
  summarise(from_t = first(from_t),
            to_t = last(to_t),
            duration = sum(duration),
            type = last(type),
            index = first(index)) %>%
  ungroup() %>%
  filter(duration >  60*60*1.5) %>%
  group_by(type) %>%
  summarise(last_t = last(to_t),
            index = first(index),
            count = n(),
            duration = mean(duration)) %>%
  ungroup() %>%
  mutate(minutes = duration/60) %>%
  mutate(hours = minutes/60) %>%
  mutate(label = paste(count, " invividuals ", round(hours, digits= 1), " hrs", sep = ""))

ggplot(subset_sun, aes(x=from_t,y=index)) +
  geom_segment(aes(xend=to_t, yend=index, color = type )) +
  theme(legend.position="bottom", axis.title.x=element_blank()) +
  ggtitle("Movement pattern at the Grinosaurus Stage on Sunday")+
  geom_text(data = annotation_sun, aes(x=last_t, y = index, label= label), vjust = 1, hjust=1, cex= 4)+
  coord_cartesian(xlim = c(ymd_hm("2014-06-08 08:00"), ymd_hm("2014-06-08 18:00"))) +
  scale_x_datetime(breaks = seq(as.POSIXct(strptime("2014-06-08 08:00:00", "%Y-%m-%d %H:%M:%S")), as.POSIXct(strptime("2014-06-08 19:00:00", "%Y-%m-%d %H:%M:%S")), by = 60*60),
                   labels = date_format("%H:%M"))
# subset individuals ----
fri_8 <- subset_friday %>%
  group_by(id) %>%
  summarise(from_t = first(from_t),
            to_t = last(to_t),
            duration = sum(duration),
            type = last(type),
            index = first(index)) %>%
  ungroup() %>%
  filter(duration >  60*60*1.5) %>%
  filter(type == "movement")

sat_8 <- subset_sat %>%
  group_by(id) %>%
  summarise(from_t = first(from_t),
            to_t = last(to_t),
            duration = sum(duration),
            type = last(type),
            index = first(index)) %>%
  ungroup() %>%
  filter(duration >  60*60*1.5) %>%
  filter(type == "movement")

sun_8 <- subset_sun %>%
  group_by(id) %>%
  summarise(from_t = first(from_t),
            to_t = last(to_t),
            duration = sum(duration),
            type = last(type),
            index = first(index)) %>%
  ungroup() %>%
  filter(duration >  60*60*1.5) %>%
  filter(type == "movement")

# fri_8$id == sat_8$id
# sun_8$id == sat_8$id
# fri_8$id == sun_8$id

#movement
load("gps_fri_2.RData")
load("gps_sat.RData")
load("gps_sun.RData")

subset_fri <- fri %>%
  filter(id %in% fri_8$id) %>%
  mutate(day = "friday") %>%
  mutate(hour_f = (hour(timestamp)*60 + minute(timestamp))/60) %>%
  mutate(hour = hour(timestamp))
subset_sat <- sat %>%
  filter(id %in% fri_8$id) %>%
  mutate(day = "saturday")%>%
  mutate(hour_f = (hour(timestamp)*60 + minute(timestamp))/60) %>%
  mutate(hour = hour(timestamp))
subset_sun <- sun %>%
  filter(id %in% fri_8$id) %>%
  mutate(day = "sunday")%>%
  mutate(hour_f = (hour(timestamp)*60 + minute(timestamp))/60) %>%
  mutate(hour = hour(timestamp))
subset <- bind_rows(subset_fri, subset_sat, subset_sun) %>%
  arrange(timestamp)

load("map_pos.RData")
map = ggplot(all_pos)+ geom_rect(aes(xmin=X, ymin=Y, xmax=X+1, ymax=Y+1), fill="white") + coord_fixed(xlim = c(-2, 102), ylim = c(-2, 102)) 
map

ggplot(data = subset, aes(x=X, y=Y)) +
  geom_path(position = position_jitter(w = 0.5, h=0.5), color="black", alpha= 0.5) +
  coord_fixed(xlim = c(0, 110), ylim = c(0, 110)) + 
  geom_point(data = subset %>% filter(type=="check-in"), aes(x=X, y=Y), size= 3, color="cornflowerblue")+
  annotate("point", x=76, y=22, shape=1, size=4, color="gray20")+
  annotate("text", x=76, y=22, label="Grinosaurus Stage", hjust=0, vjust=2,cex=3, color="gray20")+
  annotate("point", x=99, y=77, shape=1, size=4, color="gray20")+
  annotate("text", x=99, y=77, label="East Entrance", hjust=0, vjust=0, cex=3, color="gray20")+
  facet_grid(day~hour)

map +
  geom_path(data = subset, aes(x=X, y=Y), position = position_jitter(w = 0.5, h=0.5), color="black", alpha= 0.5) +
  coord_fixed(xlim = c(0, 110), ylim = c(0, 110)) + 
  geom_point(data = subset %>% filter(type=="check-in"), aes(x=X, y=Y), size= 3, color="cornflowerblue")+
  annotate("point", x=76, y=22, shape=1, size=4, color="gray20")+
  annotate("text", x=76, y=22, label="Grinosaurus Stage", hjust=0, vjust=2,cex=3, color="gray20")+
  annotate("point", x=99, y=77, shape=1, size=4, color="gray20")+
  annotate("text", x=99, y=77, label="East Entrance", hjust=0, vjust=0, cex=3, color="gray20")+
  facet_grid(day~hour)
# compare movement pattern of subset 2 and 3 ----

fri_check<- subset_friday %>%
  group_by(id) %>%
  summarise(from_t = first(from_t),
            to_t = last(to_t),
            duration = sum(duration),
            type = last(type),
            index = first(index)) %>%
  ungroup() %>%
  filter(duration >  60*60*1.5) %>%
  filter(type == "checkin")

sat_check <- subset_sat %>%
  group_by(id) %>%
  summarise(from_t = first(from_t),
            to_t = last(to_t),
            duration = sum(duration),
            type = last(type),
            index = first(index)) %>%
  ungroup() %>%
  filter(duration >  60*60*1.5) %>%
  filter(type == "checkin")

dput(as.character(fri_8$id))
sub1_ids = c("521750", "644885", "1080969", "1600469", "1629516", "1781070", "1787551", "1935406")
dput(as.character(fri_check$id))
sub2_ids = c("629048", "1486047", "1690685", "1797150")
dput(as.character(sat_check$id))
sub3_ids = c("631146", "802410", "1856633")

#subset 2
sub2_fri <- fri %>%
  filter(id %in% sub2_ids) %>%
  mutate(subgroup = 2) %>%
  mutate(day = "friday") %>%
  mutate(hour_f = (hour(timestamp)*60 + minute(timestamp))/60) %>%
  mutate(hour = hour(timestamp))
sub2_sat <- sat %>%
  filter(id %in% sub2_ids) %>%
  mutate(subgroup = 2) %>%
  mutate(day = "saturday") %>%
  mutate(hour_f = (hour(timestamp)*60 + minute(timestamp))/60) %>%
  mutate(hour = hour(timestamp))
sub2_sun <- sun %>%
  filter(id %in% sub2_ids) %>%
  mutate(subgroup = 2) %>%
  mutate(day = "sunday") %>%
  mutate(hour_f = (hour(timestamp)*60 + minute(timestamp))/60) %>%
  mutate(hour = hour(timestamp))
sub2 <- bind_rows(sub2_fri, sub2_sat, sub2_sun)
#plot
load("map_pos.RData")
map = ggplot(all_pos)+ geom_rect(aes(xmin=X, ymin=Y, xmax=X+1, ymax=Y+1), fill="white") + coord_fixed(xlim = c(-2, 102), ylim = c(-2, 102)) 
map
map + 
  geom_path(data = sub2, aes(x=X, y=Y, color=hour_f), position = position_jitter(w = 0.5, h=0.5)) +
  scale_color_gradientn(colours = brewer.pal(11, "Spectral"), space="rgb") +
  coord_fixed(xlim = c(0, 110), ylim = c(0, 110)) + 
  geom_point(data = sub2 %>% filter(type=="check-in"), aes(x=X, y=Y, color=hour_f), size= 3)+
  annotate("point", x=76, y=22, shape=1, size=4, color="gray20")+
  annotate("text", x=76, y=22, label="Stage", hjust=0, vjust=2,cex=3, color="gray20")+
  annotate("point", x=32, y=33, shape=1, size=4, color="gray20")+
  annotate("text", x=32, y=33, label="Pavilion", hjust=0, vjust=0, cex=3, color="gray20")+
  ggtitle("Subset 2")+
  facet_grid(day~id)

# subset 3
sub3_fri <- fri %>%
  filter(id %in% sub3_ids) %>%
  mutate(subgroup = 3) %>%
  mutate(day = "friday") %>%
  mutate(hour_f = (hour(timestamp)*60 + minute(timestamp))/60) %>%
  mutate(hour = hour(timestamp))
sub3_sat <- sat %>%
  filter(id %in% sub3_ids) %>%
  mutate(subgroup = 3) %>%
  mutate(day = "saturday") %>%
  mutate(hour_f = (hour(timestamp)*60 + minute(timestamp))/60) %>%
  mutate(hour = hour(timestamp))
sub3_sun <- sun %>%
  filter(id %in% sub3_ids) %>%
  mutate(subgroup = 3) %>%
  mutate(day = "sunday") %>%
  mutate(hour_f = (hour(timestamp)*60 + minute(timestamp))/60) %>%
  mutate(hour = hour(timestamp))
sub3 <- bind_rows(sub3_fri, sub3_sat, sub3_sun)
#plot
load("map_pos.RData")
map = ggplot(all_pos)+ geom_rect(aes(xmin=X, ymin=Y, xmax=X+1, ymax=Y+1), fill="white") + coord_fixed(xlim = c(-2, 102), ylim = c(-2, 102)) 
map
map + 
  geom_path(data = sub3, aes(x=X, y=Y, color=hour_f), position = position_jitter(w = 0.5, h=0.5)) +
  scale_color_gradientn(colours = brewer.pal(11, "Spectral"), space="rgb") +
  coord_fixed(xlim = c(0, 110), ylim = c(0, 110)) + 
  geom_point(data = sub3 %>% filter(type=="check-in"), aes(x=X, y=Y, color=hour_f), size= 3)+
  annotate("point", x=76, y=22, shape=1, size=4, color="gray20")+
  annotate("text", x=76, y=22, label="Stage", hjust=0, vjust=2,cex=3, color="gray20")+
  annotate("point", x=32, y=33, shape=1, size=4, color="gray20")+
  annotate("text", x=32, y=33, label="Pavilion", hjust=0, vjust=0, cex=3, color="gray20")+
  ggtitle("Subset 3")+
  facet_grid(day~id)

sub2_3 <- bind_rows(sub2, sub3)
map + 
  geom_path(data = sub2_3, aes(x=X, y=Y, color=hour_f), position = position_jitter(w = 0.5, h=0.5)) +
  scale_color_gradientn(colours = brewer.pal(11, "Spectral"), space="rgb") +
  coord_fixed(xlim = c(0, 110), ylim = c(0, 110)) + 
  geom_point(data = sub2_3 %>% filter(type=="check-in"), aes(x=X, y=Y, color=hour_f), size= 3)+
  annotate("point", x=76, y=22, shape=1, size=4, color="gray20")+
  annotate("text", x=76, y=22, label="Stage", hjust=0, vjust=2,cex=3, color="gray20")+
  annotate("point", x=32, y=33, shape=1, size=4, color="gray20")+
  annotate("text", x=32, y=33, label="Pavilion", hjust=0, vjust=0, cex=3, color="gray20")+
  ggtitle("Subset 2 and 3")+
  facet_grid(day~id)

# communication patterns ----
sub1_ids = c("521750", "644885", "1080969", "1600469", "1629516", "1781070", "1787551", "1935406")
sub2_ids = c("629048", "1486047", "1690685", "1797150")
sub3_ids = c("631146", "802410", "1856633")

load("com_pos_fri.RData")
sub1_fri <- com_fri %>%
  filter(to != "0") %>%
  filter(from != "1278894" & to != "1278894") %>%  
  filter(from != "839736" & to != "839736")%>% 
  filter(from %in% sub1_ids | to %in% sub1_ids)

# sub1_fri <- com_fri %>%
#   filter(to != "0") %>%
#   filter(from != "1278894" & to != "1278894") %>%  
#   filter(from != "839736" & to != "839736")%>% 
#   filter(from %in% sub1_ids)

# no communication on friday
load("com_pos_sat.RData")
sub1_sat <- com_sat %>%
  filter(to != "0") %>%
  filter(from != "1278894" & to != "1278894") %>%  
  filter(from != "839736" & to != "839736") %>% 
  filter(from %in% sub1_ids | to %in% sub1_ids)
# no communication on saturday
load("com_pos_sun.RData")
sub1_sun <- com_sun %>%
  filter(to != "0") %>%
  filter(from != "1278894" & to != "1278894") %>%  
  filter(from != "839736" & to != "839736") %>% 
  filter(from %in% sub1_ids | to %in% sub1_ids)
# no communication on sunday

# sub2
sub2_fri <- com_fri %>%
  filter(to != "0") %>%
  filter(from != "1278894" & to != "1278894") %>%  
  filter(from != "839736" & to != "839736") %>% 
  filter(from %in% sub2_ids | to %in% sub2_ids)
sub2_sat <- com_sat %>%
  filter(to != "0") %>%
  filter(from != "1278894" & to != "1278894") %>%  
  filter(from != "839736" & to != "839736") %>% 
  filter(from %in% sub2_ids | to %in% sub2_ids)
sub2_sun <- com_sun %>%
  filter(to != "0") %>%
  filter(from != "1278894" & to != "1278894") %>%  
  filter(from != "839736" & to != "839736") %>% 
  filter(from %in% sub2_ids | to %in% sub2_ids)
# sub3
sub3_fri <- com_fri %>%
  filter(to != "0") %>%
  filter(from != "1278894" & to != "1278894") %>%  
  filter(from != "839736" & to != "839736") %>% 
  filter(from %in% sub3_ids | to %in% sub3_ids)
sub3_sat <- com_sat %>%
  filter(to != "0") %>%
  filter(from != "1278894" & to != "1278894") %>%  
  filter(from != "839736" & to != "839736") %>% 
  filter(from %in% sub3_ids | to %in% sub3_ids)
sub3_sun <- com_sun %>%
  filter(to != "0") %>%
  filter(from != "1278894" & to != "1278894") %>%  
  filter(from != "839736" & to != "839736") %>% 
  filter(from %in% sub3_ids | to %in% sub3_ids)


#includes external and others
sub1_fri <- com_fri %>%
  filter(from %in% sub1_ids | to %in% sub1_ids)
# no communication on friday
sub1_sat <- com_sat %>%
  filter(from %in% sub1_ids | to %in% sub1_ids)
# no communication on saturday
sub1_sun <- com_sun %>%
  filter(from %in% sub1_ids | to %in% sub1_ids)
# no communication on sunday

# sub2
sub2_fri <- com_fri %>%
 filter(from %in% sub2_ids | to %in% sub2_ids)
sub2_sat <- com_sat %>%
  filter(from %in% sub2_ids | to %in% sub2_ids)
sub2_sun <- com_sun %>%
  filter(from %in% sub2_ids | to %in% sub2_ids)
# sub3
sub3_fri <- com_fri %>%
  filter(from %in% sub3_ids | to %in% sub3_ids)
sub3_sat <- com_sat %>%
  filter(from %in% sub3_ids | to %in% sub3_ids)
sub3_sun <- com_sun %>%
  filter(from %in% sub3_ids | to %in% sub3_ids)

# GC.2 busy attractions
busy = c("Firefall", "Flight of the Swingodon", "TerrorSaur", "Wendisaurus Chase")
locations <- read.csv("Derived_Data/park_locations.csv", quote="\"", header = TRUE)
locations <- locations %>%
  mutate(pos = paste(X, Y, sep="_"))
busy_loc <- locations %>%
  filter(name %in% busy)
load("move_fri.RData")
load("move_sat.RData")
load("move_sun.RData")

fri_busy <- friday %>%
  mutate(pos = paste(from_x, from_y, sep="_")) %>%
  filter(pos %in% busy_loc$pos) %>%
  filter(type =="checkin") %>%
  mutate(hour = hour(from_t)) %>%
  mutate(day = "friday") %>%
  mutate(minutes = duration/60) %>%
  left_join(locations, by="pos")

#box plot
ggplot(fri_busy, aes(factor(hour), minutes)) + geom_boxplot()+ facet_grid(name ~.)
ggplot(fri_busy, aes(factor(hour), minutes)) + geom_violin(scale="area")+ facet_grid(name ~.)
ggplot(fri_busy, aes(factor(hour), minutes)) + geom_violin(scale="width")+ facet_grid(name ~.)


sat_busy<- saturday %>%
  mutate(pos = paste(from_x, from_y, sep="_")) %>%
  filter(pos %in% busy_loc$pos) %>%
  filter(type =="checkin") %>%
  mutate(hour = hour(from_t)) %>%
  mutate(day = "saturday") %>%
  mutate(minutes = duration/60) %>%
  left_join(locations, by="pos")
#box plot or violin plot
ggplot(sat_busy, aes(factor(hour), minutes)) + geom_boxplot(outlier.shape = 1)+ facet_grid(name ~.)
ggplot(sat_busy, aes(factor(hour), minutes)) + geom_violin(scale="area")+ facet_grid(name ~.)
ggplot(sat_busy, aes(factor(hour), minutes)) + geom_violin(scale="count")+ facet_grid(name ~.)
ggplot(sat_busy, aes(factor(hour), minutes)) + geom_violin(scale="width")+ facet_grid(name ~.)

# sunday
sun_busy<- sunday %>%
  mutate(pos = paste(from_x, from_y, sep="_")) %>%
  filter(pos %in% busy_loc$pos) %>%
  filter(type =="checkin") %>%
  mutate(hour = hour(from_t)) %>%
  mutate(day = "sunday") %>%
  mutate(minutes = duration/60) %>%
  left_join(locations, by="pos")

ggplot(sun_busy, aes(factor(hour), minutes)) + geom_violin(scale="width")+ facet_grid(name ~.)
ggplot(sun_busy, aes(factor(hour), minutes)) + geom_violin(scale="area")+ facet_grid(name ~.)
ggplot(sun_busy, aes(factor(hour), minutes)) + geom_violin(scale="count")+ facet_grid(name ~.)

all_busy <- bind_rows(fri_busy, sat_busy, sun_busy)
# ggplot(all_busy, aes(factor(hour), minutes)) + geom_violin(scale="area")+ facet_grid(day ~ name)
# ggplot(all_busy, aes(factor(hour), minutes)) + geom_violin(scale="count")+ facet_grid(day ~ name)
# final
ggplot(all_busy, aes(factor(hour), minutes)) + geom_violin(scale="width")+ facet_grid(day ~ name)


# gps heatmap  ----
# plot count per location
load("gps_fri_2.RData")
fri_count <- fri %>%
  mutate(hour = hour(timestamp)) %>%
  group_by(X, Y, hour) %>%
  summarise(count = n()) %>%
  ungroup() %>%
  mutate(day = "friday")

load("gps_sat.RData")
sat_count <- sat %>%
  mutate(hour = hour(timestamp)) %>%
  group_by(X, Y, hour) %>%
  summarise(count = n()) %>%
  ungroup() %>%
  mutate(day = "saturday")

load("gps_sun.RData")
sun_count <- sun %>%
  mutate(hour = hour(timestamp)) %>%
  group_by(X, Y, hour) %>%
  summarise(count = n()) %>%
  ungroup() %>%
  mutate(day = "saturday")



#draw heatmap > conjested area
load("map_pos.RData")
map = ggplot(all_pos)+ geom_rect(aes(xmin=X, ymin=Y, xmax=X+1, ymax=Y+1), fill="white") + coord_fixed(xlim = c(-2, 102), ylim = c(-2, 102)) 

map+geom_rect(data = fri_count, aes(xmin = X, ymin = Y, xmax = X+1, ymax = Y+1, fill =count))+
  scale_fill_gradient(low ="#fff5f0", high="red")+facet_wrap(~hour)

map+geom_rect(data = sat_count, aes(xmin = X, ymin = Y, xmax = X+1, ymax = Y+1, fill =count))+
  scale_fill_gradient(low ="#fff5f0", high="red")+facet_wrap(~hour)

map+geom_rect(data = sun_count, aes(xmin = X, ymin = Y, xmax = X+1, ymax = Y+1, fill =count))+
  scale_fill_gradient(low ="#fff5f0", high="red")+facet_wrap(~hour)

map+geom_rect(data = sun_count, aes(xmin = X, ymin = Y, xmax = X+1, ymax = Y+1, fill =count))+
  scale_fill_gradient(low ="#fff5f0", high="red", limits=c(1000, 4000), na.value ="white")+facet_wrap(~hour)

range(sun_count$count)

#Entrance count
load("map_pos.RData")
map = ggplot(all_pos)+ geom_rect(aes(xmin=X, ymin=Y, xmax=X+1, ymax=Y+1), fill="white") + coord_fixed(xlim = c(-2, 102), ylim = c(-2, 102)) 

load("move_fri.RData")
load("move_sat.RData")
load("move_sun.RData")

locations <- read.csv("Derived_Data/park_locations.csv", quote="\"", header = TRUE)
locations <- locations %>%
  mutate(pos = paste(X, Y, sep="_"))

enter_fri <- friday %>%
  group_by(id) %>%
  summarise(X = first(from_x),
            Y = first(from_y),
            t = first(from_t),
            duration= first(duration),
            type = first(type)) %>%
  mutate(pos = paste(X, Y, sep="_")) %>%
  mutate(hour = hour(t)) %>%
  left_join(locations, by="pos") %>%
  mutate(day = "friday") %>%
  filter(!is.na(name)) %>%
  mutate(t2 = t)

enter_sat <- saturday %>%
  group_by(id) %>%
  summarise(X = first(from_x),
            Y = first(from_y),
            t = first(from_t),
            duration= first(duration),
            type = first(type)) %>%
  mutate(pos = paste(X, Y, sep="_")) %>%
  mutate(hour = hour(t)) %>%
  left_join(locations, by="pos") %>%
  mutate(day = "saturday") %>%
  filter(!is.na(name)) %>%
  mutate(t2 = t - days(1))


enter_sun <- sunday %>%
  group_by(id) %>%
  summarise(X = first(from_x),
            Y = first(from_y),
            t = first(from_t),
            duration= first(duration),
            type = first(type)) %>%
  mutate(pos = paste(X, Y, sep="_")) %>%
  mutate(hour = hour(t)) %>%
  left_join(locations, by="pos") %>%
  mutate(day = "sunday") %>%
  filter(!is.na(name)) %>%
  mutate(t2 = t - days(2))

enter_all <- bind_rows(enter_fri, enter_sat, enter_sun)


#bar chart
# ggplot(enter_fri)+geom_bar(aes(x=name))
# ggplot(enter_fri)+geom_bar(aes(x=t), binwidth = 60*30)
# ggplot(enter_fri)+geom_bar(aes(x=t), binwidth = 60*30)+facet_grid(name ~.)
# ggplot(enter_fri)+geom_violin(aes(x=name, y=duration), scale="count")
# 
# ggplot(enter_sat)+geom_violin(aes(x=name, y=duration), scale="area")
# ggplot(enter_sat)+geom_violin(aes(x=factor(hour), y=duration), scale="width")+facet_wrap(~name)
# ggplot(enter_all)+geom_bar(aes(x=name))+facet_wrap(~day)

# count
ggplot(enter_all)+geom_bar(aes(x=day))+facet_wrap(~name)

# duration
ggplot(enter_all)+geom_histogram(aes(x = t2), binwidth= 60*10) + facet_grid(day ~name) 

#binning
?cut
cut(enter_all$t2, breaks="10 mins")
enter_all_bin <- enter_all %>%
  group_by(name, day, cut(enter_all$t2, breaks="10 mins")) %>%
  summarise(count = n(),
            avg_duration = mean(duration))
colnames(enter_all_bin) = c("name", "day", "t", "count", "duration")
enter_all_bin$minutes <- enter_all_bin$duration/60

# enter_all_bin <- enter_all_bin %>%
#   mutate(day = wday(t, label = TRUE, abbr=FALSE))
enter_all_bin$t = as.POSIXct(enter_all_bin$t)

ggplot(enter_all_bin) + geom_bar(aes(x = t, y=count, fill= minutes), stat="identity") + facet_grid(day~name)


# stacked histogram
bins <- enter_all %>%
  mutate(time_bin = cut(t2, breaks="10 mins")) %>%
  mutate(minutes = duration /60) %>%
  mutate(min_bin = cut(bins$minutes, breaks = c(0,5, 10, 15, 20, 25)))

# ggplot(bins) + geom_bar(aes(x=time_bin, fill= min_bin), position="stack")+ facet_grid(day~name) + 
#   scale_fill_brewer(type = "seq", palette = "OrRd")
# ggplot(bins) + geom_bar(aes(x=time_bin, fill= min_bin), position="stack")+ facet_grid(day~name) + 
#   scale_fill_brewer(type = "seq", palette = "Blues")
# 
# ggplot(bins) + geom_bar(aes(x=time_bin, fill= min_bin), position="stack")+ facet_grid(day~name) + 
#   scale_fill_identity(values = c("#C6DBEF", "#9ECAE1", "#6BAED6", "#3182BD", "#08519C"))


### final
ggplot(bins) + geom_histogram(aes(x=t2, fill= min_bin),binwidth=60*10, position="stack")+ facet_grid(day~name)+ 
  scale_fill_manual(values = c("#6BAED6", "#4292C6", "#2171B5", "#08519C", "#08306B"))



dput(brewer.pal(9, "Blues"))
c("#C6DBEF", "#9ECAE1", "#6BAED6", "#4292C6", "#2171B5", 
  "#084594")

display.brewer.all()

range(bins$minutes)
cut(bins$minutes, breaks = c(0,5, 10, 15, 20, 25))

?geom_bar
