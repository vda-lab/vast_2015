#### those who come back several times, with just movements are also suspicious
library(dplyr)
library(ggplot2)
library(lubridate)
library(grid)
library(scales)
library(RColorBrewer)
library(tidyr)
## nice colors  = "aquamarine", "cornflowerblue", "coral", "darkcyan", "brown1", "darkolivegreen1", "darkorchid","deeppink", "gold", "royalblue", "seagreen1"
library(dendsort)
library(vegan)
# load data  ----
load("move_fri.RData")
load("move_sat.RData")
load("move_sun.RData")
locations <- read.csv("Derived_Data/park_locations.csv", quote="\"", header = TRUE)
locations <- locations %>%
  mutate(pos = paste(X, Y, sep="_"))

sub_sun <- sunday %>%
  filter(from_x == 32 & from_y == 33)
ids_sun <- sub_sun %>%
  group_by(id) %>%
  summarise(arrival = min(from_t)) %>%
  arrange(arrival) %>%            
  select(id) %>%
  t() %>%
  as.vector() %>%
  as.character()
#index individual
sub_sun <- sub_sun %>%
  mutate(index = match(id, ids_sun)) %>%
  mutate(day ="Sunday")
#plot
ggplot(sub_sun, aes(x=from_t,y=index)) +
  geom_segment(aes(xend=to_t, yend=index, color = type )) +
  theme(legend.position="bottom", axis.title.x=element_blank()) +
  ggtitle("Movement Pattern at the Creighton Pavilion on Sunday")+
  annotate("rect", xmin= as.POSIXct(strptime("2014-06-08 10:00:00", "%Y-%m-%d %H:%M:%S"), tz = "UTC"), 
           xmax= as.POSIXct(strptime("2014-06-08 11:30:00", "%Y-%m-%d %H:%M:%S"), tz = "UTC"), ymin =0, ymax = 4000,  alpha = 0.1, fill="grey20")

repeater <- sub_sun %>%
  group_by(id) %>%
  mutate(move_sum = sum(ifelse(type=="movement", duration, 0))) %>%
  mutate(move_count = sum(ifelse(type=="movement", 1, 0))) %>%
  ungroup() 
repeater <- repeater %>%
  arrange(desc(move_sum)) %>%
  filter(move_count > 2) %>%
  filter(move_sum >1000)

ggplot(repeater, aes(x=from_t,y=index)) +
  geom_segment(aes(xend=to_t, yend=index), color="black") +
  theme(legend.position="bottom", axis.title.x=element_blank()) +
  ggtitle("Movement Pattern at the Creighton Pavilion on Sunday")+
  annotate("rect", xmin= as.POSIXct(strptime("2014-06-08 10:00:00", "%Y-%m-%d %H:%M:%S"), tz = "UTC"), 
           xmax= as.POSIXct(strptime("2014-06-08 11:30:00", "%Y-%m-%d %H:%M:%S"), tz = "UTC"), ymin =0, ymax = 4000,  alpha = 0.1, fill="grey20")

#  coord_cartesian(xlim = c(ymd_hm("2014-06-06 08:00"), ymd_hm("2014-06-06 18:00"))) +
group1 = c("1038892", "1041478", "1159870", "124441", "1309055", "1350376", 
           "1358860", "1364488", "1390642", "1570276", "1635915", "1644402", 
           "1708002", "1742503", "1784014", "1872848", "1938686", "2047906", 
           "2056066", "2085681", "229760", "245363", "284127", "378256", 
           "408906", "508963", "543006", "611447", "651308", "661984", "668872", 
           "763108", "771453", "887530", "955733", "972182", "988181")

#before 12 exclude group1
b12_ids <- repeater %>%
  filter(from_t < ymd_hm("2014-06-08 11:00")) %>%
  filter(!id %in% group1) %>%
  select(id) %>%
  distinct() %>%
  t() %>%
  as.vector() %>%
  as.character()

dput(b12_ids)
repeater_ids = c("1711922", "1601276", "500084", "430595", "159893", "1149884", "1217381")
b11_ids = c("1711922", "430595", "159893", "1217381")
b12 <- repeater  %>% 
  filter(id %in% b12_ids) 
annotation <- b12 %>%
  group_by(id) %>%
  summarise(last_x = last(to_t),
            index = first(index))

ggplot(b12, aes(x=from_t,y=index)) +
  geom_segment(aes(xend=to_t, yend=index), color="black") +
  theme(legend.position="bottom", axis.title.x=element_blank()) +
  ggtitle("Movement Pattern at the Creighton Pavilion on Sunday")+
  annotate("rect", xmin= as.POSIXct(strptime("2014-06-08 10:00:00", "%Y-%m-%d %H:%M:%S"), tz = "UTC"), 
           xmax= as.POSIXct(strptime("2014-06-08 11:30:00", "%Y-%m-%d %H:%M:%S"), tz = "UTC"), ymin =0, ymax = 4000,  alpha = 0.1, fill="grey20")+
  annotate("text", x = annotation$last_x, y = annotation$index, label = annotation$id, color="black", cex = 3, hjust=0, vjust=0.5)


#movement pattern of repeaters
load("gps_sun.RData")
rep_sun <- sun %>%
  filter(id %in% repeater_ids)
rep_sun <- rep_sun %>%
  mutate(hour_f = (hour(timestamp)*60 + minute(timestamp))/60) %>%
  mutate(hour = hour(timestamp))

load("map_pos.RData")
map = ggplot(all_pos)+ geom_rect(aes(xmin=X, ymin=Y, xmax=X+1, ymax=Y+1), fill="white") + coord_fixed(xlim = c(-2, 102), ylim = c(-2, 102)) 

map + 
  geom_path(data = rep_sun, aes(x=X, y=Y, color=hour_f), position = position_jitter(w = 0.5, h=0.5)) +
  scale_color_gradientn(colours = brewer.pal(11, "Spectral"), space="rgb") +
  coord_fixed(xlim = c(0, 110), ylim = c(0, 110)) + 
  geom_point(data = rep_sun %>% filter(type=="check-in"), aes(x=X, y=Y, color=hour_f), size= 3)+
  geom_point(data = rep_sun %>% filter(type=="check-in"), aes(x=X, y=Y),color ="grey20", size= 4, shape = 1)+
  geom_text(data = rep_sun %>% filter(type=="check-in") %>% mutate(pos=paste(X, Y, sep="_")) %>% 
              left_join(locations %>% select(pos, name), by="pos"), aes(x=X, y=Y,label = name), hjust=0, vjust=2,cex=3, color="gray20")+
  facet_wrap(~id, nrow = 2)
map + 
  geom_path(data = rep_sun, aes(x=X, y=Y, color=hour_f), position = position_jitter(w = 0.5, h=0.5), color = "grey20") +
  facet_grid(id~hour)

#sequence plot
rep_sun_tra <- sunday %>%
  filter(id %in% repeater_ids)
#index individual
rep_mov <- rep_sun_tra %>%
  mutate(index = match(id, repeater_ids)) %>%
  mutate(pos = paste(from_x, from_y, sep="_")) %>%
  left_join(locations %>% select(pos, name, type, area), by="pos")
ggplot(rep_mov, aes(x=from_t,y=index)) +
  geom_segment(aes(xend=to_t, yend=index, color = name), size =3) +
  ggtitle("Movement pattern of Repeater on Sundsy")
ggplot(rep_mov, aes(x=from_t,y=index)) +
  geom_segment(aes(xend=to_t, yend=index, color = type.x), size =3) +
  ggtitle("Movement pattern of Repeater on Sundsy")

#check if they appear on sat and friday ----
#friday
load("move_fri.RData")
sub_fri <- friday %>%
  filter(from_x == 32 & from_y == 33)
ids_fri <- sub_fri %>%
  group_by(id) %>%
  summarise(arrival = min(from_t)) %>%
  arrange(arrival) %>%            
  select(id) %>%
  t() %>%
  as.vector() %>%
  as.character()
#index individual
sub_fri <- sub_fri %>%
  mutate(index = match(id, ids_fri)) %>%
  mutate(day ="friday")
#plot
ggplot(sub_fri, aes(x=from_t,y=index)) +
  geom_segment(aes(xend=to_t, yend=index, color = type )) +
  theme(legend.position="bottom", axis.title.x=element_blank()) +
  ggtitle("Movement Pattern at the Creighton Pavilion on friday")

repeater <- sub_fri %>%
  group_by(id) %>%
  mutate(move_sum = sum(ifelse(type=="movement", duration, 0))) %>%
  mutate(move_count = sum(ifelse(type=="movement", 1, 0))) %>%
  ungroup() 
repeater <- repeater %>%
  arrange(desc(move_sum)) %>%
  filter(move_count > 2) %>%
  filter(move_sum >1000)

ggplot(repeater, aes(x=from_t,y=index)) +
  geom_segment(aes(xend=to_t, yend=index), color="black") +
  theme(legend.position="bottom", axis.title.x=element_blank()) +
  ggtitle("Movement Pattern at the Creighton Pavilion on Friday")

repeater_fri <- repeater %>%
  select(id) %>%
  distinct() %>%
  t() %>%
  as.vector() %>%
  as.character
dput(repeater_fri)
repeater_fri = c("1081515", "1307724", "415491", "1763672")

annotation <- repeater %>%
  group_by(id) %>%
  summarise(last_x = last(to_t),
            index = first(index))
ggplot(repeater, aes(x=from_t,y=index)) +
  geom_segment(aes(xend=to_t, yend=index), color="black") +
  theme(legend.position="bottom", axis.title.x=element_blank()) +
  ggtitle("Movement Pattern at the Creighton Pavilion on Friday")+
  annotate("text", x = annotation$last_x, y = annotation$index, label = annotation$id, color="black", cex = 3, hjust=0, vjust=0.5)

#spectral plot
load("gps_fri.RData")
rep_fri <- fri %>%
  filter(id %in% repeater_fri)
rep_fri <- rep_fri %>%
  mutate(hour_f = (hour(timestamp)*60 + minute(timestamp))/60) %>%
  mutate(hour = hour(timestamp))

load("map_pos.RData")
map = ggplot(all_pos)+ geom_rect(aes(xmin=X, ymin=Y, xmax=X+1, ymax=Y+1), fill="white") + coord_fixed(xlim = c(-2, 102), ylim = c(-2, 102)) 

map + 
  geom_path(data = rep_fri, aes(x=X, y=Y, color=hour_f), position = position_jitter(w = 0.5, h=0.5)) +
  scale_color_gradientn(colours = brewer.pal(11, "Spectral"), space="rgb") +
  coord_fixed(xlim = c(0, 110), ylim = c(0, 110)) + 
  geom_point(data = rep_fri %>% filter(type=="check-in"), aes(x=X, y=Y, color=hour_f), size= 3)+
  geom_point(data = rep_fri %>% filter(type=="check-in"), aes(x=X, y=Y),color ="grey20", size= 4, shape = 1)+
  geom_text(data = rep_fri %>% filter(type=="check-in") %>% mutate(pos=paste(X, Y, sep="_")) %>% 
              left_join(locations %>% select(pos, name), by="pos"), aes(x=X, y=Y,label = name), hjust=0, vjust=2,cex=3, color="gray20")+
  facet_wrap(~id, nrow = 2)


#saturday
load("move_sat.RData")
sub_sat <- saturday %>%
  filter(from_x == 32 & from_y == 33)
ids_sat <- sub_sat %>%
  group_by(id) %>%
  summarise(arrival = min(from_t)) %>%
  arrange(arrival) %>%            
  select(id) %>%
  t() %>%
  as.vector() %>%
  as.character()
#index individual
sub_sat <- sub_sat %>%
  mutate(index = match(id, ids_sat)) %>%
  mutate(day ="saturday")
#plot
ggplot(sub_sat, aes(x=from_t,y=index)) +
  geom_segment(aes(xend=to_t, yend=index, color = type )) +
  theme(legend.position="bottom", axis.title.x=element_blank()) +
  ggtitle("Movement Pattern at the Creighton Pavilion on Saturday")

repeater <- sub_sat %>%
  group_by(id) %>%
  mutate(move_sum = sum(ifelse(type=="movement", duration, 0))) %>%
  mutate(move_count = sum(ifelse(type=="movement", 1, 0))) %>%
  ungroup() 
repeater <- repeater %>%
  arrange(desc(move_sum)) %>%
  filter(move_count > 2) %>%
  filter(move_sum >1000)

# ggplot(repeater, aes(x=from_t,y=index)) +
#   geom_segment(aes(xend=to_t, yend=index), color="black") +
#   theme(legend.position="bottom", axis.title.x=element_blank()) +
#   ggtitle("Movement Pattern at the Creighton Pavilion on Saturday")

repeater_sat <- repeater %>%
  select(id) %>%
  distinct() %>%
  t() %>%
  as.vector() %>%
  as.character
dput(repeater_sat)
repeater_sat = c("953838", "1725365", "1458915", "626433", "417205", "1983765")
annotation <- repeater %>%
  group_by(id) %>%
  summarise(last_x = last(to_t),
            index = first(index))
ggplot(repeater, aes(x=from_t,y=index)) +
  geom_segment(aes(xend=to_t, yend=index), color="black") +
  theme(legend.position="bottom", axis.title.x=element_blank()) +
  ggtitle("Movement Pattern at the Creighton Pavilion on Saturday")+
  annotate("text", x = annotation$last_x, y = annotation$index, label = annotation$id, color="black", cex = 3, hjust=0, vjust=0.5)

#spectral plot
load("gps_sat.RData")
rep_sat <- sat %>%
  filter(id %in% repeater_sat)
rep_sat <- rep_sat %>%
  mutate(hour_f = (hour(timestamp)*60 + minute(timestamp))/60) %>%
  mutate(hour = hour(timestamp))

load("map_pos.RData")
map = ggplot(all_pos)+ geom_rect(aes(xmin=X, ymin=Y, xmax=X+1, ymax=Y+1), fill="white") + coord_fixed(xlim = c(-2, 102), ylim = c(-2, 102)) 

map + 
  geom_path(data = rep_sat, aes(x=X, y=Y, color=hour_f), position = position_jitter(w = 0.5, h=0.5)) +
  scale_color_gradientn(colours = brewer.pal(11, "Spectral"), space="rgb") +
  coord_fixed(xlim = c(0, 110), ylim = c(0, 110)) + 
  geom_point(data = rep_sat %>% filter(type=="check-in"), aes(x=X, y=Y, color=hour_f), size= 3)+
  geom_point(data = rep_sat %>% filter(type=="check-in"), aes(x=X, y=Y),color ="grey20", size= 4, shape = 1)+
  geom_text(data = rep_sat %>% filter(type=="check-in") %>% mutate(pos=paste(X, Y, sep="_")) %>% 
              left_join(locations %>% select(pos, name), by="pos"), aes(x=X, y=Y,label = name), hjust=0, vjust=2,cex=3, color="gray20")+
  facet_wrap(~id, nrow = 2)

#1983765 odd

repeater_sun = repeater_ids
repeater_sun = c("1711922", "1601276", "500084", "430595", "159893", "1149884", "1217381")

repeater_sun %in% repeater_fri
repeater_sun %in% repeater_sat
repeater_fri %in% repeater_sat
#no overlaps

#communication pattern between repeaters
load("com_pos_sun.RData")
rep_sun_com <- com_sun %>%
  filter(from %in% repeater_sun | to %in% repeater_sun)

table(rep_sun_com$from, rep_sun_com$to)
# no interaction at all....


# combined histograms ----
load("com_sun.RData")
#subgraph1
sub1 = c("1038892", "1041478", "124441", "1309055", "1350376", "1358860", 
         "1364488", "1390642", "1570276", "1644402", "1784014", "1938686", 
         "2047906", "2056066", "2085681", "229760", "245363", "284127", 
         "378256", "408906", "508963", "543006", "611447", "651308", "763108", 
         "771453", "955733", "972182", "668872", "1159870", "1635915", 
         "1708002", "1742503", "1872848", "661984", "887530", "988181")

#subgraph2
sub2 = c("1120259", "1132417", "1155913", "1180332", "1229137", "1399218", 
         "1409556", "1411980", "1432187", "1446911", "1580679", "1615382", 
         "1620771", "170456", "1721744", "1725194", "174974", "1768584", 
         "1814974", "1882328", "1884134", "1897980", "1906865", "1937540", 
         "195725", "2002193", "2026293", "2028377", "2058029", "213448", 
         "240876", "266580", "291785", "365108", "408230", "412874", "427170", 
         "436644", "514438", "547838", "602296", "618704", "64304", "647535", 
         "729136", "777486", "830650", "856067", "896617", "910832", "968489", 
         "969340", "643918", "1559001", "634880", "1392907", "1208350", 
         "1507734", "1707585", "2095479", "282427", "347430", "546233", 
         "748592", "830269", "855159", "1623035", "664389", "2040751", 
         "1151513", "943665", "1246261", "1031147", "1227319", "1770588", 
         "1910940", "2013533", "535955", "772368", "1158948", "1168991", 
         "1215994", "1281551", "1460660", "1615853", "1643702", "1688395", 
         "1813111", "1825209", "19249", "376904", "451555", "502803", 
         "514417", "515929", "675346", "829943", "941906", "752462", "1022772", 
         "1034802", "1083901", "111118", "1166122", "1228230", "1230809", 
         "1356570", "1535918", "1581087", "1700246", "1841510", "1848459", 
         "1895027", "1952914", "1953077", "2007364", "365994", "400640", 
         "445493", "458951", "590805", "620184", "631684", "692899", "803659", 
         "866636", "972532", "988972", "439435", "1810447", "1128580", 
         "1970360", "583339", "1063521", "366288", "747874")

wet_sun <- sunday %>%
  filter(location == "Wet Land") %>%
  mutate(type = "others") %>%
  mutate(type = ifelse(from == "1278894" | to == "1278894", "1278894", type)) %>%
  mutate(type = ifelse(from == "839736" | to == "839736", "839736", type)) %>%
  mutate(type = ifelse(from == "0" | to == "0", "external", type)) %>%
  mutate(type = ifelse(from %in% sub1 | to %in% sub1, "subgraph1", type)) %>%
  mutate(type = ifelse(from %in% sub2 | to %in% sub2, "subgraph2", type)) 

# ordering factor levels
order = c( "1278894", "839736", "external", "others","subgraph1", "subgraph2")
wet_sun$type <- factor(wet_sun$type, levels = order )


#combined histograms
ggplot(wet_sun, aes(x=timestamp, fill=type))+stat_bin(binwidth= 60*5, geom="bar", position="stack") +
  scale_fill_manual(values = c(
    "others" = "grey40", 
    "1278894"="seagreen2", 
    "839736"="deepskyblue1", 
    "external"= "coral",
    "subgraph1"="brown1", 
    "subgraph2"= "darkorchid"),name ="") +
  xlab("")+ylab("")+
  coord_cartesian(ylim = c(0, 12000), xlim = c(ymd_hm("2014-06-08 07:30"), ymd_hm("2014-06-08 24:00")))+
  scale_x_datetime(breaks = seq(as.POSIXct(strptime("2014-06-08 08:00:00", "%Y-%m-%d %H:%M:%S")), as.POSIXct(strptime("2014-06-08 24:00:00", "%Y-%m-%d %H:%M:%S")), by = 60*60),
                   labels = date_format("%H:%M"))



