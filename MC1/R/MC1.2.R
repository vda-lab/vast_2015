#MC1.2: notable differences
library(dplyr)
library(ggplot2)
library(lubridate)
library(grid)
library(scales)
library(RColorBrewer)
library(tidyr)
## nice colors  = "aquamarine", "cornflowerblue", "coral", "darkcyan", "brown1", "darkolivegreen1", "darkorchid","deeppink", "gold", "royalblue", "seagreen1"
library(dendsort)
library(vegan)
# load data  ----
load("move_fri.RData")
load("move_sat.RData")
load("move_sun.RData")
locations <- read.csv("Derived_Data/park_locations.csv", quote="\"", header = TRUE)
locations <- locations %>%
  mutate(pos = paste(X, Y, sep="_"))


# get stationary time
fri_set <- friday %>%
  filter(speed < 0.01) %>%
  rename(u_id = id) %>%
  mutate(pos = paste(from_x, from_y, sep="_")) %>%
  left_join(locations[, c("id", "name", "pos", "type", "area")], by="pos") %>%
  filter(!is.na(id)) %>%
  mutate(day = "friday") %>%
  mutate(time = from_t)
sat_set <- saturday %>%
  filter(speed < 0.01) %>%
  rename(u_id = id) %>%
  mutate(pos = paste(from_x, from_y, sep="_")) %>%
  left_join(locations[, c("id", "name", "pos", "type", "area")], by="pos") %>%
  filter(!is.na(id)) %>%
  mutate(day = "saturday") %>%
  mutate(time = from_t - days(1))
sun_set <- sunday %>%
  filter(speed < 0.01) %>%
  rename(u_id = id) %>%
  mutate(pos = paste(from_x, from_y, sep="_")) %>%
  left_join(locations[, c("id", "name", "pos", "type", "area")], by="pos") %>%
  filter(!is.na(id)) %>%
  mutate(day = "sunday")%>%
  mutate(time = from_t - days(2))
all_set <- bind_rows(fri_set, sat_set, sun_set)

#plotting
# ggplot(fri_set) +geom_step(aes(x= from_t), stat="bin", binwidth=60*60) +facet_wrap(~name)
# ggplot(all_set) +geom_step(aes(x= time, color=day), stat="bin", binwidth=60*60) +facet_wrap(~name)

#Figure 10
ggplot(all_set) +geom_step(aes(x= time, color=day), stat="bin", binwidth=60*60) +facet_wrap(~name, scales="free_y", ncol = 5) 
# ggplot(all_set) +geom_step(aes(x= time, color=day), stat="bin", binwidth=60*10) +facet_wrap(~type.y, scales="free_y", ncol = 1) 

ggplot(all_set) +geom_step(aes(x= time, color=day), stat="bin", binwidth=60*30) +facet_grid(type.y~area, scales="free_y") 

?element_rect

locations$name
locations[match("Liggement Fix-Me-Up",locations$name), ]
Liggement<-fri_set %>%
  filter(from_x == 50 & from_y == 57)

ggplot(Liggement, aes(x= from_t)) + geom_histogram(binwidth = 60)






# time spent in attractions ----
all_set <- all_set %>%
  mutate(minutes = duration/60)
ggplot(all_set) +geom_step(aes(x= minutes, color=day), stat="bin", binwidth=1) +facet_wrap(~name, scales="free", ncol = 5) 








