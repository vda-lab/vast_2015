PVector m_pressed = null;
PVector m_released = null;
PVector m_dragged = null;


void keyPressed(){
	if(key == '1'){
		//sort by beginning
		Collections.sort(person_array, new PersonComparator(SORT_FIRST));
		set_y_pos();
		pg.clear();
		render_plot(pg);
		loop();
	}else if(key == '2'){
		//sort by exit
		Collections.sort(person_array, new PersonComparator(SORT_LAST));
		set_y_pos();
		pg.clear();
		render_plot(pg);
		loop();
	}else if(key == '3'){
		//sort by entrace point then time
		//update person order index
		for(int i = 0; i < person_array.size(); i++){
			//check the first area type
			Person p = person_array.get(i);
			p.update_order_by_area(p.first_index);
		}
		Collections.sort(person_array, new PersonComparator(SORT_FIRST));
		Collections.sort(person_array, new PersonComparator(ORDER_INDEX));
		set_y_pos();
		pg.clear();
		render_plot(pg);
		loop();
	}else if(key == '4'){
		//sort by exit time then exit point area
		//update person order index
		for(int i = 0; i < person_array.size(); i++){
			//check the first area type
			Person p = person_array.get(i);
			p.update_order_by_area(p.last_index);
		}
		Collections.sort(person_array, new PersonComparator(SORT_LAST));
		Collections.sort(person_array, new PersonComparator(ORDER_INDEX));
		set_y_pos();
		pg.clear();
		render_plot(pg);
		loop();
	}else if(key == '5'){
		if(hovering_time_index != -1){
			//sort by area at that particular time of the day
			//update person order index
			for(int i = 0; i < person_array.size(); i++){
				//check the first area type
				Person p = person_array.get(i);
				p.update_order_by_area(hovering_time_index);
			}
			Collections.sort(person_array, new PersonComparator(ORDER_INDEX));
			set_y_pos();
			pg.clear();
			render_plot(pg);
			loop();
		}
	}else if(key == 'g'){
		//swap grid
		if(current_grid == ORIGINAL_GRID){
			current_grid = CUSTOM_GRID;
		}else if(current_grid == CUSTOM_GRID){
			current_grid = ORIGINAL_GRID;
		}
		pg.clear();
		render_plot(pg);
		loop();
	}
}

void mouseMoved(){
	cursor(ARROW);
	hovering_time_index = -1;
	hovering_now = null;

	if(plot_rect.contains(mouseX, mouseY)){
		cursor(CROSS);
		if(mousePressed){
			//hovering time index
			hovering_time_index = constrain(round(map(m_pressed.x, plot_rect.x, plot_rect.x+plot_rect.width, 0, time_interval_count)), 0 , time_interval_count-1);
		}else{
			//hovering time index
			hovering_time_index = constrain(round(map(mouseX, plot_rect.x, plot_rect.x+plot_rect.width, 0, time_interval_count)), 0 , time_interval_count-1);
		}
		int seconds = min_sec+hovering_time_index*time_interval;
		hovering_now = new Date((long)(seconds)*(1000l));
	}
	loop();
}

void mousePressed(){
	m_pressed = new PVector(mouseX, mouseY);
	m_released = null;
	m_dragged = null;
	hovering_time_index = constrain(round(map(m_pressed.x, plot_rect.x, plot_rect.x+plot_rect.width, 0, time_interval_count)), 0 , time_interval_count-1);
	int seconds = min_sec+hovering_time_index*time_interval;
	hovering_now = new Date((long)(seconds)*(1000l));
	loop();
}

void mouseDragged(){
	m_dragged = new PVector(mouseX, mouseY);
	loop();
}

void mouseReleased(){
	m_released = new PVector(mouseX, mouseY);

	
	if(m_dragged != null && m_pressed != null){
		float y_from = m_pressed.y;
		float y_to = m_released.y;
		if(m_pressed.y > m_released.y){
			y_from = m_released.y;
			y_to = m_pressed.y;
		}
		ArrayList<Person> unaffected_top = new ArrayList<Person>();
		ArrayList<Person> affected = null;
		ArrayList<Person> unaffected_bottom = null; 

		//adjust order index for the rage
		for(int i = 0; i < person_array.size(); i++){
			Person p = person_array.get(i);
			if(unaffected_bottom == null){
				if(affected == null){
					if(y_from <= p.dy && p.dy <= y_to){
						//in the range
						affected = new ArrayList<Person>();
						p.update_order_by_area(hovering_time_index);
						affected.add(p);
					}else{
						unaffected_top.add(p);
					}
				}else{
					if(y_from <= p.dy && p.dy <= y_to){
						//in the range
						p.update_order_by_area(hovering_time_index);
						affected.add(p);
					}else{
						unaffected_bottom = new ArrayList<Person>();
						unaffected_bottom.add(p);
					}
				}
			}else{
				unaffected_bottom.add(p);
			}
		}
		if(affected == null){
			affected = new ArrayList<Person>();
		}
		if(unaffected_bottom == null){
			unaffected_bottom = new ArrayList<Person>();
		}
		println("Debug: top = "+unaffected_top.size()+"\taffected = "+affected.size()+ "\tbottom = "+unaffected_bottom.size());

		//sort affected
		Collections.sort(affected, new PersonComparator(ORDER_INDEX));
		//combine
		unaffected_top.addAll(affected);
		unaffected_top.addAll(unaffected_bottom);
		person_array = unaffected_top;
		//re-render
		set_y_pos();
		pg.clear();
		render_plot(pg);
	}
	//reset
	m_released = null;
	m_dragged = null;
	m_pressed = null;
	hovering_time_index = -1;
	//loop
	loop();
}
