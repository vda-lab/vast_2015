import java.io.FileInputStream;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.*;
import java.awt.*;
import java.awt.event.*;
import java.text.*;
import weka.core.*;
import weka.clusterers.AbstractClusterer;
import weka.clusterers.HierarchicalClusterer;


ArrayList<Location> location_array;
HashMap<String, Location> location_map;
HashSet<String> unique_location_id;
HashSet<String> selected_location_id;


ArrayList<Grid> grid_array;
HashMap<String, Grid> grid_map;


ArrayList<Person> person_array;
HashMap<Integer, Person> person_map;
Date min_time, max_time;
long start_m_sec, end_m_sec;  //milliseconds
int min_sec, max_sec;			// min and max in seconds
int time_interval_count  = 0; 	// number of minutes
int time_interval = 0; 			// number of seconds per interval


PGraphics pg;

int _margin = 10;
int _left_margin = _margin*3;
int _right_margin = _margin*3;
int _top_margin = _margin*3;
int _bottom_margin = _margin*3;
int _menu_bar = _margin*5;
float _line_weight  = 1.0f;

int _plot_width, _plot_height;
Rectangle plot_rect;

//CPU optimization
int draw_counter = 0;
int draw_max = 30; //1 seconds = draw 60 times before it stops


int c_light_blue = color(236,231,242);
int c_movement = color(0,0,0,50);

PFont font;

boolean showGreyLines = false;


String[] area_types = {"Entry Corridor", "Wet Land","Kiddie Land","Coarster Alley","Tundra Land"};
ArrayList<String> area_type_array;
int[] area_colors= {color(190,186,218), color(141,211,199), color(255,255,179), color(251,128,114), color(128,177,211)}; 

//custom grid
int[] color_array = {color(228,26,28),color(55,126,184),color(77,175,74),color(152,78,163),color(255,127,0),color(255,255,51),color(166,86,40),color(247,129,191),color(153,153,153)};



SimpleDateFormat ft = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
SimpleDateFormat hourFormat = new SimpleDateFormat("HH:mm");
SimpleDateFormat weekFormat = new SimpleDateFormat("EEEE, MMMM dd yyyy");

static final int SORT_FIRST = 0;
static final int SORT_LAST = 1;
static final int ORDER_INDEX = 2;

static final int ORIGINAL_GRID = 0;
static final int CUSTOM_GRID = 1;	

//weka stufff
WekaData mydata;
WekaHierarchicalClusterer clusterer;


//interaction
int hovering_time_index = -1;
Date hovering_now = null;


//Current status
int current_grid = CUSTOM_GRID;
// int current_grid = ORIGINAL_GRID;


void setup(){
	person_map = new HashMap<Integer, Person>();
	person_array = new ArrayList<Person>();
	selected_location_id = new HashSet<String>();

	//test
	selected_location_id.add("32_33");

	load_matrix_x("/Users/Ryo/Desktop/PhD/Year_4/22_VAST_2015/Derived_Data/x_matrix_Fri.csv");
	load_matrix_y("/Users/Ryo/Desktop/PhD/Year_4/22_VAST_2015/Derived_Data/y_matrix_Fri.csv");

	// load_matrix_x("/Users/Ryo/Desktop/PhD/Year_4/22_VAST_2015/Derived_Data/x_matrix_Sat.csv");
	// load_matrix_y("/Users/Ryo/Desktop/PhD/Year_4/22_VAST_2015/Derived_Data/y_matrix_Sat.csv");
	// load_matrix_x("/Users/Ryo/Desktop/PhD/Year_4/22_VAST_2015/Derived_Data/x_matrix_Sun.csv");
	// load_matrix_y("/Users/Ryo/Desktop/PhD/Year_4/22_VAST_2015/Derived_Data/y_matrix_Sun.csv");


	//load_grid
	load_grid("park_grids_area.csv");
	load_custom_grid("/Users/Ryo/Desktop/PhD/Year_4/22_VAST_2015/Derived_Data/custom_grid_1.csv");

	//load locations
	load_location("park_locations.csv");

	area_type_array = new ArrayList<String>(Arrays.asList(area_types));

	//setup stage
	int stage_width = displayWidth;
	_plot_width = stage_width - _left_margin - _right_margin;
	int stage_height = displayHeight  - _menu_bar;
	_plot_height = stage_height - _top_margin - _bottom_margin;

	size(stage_width, stage_height);
	plot_rect = new Rectangle(_left_margin, _top_margin, _plot_width, _plot_height);

	//debug
	float line_y_gap = (float)_plot_height / (float)person_array.size();
	// println("Debug: line_y_gap ="+line_y_gap +"  height = "+_plot_height+"  people="+person_array.size());
	_line_weight = 0.5;


	font = createFont("SUPERNAT1001.TTF", 10);
	textFont(font, 10);


	//sort Person
	Collections.sort(person_array, new PersonComparator(SORT_FIRST));
	// Collections.sort(person_array, new PersonComparator(SORT_LAST));
	//set dy
	set_y_pos();


	//set data
	// mydata = getData(0, person_array.size()-1, 150, 151);

	//debug
	// for(int i =0; i<mydata.myInstances.numInstances(); i++){
	// 	println("ID: " + mydata.myInstances.instance(i).value(0));
	// 	println("\t" + mydata.myInstances.instance(i).toString());
	// }
	// clusterer = new WekaHierarchicalClusterer();
	//run clustering
	// String output = clusterer.clusterDataGetOrder(mydata);
	// String[] lines = split(output, "\n");
	// ArrayList<String> result = parseNewwick(lines[1].trim());

	//assign new index
	// ArrayList<Person> unselected = new ArrayList<Person>(person_array.subList(102, person_array.size()));

	// int runningScore = 0;
	// for(String id:result){
	// 	Person person = (Person) person_map.get(new Integer(id));
	// 	if(person  == null){
	// 		println("Error: person not found:"+id);
	// 	}else{
	// 		person.order_index = runningScore;
	// 		runningScore ++;
	// 	}
	// }
	// for(Person p:unselected){
	// 	p.order_index = runningScore;
	// 	runningScore ++;
	// }

	// Collections.sort(person_array, new PersonComparator(ORDER_INDEX));
	// set_y_pos();
	// println("Clustering Result! "+result);

	//debug
	// println("Distance Function Tip:");
	// println(((HierarchicalClusterer)clusterer.myClusterer).distanceFunctionTipText());
	// println("Branch height Tip:");
	// println(((HierarchicalClusterer)clusterer.myClusterer).distanceIsBranchLengthTipText());
	// println("Newick Tip:");
	// println(((HierarchicalClusterer)clusterer.myClusterer).printNewickTipText());
	//render 
	pg = createGraphics(stage_width, stage_height);
	render_plot(pg);

	// exit();
	noLoop();
}	

void draw(){
	background(230);

	if(pg!= null){
		image(pg, 0,0);
	}

	draw_interaction();

	if(draw_counter > draw_max){
		println(" --------- ");
		noLoop();
		draw_counter = 0;
	}else{
		draw_counter ++;
	}
}


void draw_interaction(){
	//hovering time
	if(hovering_time_index != -1){
		//get Date
		int seconds = min_sec+hovering_time_index*time_interval;
		// Date now = new Date((long)(seconds)*(1000l));
		String time_label  = hourFormat.format(hovering_now);
		float label_width = textWidth(time_label) + _margin;
		float dx = map(seconds, min_sec, max_sec, plot_rect.x, plot_rect.x+plot_rect.width);

		if(mousePressed){
			if(m_pressed!=null && m_dragged != null){
				//line
				strokeWeight(1);
				stroke(0);
				line(dx, m_pressed.y, dx, m_dragged.y);
				line(dx - 2, m_pressed.y, dx+2, m_pressed.y);
				line(dx - 2, m_dragged.y, dx+2, m_dragged.y);
			}
		}else{
			//background
			fill(230, 230);
			noStroke();
			rect(dx - (label_width/2), plot_rect.y -15, label_width, 15);
			textAlign(CENTER, BOTTOM);
			fill(0);
			text(time_label, dx, plot_rect.y-2);
			//line
			strokeWeight(1);
			stroke(0);
			line(dx, plot_rect.y, dx, plot_rect.y+plot_rect.height);
		}
	}
}

//grid
void load_grid(String url){
	grid_array = new ArrayList<Grid>();
	grid_map = new HashMap<String, Grid>();
	HashSet<String> unique_type = new HashSet<String>();

	String[] lines = loadStrings(url);
	for(String l: lines){
		if(l.startsWith("id")){
			//header
		}else{
			String[] s = split(l, ",");
			String id = s[0];
			int gx = Integer.parseInt(s[1]);
			int gy = Integer.parseInt(s[2]);
			String area_type = s[3].trim();

			Grid g = new Grid(id, gx, gy, area_type);
			grid_array.add(g);
			grid_map.put(id, g);
			unique_type.add(area_type);
		}
	}

	println("Debug:total number of girds ="+grid_array.size());
	println("Debug:unique type ="+unique_type);
}

void load_custom_grid(String url){
	String[] lines = loadStrings(url);
	for(String l: lines){
		if(l.startsWith("id")){
			//header
		}else{
			String[] s = split(l, ",");
			String id = s[0];
			int gx = Integer.parseInt(s[1]);
			int gy = Integer.parseInt(s[2]);
			String area_type = s[3].trim();

			Grid g = (Grid) grid_map.get(id);
			if(g == null){
				println("Error: custom grid: cannot find:"+id);
			}else{
				g.custom_type = area_type;
			}
		}
	}
	//debug
	// for(Grid g: grid_array){
	// 	if(g.custom_type == ""){
	// 		println("Error: missing type: "+g.id);
	// 	}
	// }
}


void load_location(String url){
	location_array = new ArrayList<Location>();
	location_map = new HashMap<String, Location>();
	unique_location_id = new HashSet<String>();
	String[] lines = loadStrings(url);
	for(String l : lines){
		if(l.startsWith("id")){
			//header
		}else{
			String[] s = split(l, ",");
			int index = Integer.parseInt(s[0]);
			String name = s[1].trim();
			String type = s[2].trim();
			String area = s[3].trim();
			int lx = Integer.parseInt(s[4]);
			int ly = Integer.parseInt(s[5]);

			String label = lx+"_"+ly;
			Location loc = new Location(label, index, lx, ly, name, type, area);
			location_array.add(loc);
			location_map.put(label, loc);
			unique_location_id.add(label);
		}
	}
	println("Debug:"+location_array.size()+" locations loaded");
}

void load_matrix_x(String url){
	int counter = 0;
	File file = new File(url);
	int array_size = 0;
	try {
		FileInputStream fstream = new FileInputStream(file);
		BufferedReader br = new BufferedReader(new InputStreamReader(fstream));
		String line;
		//Read File Line By Line
		while ((line = br.readLine()) != null){
			if(counter%100000 == 0){
				println("loading data:"+counter);
			}
			if(counter == 0){
				println("header:"+line);
				String header = line.substring(1);
				String[] s = split(header,",");
				int start_sec = Integer.parseInt(s[0]);
				int end_sec = Integer.parseInt(s[1]);
				int interval = Integer.parseInt(s[2]);
				array_size = (end_sec - start_sec)/interval;
				println("array_size ="+array_size);
				time_interval_count = array_size;
				time_interval = interval;

				if(min_time == null){
					min_time = new Date((long)start_sec * 1000l);
					max_time = new Date((long)end_sec * 1000l);
					min_sec = start_sec;
					max_sec = end_sec;

					println("Debug: start:\t"+min_time);
					println("Debug: end:\t"+max_time);
				}
			}else{
				String[] s = split(line, ",");
				int id = Integer.parseInt(s[0]);
				int[] x_pos = new int[array_size];
				for(int i = 1; i<s.length; i++){
					x_pos[i-1] = Integer.parseInt(s[i]);
				}
				//find Person
				Person p = (Person) person_map.get(new Integer(id));
				if(p == null){
					p = new Person(id);
					person_array.add(p);
					person_map.put(new Integer(id), p);
				}
				p.x_pos = x_pos;
				//find first and the last index
				for(int i = 0; i<p.x_pos.length; i++){
					if(p.x_pos[i] != -1){
						p.first_index = i;
						break;
					}
				}
				for(int i = p.x_pos.length-1; i >= 0; i--){
					if(p.x_pos[i] != -1){
						p.last_index = i;
						break;
					}
				}
				// println("Debug: "+id+" first:"+p.first_index+" last:"+p.last_index);

			}
		  	counter ++;
		}
		//Close the input stream
		br.close();
	}catch(Exception e){
		e.printStackTrace();
	}
	println("Debug: total number of people:"+person_array.size());
}

void load_matrix_y(String url){
	int counter = 0;
	File file = new File(url);
	int array_size = 0;
	try {
		FileInputStream fstream = new FileInputStream(file);
		BufferedReader br = new BufferedReader(new InputStreamReader(fstream));
		String line;
		//Read File Line By Line
		while ((line = br.readLine()) != null){
			if(counter%100000 == 0){
				println("loading data:"+counter);
			}
			if(counter == 0){
				println("header:"+line);
				String header = line.substring(1);
				String[] s = split(header,",");
				int start_sec = Integer.parseInt(s[0]);
				int end_sec = Integer.parseInt(s[1]);
				int interval = Integer.parseInt(s[2]);
				array_size = (end_sec - start_sec)/interval;
				println("array_size ="+array_size);

				if(min_time == null){
					min_time = new Date((long)start_sec * 1000l);
					max_time = new Date((long)end_sec * 1000l);

					println("Debug: start:\t"+min_time);
					println("Debug: end:\t"+max_time);
				}
			}else{
				String[] s = split(line, ",");
				int id = Integer.parseInt(s[0]);
				int[] y_pos = new int[array_size];
				for(int i = 1; i<s.length; i++){
					y_pos[i-1] = Integer.parseInt(s[i]);
				}
				//find Person
				Person p = (Person) person_map.get(new Integer(id));
				if(p == null){
					p = new Person(id);
					person_array.add(p);
					person_map.put(new Integer(id), p);
				}
				p.y_pos = y_pos;
				// println("Debug: "+id+" first:"+p.first_index+" last:"+p.last_index);
			}
		  	counter ++;
		}
		//Close the input stream
		br.close();
	}catch(Exception e){
		e.printStackTrace();
	}
	println("Debug: total number of people:"+person_array.size());
}

void set_y_pos(){
	for(int i = 0; i<person_array.size(); i++){
		Person p = person_array.get(i);
		p.dy = map(i, 0, person_array.size()-1, plot_rect.y, plot_rect.y+plot_rect.height);
	}
}


//both inclusive
WekaData getData(int p_from, int p_to, int t_from, int t_to){
	int row_length = 1 + (t_to - t_from +1)*2;
	int row_count = p_to - p_from +1;
	WekaData data = new WekaData(row_length, row_count);
	//add attribute 
	data.AddStringAttribute("id");
	for(int i = t_from; i <= t_to; i++){
		data.AddAttribute("x"+i);
		data.AddAttribute("y"+i);
	}
	//add data point
	for(int i = p_from; i <= p_to; i++){
		Person p = person_array.get(i);
		Object[]row = new Object[row_length];
		row[0] = ""+p.id;
		int runningIndex = 1;
		for(int j = t_from; j <= t_to; j++){
			row[runningIndex] = p.x_pos[j];
			runningIndex++;
			row[runningIndex] = p.y_pos[j];
			runningIndex++;
		}
		Object[][] tableRow = new Object[][]{row};
		data.InsertData(tableRow);
	}
	return data;
}



void render_plot(PGraphics p){
	println("*** start rendering ***");
	p.beginDraw();
	p.textFont(font, 10);
	p.fill(255);
	p.noStroke();
	p.rect(plot_rect.x, plot_rect.y, plot_rect.width, plot_rect.height);

	//day of the week
	p.fill(0);
	p.textAlign(LEFT, TOP);
	p.text(weekFormat.format(min_time), _left_margin, 5);

	//draw time intervals
	for(Date d = new Date(min_time.getTime()); d.before(max_time); d = increment_minute(d, 30)){
		int seconds = getSeconds(d);
		String label = hourFormat.format(d);
		float dx = map(seconds, min_sec, max_sec, plot_rect.x, plot_rect.x+plot_rect.width);
		//label
		p.fill(0);
		p.textAlign(CENTER, BOTTOM);
		p.text(label, dx, plot_rect.y -2);
		p.textAlign(CENTER, TOP);
		p.text(label, dx, plot_rect.y + _plot_height);
		//line
		p.stroke(230);
		p.strokeWeight(1);
		p.noFill();
		p.line(dx, plot_rect.y-2, dx, plot_rect.y+plot_rect.height);
	}
	//draw person
	for(int i = 0; i<person_array.size(); i++){
		Person per = person_array.get(i);
		if(current_grid == ORIGINAL_GRID){
			per.render(p);
		}else if(current_grid == CUSTOM_GRID){
			per.render_custom_grid(p);
		}

		// per.render_locations(p);


		// println("Debug:last:"+per.last_index);
	}

	p.endDraw();

	println("*** end rendering ***");
}




/// utility
//// Date
int getYear(Date d){
	Calendar cal = GregorianCalendar.getInstance();
	cal.setTime(d);
	return cal.get(Calendar.YEAR);
}
int getMonth(Date d){
	Calendar cal = Calendar.getInstance();
	cal.setTime(d);
	return cal.get(Calendar.MONTH);
}
int getDay(Date d){
	Calendar cal = Calendar.getInstance();
	cal.setTime(d);
	return cal.get(Calendar.DAY_OF_MONTH);
}
int getHour(Date d){
	Calendar cal = GregorianCalendar.getInstance();
	cal.setTime(d);
	return cal.get(Calendar.HOUR_OF_DAY);
}
int getMin(Date d){
	Calendar cal = GregorianCalendar.getInstance();
	cal.setTime(d);
	return cal.get(Calendar.MINUTE);
}
int getSec(Date d){
	Calendar cal = GregorianCalendar.getInstance();
	cal.setTime(d);
	return cal.get(Calendar.SECOND);
}
int getSeconds(Date d){
	// return (int)(d.getTime()/1000);
	long l = d.getTime();
	int result = (int) (l/1000l);
	return result;
}
Date increment_sec(Date d){
	long t = d.getTime(); //millisecond
	t =  t+1000;
	return new Date(t);
}
Date increment_sec(Date d, int n){
	long t = d.getTime(); //millisecond
	t =  t+1000*n;
	return new Date(t);
}
Date increment_minute(Date d, int m){
	long t = d.getTime();
	t = t+ (1000l)*m*60l;
	return new Date(t);
}

float log10 (float x) {
  return (log(x) / log(10));
}
