ArrayList<String> parseNewwick (String s){
	// println(" --- parseNewick() "+s);
	ArrayList<String> result = new ArrayList<String>();
	StringTokenizer st = new StringTokenizer(s, "(,)", true);
	String token = st.nextToken();
	if (token.equals("(")) {
		// Inner node
		ArrayList<String> left = parseNewick(st);
		String comma = st.nextToken();
		ArrayList<String> right = parseNewick(st);	
		String close = st.nextToken();
		String label = "";
		if(st.hasMoreElements()){
			label = st.nextToken();
			String[] pieces = label.split(":");
			String piece = pieces[0].trim();
			if(!piece.equals("")){
				result.add(piece);
			}
		}
		if(left.size()> 0){
			result.addAll(left); 
		}
		if(right.size()> 0){
			result.addAll(right); 
		}
		return result;
	}
	else {
		// Leaf
		String[] pieces = token.split(":");
		String piece = pieces[0].trim();
		if(!piece.equals("")){
			result.add(piece);
		}
		return  result;
	}
}

ArrayList<String> parseNewick(StringTokenizer st) {
	// println("parseNewick() -- token "+st);
	String token = st.nextToken();
	ArrayList<String> result = new ArrayList<String>();
	if (token.equals("(")) {
		// Inner node
		ArrayList<String> left = parseNewick(st);
		String comma = st.nextToken();
		ArrayList<String> right = parseNewick(st);	
		String close = st.nextToken();
		String label = "";
		if(st.hasMoreElements()){
			label = st.nextToken();
			String[] pieces = label.split(":");
			String piece = pieces[0].trim();
			if(!piece.equals("")){
				result.add(piece);
			}
		}
		if(left.size()> 0){
			result.addAll(left); 
		}
		if(right.size()> 0){
			result.addAll(right); 
		}
		return result;
	}
	else {
		// Leaf
		String[] pieces = token.split(":");
		String piece = pieces[0].trim();
		if(!piece.equals("")){
			result.add(piece);
		}
		return  result;
	}
}