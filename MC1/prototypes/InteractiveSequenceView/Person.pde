class Person{
	int id;
	int[] x_pos, y_pos;
	int first_index = 0; 
	int last_index = 0;
	int order_index = 0;

	float dy = 0; //display position

	Person(int id){
		this.id = id;
	}

	void render(PGraphics p){
		int prev_x = -1;
		int prev_y = -1;
		float prev_dx = plot_rect.x;
		int prev_type = -1;

		p.strokeCap(SQUARE);
		p.strokeWeight(_line_weight);
		// for(int i = first_index; i <= last_index; i++){
		// for(int i = 0; i < x_pos.length; i++){
		for(int i = 0; i <= last_index; i++){
			int px = x_pos[i];
			int py = y_pos[i];
			if(px == prev_x && py == prev_y){
				//same position as before
				if(i == last_index){
					float new_dx = map(i, 0, x_pos.length, plot_rect.x, plot_rect.x+plot_rect.width);
					p.stroke(area_colors[prev_type]);
					p.line(prev_dx, dy, new_dx, dy);
				}
			}else{
				//different position
				Grid grid = (Grid) grid_map.get(px+"_"+py);
				if(grid == null){
					println("Error: grid is null: "+px+"_"+py +" index ="+i);
				}else{
					String type = grid.type;
					int type_index = area_type_array.indexOf(type);
					float  new_dx = map(i, 0, x_pos.length, plot_rect.x, plot_rect.x+plot_rect.width);
					int opacity = 255;
					if(prev_x == -1){
						//first time
						prev_dx = new_dx;
						prev_type = type_index;						
					}else if(px == -1){
						//last time
						p.stroke(area_colors[prev_type],opacity);
						p.line(prev_dx, dy, new_dx, dy);
						prev_dx = new_dx;
						prev_type = type_index;
						// println("px == -1 :"+i);

					}else{
						//check if same type
						if(prev_type == type_index){
							//move on
							//unless its last move
							if(i == last_index){
								p.stroke(area_colors[prev_type],opacity);
								p.line(prev_dx, dy, new_dx, dy);
							}
						}else{
							//draw
							p.stroke(area_colors[prev_type],opacity);
							p.line(prev_dx, dy, new_dx, dy);
							prev_dx = new_dx;
							prev_type = type_index;
							if(i == last_index){
								p.stroke(area_colors[prev_type],opacity);
								p.line(prev_dx, dy, new_dx, dy);
							}
						}
					}
				}
				prev_x = px;
				prev_y = py;
			}
		}

		//debug
		// float last_dx = map(last_index, 0, x_pos.length, plot_rect.x, plot_rect.x+plot_rect.width);	
		// p.stroke(0);
		// p.strokeCap(PROJECT);
		// p.point(last_dx, dy);
	}

	void render_locations(PGraphics p){
		int prev_x = -1;
		int prev_y = -1;
		float prev_dx = plot_rect.x;
		int prev_type = -1;
		boolean prevIsLocation = false;

		p.strokeCap(SQUARE);
		p.strokeWeight(_line_weight);
		// for(int i = first_index; i <= last_index; i++){
		// for(int i = 0; i < x_pos.length; i++){
		for(int i = 0; i <= last_index; i++){
			int px = x_pos[i];
			int py = y_pos[i];
			if(px == prev_x && py == prev_y){
				//same position as before
				if(i == last_index && prevIsLocation){
					float new_dx = map(i, 0, x_pos.length, plot_rect.x, plot_rect.x+plot_rect.width);
					p.stroke(area_colors[prev_type]);
					p.line(prev_dx, dy, new_dx, dy);
				}
			}else{
				//different position
				Grid grid = (Grid) grid_map.get(px+"_"+py);
				boolean isLocation = (unique_location_id.contains(grid.id)? true:false);

				if(grid == null){
					println("Error: grid is null: "+px+"_"+py +" index ="+i);
				}else{
					String type = grid.type;
					int type_index = area_type_array.indexOf(type);
					float  new_dx = map(i, 0, x_pos.length, plot_rect.x, plot_rect.x+plot_rect.width);
					int opacity = 255;

					if(prev_x == -1){
						//first time
						prev_dx = new_dx;
						prev_type = type_index;						
					}else if(px == -1 && prevIsLocation){
						//last time
						p.stroke(area_colors[prev_type],opacity);
						p.line(prev_dx, dy, new_dx, dy);
						prev_dx = new_dx;
						prev_type = type_index;
						// println("px == -1 :"+i);

					}else{
						//check if same type
						if(prev_type == type_index){
							//move on
							//unless its last move
							if(i == last_index && prevIsLocation){
								p.stroke(area_colors[prev_type],opacity);
								p.line(prev_dx, dy, new_dx, dy);
							}
						}else{
							//draw
							if(prevIsLocation){
								p.stroke(area_colors[prev_type],opacity);
								p.line(prev_dx, dy, new_dx, dy);
							}
							prev_dx = new_dx;
							prev_type = type_index;
							if(i == last_index){
								p.stroke(area_colors[prev_type],opacity);
								p.line(prev_dx, dy, new_dx, dy);
							}
						}
					}
				}
				prev_x = px;
				prev_y = py;
				prevIsLocation = isLocation;
			}
		}

		//debug
		// float last_dx = map(last_index, 0, x_pos.length, plot_rect.x, plot_rect.x+plot_rect.width);	
		// p.stroke(0);
		// p.strokeCap(PROJECT);
		// p.point(last_dx, dy);
	}

	

	void render_only_selected_location(PGraphics p){
		int prev_x = -1;
		int prev_y = -1;
		float prev_dx = plot_rect.x;
		int prev_type = -1;

		p.strokeCap(SQUARE);
		p.strokeWeight(_line_weight);
		// for(int i = first_index; i <= last_index; i++){
		// for(int i = 0; i < x_pos.length; i++){
		for(int i = 0; i <= last_index; i++){
			int px = x_pos[i];
			int py = y_pos[i];
			if(px == prev_x && py == prev_y){
				//same position as before
			}else{
				//different position
				Grid grid = (Grid) grid_map.get(px+"_"+py);
				if(grid == null){
					println("Error: grid is null: "+px+"_"+py +" index ="+i);
				}else{
					String type = grid.type;
					int type_index = area_type_array.indexOf(type);
					float  new_dx = map(i, 0, x_pos.length, plot_rect.x, plot_rect.x+plot_rect.width);
					
					int opacity = (selected_location_id.contains(grid.id)?255: 0);

					if(prev_x == -1){
						//first time
						prev_dx = new_dx;
						prev_type = type_index;						
					}else if(px == -1){
						//last time
						p.stroke(area_colors[prev_type],opacity);
						p.line(prev_dx, dy, new_dx, dy);
						prev_dx = new_dx;
						prev_type = type_index;
						// println("px == -1 :"+i);

					}else{
						//check if same type
						if(prev_type == type_index){
							//move on
							//unless its last move
							if(i == last_index){
								p.stroke(area_colors[prev_type],opacity);
								p.line(prev_dx, dy, new_dx, dy);
							}
						}else{
							//draw
							p.stroke(area_colors[prev_type],opacity);
							p.line(prev_dx, dy, new_dx, dy);
							prev_dx = new_dx;
							prev_type = type_index;
						}
					}
				}
				prev_x = px;
				prev_y = py;
			}
		}
	}

	void render_custom_grid(PGraphics p){
		int prev_x = -1;
		int prev_y = -1;
		float prev_dx = plot_rect.x;
		int prev_type = -1;

		p.strokeCap(SQUARE);
		// p.strokeCap(PROJECT);
		p.strokeWeight(_line_weight);
		// for(int i = first_index; i <= last_index; i++){
		// for(int i = 0; i < x_pos.length; i++){
		for(int i = 0; i <= last_index; i++){
			int px = x_pos[i];
			int py = y_pos[i];
			if(px == prev_x && py == prev_y){
				//same position as before
				if(i == last_index){
					float new_dx = map(i, 0, x_pos.length, plot_rect.x, plot_rect.x+plot_rect.width);
					p.stroke(color_array[prev_type]);
					p.line(prev_dx, dy, new_dx, dy);
				}
			}else{
				//different position
				Grid grid = (Grid) grid_map.get(px+"_"+py);
				if(grid == null){
					println("Error: grid is null: "+px+"_"+py +" index ="+i);
				}else{
					String type = grid.custom_type; /////////////////
					int type_index = Integer.parseInt(type);
					float new_dx = map(i, 0, x_pos.length, plot_rect.x, plot_rect.x+plot_rect.width);
					int opacity = 255;
					// int opacity = (selected_location_id.contains(grid.id)?255: 0);

					if(prev_x == -1){
						//first time
						prev_dx = new_dx;
						prev_type = type_index;		
					}else if(px == -1){
						//this never happens
						println("ERROR: px == -1");

					}else{
						//check if same type
						if(prev_type == type_index){
							//move on
							//unless its last move
							if(i == last_index){
								p.stroke(color_array[prev_type],opacity);
								p.line(prev_dx, dy, new_dx, dy);
								// p.line(prev_dx, dy, new_dx+20, dy);
							}
						}else{
							//draw
							p.stroke(color_array[prev_type],opacity);
							p.line(prev_dx, dy, new_dx, dy);
							prev_dx = new_dx;
							prev_type = type_index;

							if(i == last_index){
								p.stroke(color_array[prev_type],opacity);
								p.line(new_dx, dy, new_dx+1, dy);
							}
						}
					}
				}
				prev_x = px;
				prev_y = py;
			}
		}
	}
	


	void update_order_by_area(int t){
		int px = x_pos[t];
		int py = y_pos[t];
		Grid grid = (Grid) grid_map.get(px+"_"+py);
		int type_index = -1;
		if(grid == null){
			// println("Error: grid is null: "+px+"_"+py +" index ="+id);
		}else{
			if(current_grid == ORIGINAL_GRID){
				String type = grid.type;
			 	type_index = area_type_array.indexOf(type);
			}else if(current_grid == CUSTOM_GRID){
				type_index = Integer.parseInt(grid.custom_type);
			}
		}
		order_index = type_index;
	}
}
