class PersonComparator implements Comparator<Person>{
	int type = 0;

	PersonComparator(int type){
		this.type = type;
	}


	@Override
	public int compare(Person a, Person b){	
		if(type == SORT_FIRST){
			if(a.first_index > b.first_index){
				return 1;
			}else if(a.first_index < b.first_index){
				return -1;
			}
		}else if(type == SORT_LAST){
			if(a.last_index > b.last_index){
				return 1;
			}else if(a.last_index < b.last_index){
				return -1;
			}
		}else if(type == ORDER_INDEX){
			if(a.order_index > b.order_index){
				return 1;
			}else if(a.order_index < b.order_index){
				return -1;
			}
		}

		return 0;
	}	
}
