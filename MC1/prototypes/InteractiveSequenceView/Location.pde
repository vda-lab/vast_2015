class Location{
	int index = -1;
	String name = "";
	String type = "" ;
	String area = "";
	String id;
	int x, y;


	//display position
	int dy;


	Location(String id, int index, int x, int y, String name, String type, String area){
		this.id = id;
		this.index = index;
		this.x = x;
		this.y = y;
		this.name = name;
		this.type = type;
		this.area = area;
	}
}
