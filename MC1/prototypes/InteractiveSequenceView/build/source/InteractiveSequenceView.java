import processing.core.*; 
import processing.data.*; 
import processing.event.*; 
import processing.opengl.*; 

import java.io.FileInputStream; 
import java.io.BufferedReader; 
import java.io.InputStreamReader; 
import java.util.*; 
import java.awt.*; 
import java.awt.event.*; 
import java.text.*; 
import weka.core.*; 
import weka.clusterers.AbstractClusterer; 
import weka.clusterers.HierarchicalClusterer; 

import weka.gui.explorer.*; 
import weka.experiment.*; 
import weka.classifiers.pmml.consumer.*; 
import weka.filters.supervised.instance.*; 
import weka.*; 
import weka.classifiers.bayes.net.*; 
import weka.core.pmml.jaxbbindings.*; 
import weka.gui.sql.*; 
import weka.core.neighboursearch.kdtrees.*; 
import weka.experiment.xml.*; 
import weka.gui.scripting.*; 
import weka.core.*; 
import weka.classifiers.bayes.net.search.global.*; 
import org.bounce.*; 
import weka.classifiers.bayes.net.search.*; 
import weka.filters.unsupervised.instance.subsetbyexpression.*; 
import weka.datagenerators.classifiers.regression.*; 
import weka.classifiers.trees.*; 
import weka.gui.beans.xml.*; 
import weka.classifiers.rules.part.*; 
import weka.core.logging.*; 
import weka.gui.treevisualizer.*; 
import weka.classifiers.xml.*; 
import weka.gui.scripting.event.*; 
import weka.gui.arffviewer.*; 
import weka.core.xml.*; 
import weka.classifiers.functions.neural.*; 
import weka.filters.supervised.attribute.*; 
import weka.classifiers.evaluation.output.prediction.*; 
import weka.classifiers.meta.*; 
import weka.classifiers.lazy.*; 
import weka.gui.streams.*; 
import weka.classifiers.functions.supportVector.*; 
import weka.classifiers.misc.*; 
import weka.attributeSelection.*; 
import org.pentaho.packageManagement.*; 
import weka.datagenerators.*; 
import weka.gui.*; 
import weka.gui.hierarchyvisualizer.*; 
import weka.classifiers.rules.*; 
import weka.core.pmml.*; 
import weka.classifiers.trees.j48.*; 
import weka.core.neighboursearch.balltrees.*; 
import weka.datagenerators.classifiers.classification.*; 
import weka.gui.filters.*; 
import weka.core.tokenizers.*; 
import weka.associations.*; 
import weka.classifiers.trees.m5.*; 
import weka.classifiers.bayes.*; 
import weka.classifiers.bayes.net.search.fixed.*; 
import weka.core.neighboursearch.*; 
import weka.core.stemmers.*; 
import weka.core.scripting.*; 
import weka.gui.visualize.plugins.*; 
import weka.core.neighboursearch.covertrees.*; 
import weka.core.converters.*; 
import weka.gui.beans.*; 
import weka.classifiers.evaluation.*; 
import weka.datagenerators.clusterers.*; 
import weka.classifiers.bayes.net.estimate.*; 
import weka.classifiers.*; 
import org.bounce.net.*; 
import weka.core.mathematicalexpression.*; 
import weka.gui.visualize.*; 
import weka.filters.unsupervised.attribute.*; 
import weka.core.matrix.*; 
import weka.clusterers.*; 
import weka.gui.experiment.*; 
import weka.core.json.*; 
import weka.estimators.*; 
import weka.classifiers.functions.*; 
import weka.gui.sql.event.*; 
import weka.classifiers.trees.lmt.*; 
import weka.classifiers.bayes.net.search.local.*; 
import weka.classifiers.pmml.producer.*; 
import weka.gui.graphvisualizer.*; 
import weka.classifiers.lazy.kstar.*; 
import weka.filters.unsupervised.instance.*; 
import weka.gui.boundaryvisualizer.*; 
import weka.core.stopwords.*; 
import weka.filters.*; 
import java_cup.runtime.*; 
import weka.classifiers.bayes.net.search.ci.*; 
import weka.classifiers.trees.ht.*; 

import java.util.HashMap; 
import java.util.ArrayList; 
import java.io.File; 
import java.io.BufferedReader; 
import java.io.PrintWriter; 
import java.io.InputStream; 
import java.io.OutputStream; 
import java.io.IOException; 

public class InteractiveSequenceView extends PApplet {













ArrayList<Location> location_array;
HashMap<String, Location> location_map;
HashSet<String> unique_location_id;
HashSet<String> selected_location_id;


ArrayList<Grid> grid_array;
HashMap<String, Grid> grid_map;


ArrayList<Person> person_array;
HashMap<Integer, Person> person_map;
Date min_time, max_time;
long start_m_sec, end_m_sec;  //milliseconds
int min_sec, max_sec;			// min and max in seconds
int time_interval_count  = 0; 	// number of minutes
int time_interval = 0; 			// number of seconds per interval


PGraphics pg;

int _margin = 10;
int _left_margin = _margin*3;
int _right_margin = _margin*3;
int _top_margin = _margin*3;
int _bottom_margin = _margin*3;
int _menu_bar = _margin*5;
float _line_weight  = 1.0f;

int _plot_width, _plot_height;
Rectangle plot_rect;

//CPU optimization
int draw_counter = 0;
int draw_max = 30; //1 seconds = draw 60 times before it stops


int c_light_blue = color(236,231,242);
int c_movement = color(0,0,0,50);

PFont font;

boolean showGreyLines = false;


String[] area_types = {"Entry Corridor", "Wet Land","Kiddie Land","Coarster Alley","Tundra Land"};
ArrayList<String> area_type_array;
int[] area_colors= {color(190,186,218), color(141,211,199), color(255,255,179), color(251,128,114), color(128,177,211)}; 

//custom grid
int[] color_array = {color(228,26,28),color(55,126,184),color(77,175,74),color(152,78,163),color(255,127,0),color(255,255,51),color(166,86,40),color(247,129,191),color(153,153,153)};



SimpleDateFormat ft = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
SimpleDateFormat hourFormat = new SimpleDateFormat("HH:mm");
SimpleDateFormat weekFormat = new SimpleDateFormat("EEEE, MMMM dd yyyy");

static final int SORT_FIRST = 0;
static final int SORT_LAST = 1;
static final int ORDER_INDEX = 2;

static final int ORIGINAL_GRID = 0;
static final int CUSTOM_GRID = 1;	

//weka stufff
WekaData mydata;
WekaHierarchicalClusterer clusterer;


//interaction
int hovering_time_index = -1;
Date hovering_now = null;


//Current status
int current_grid = CUSTOM_GRID;
// int current_grid = ORIGINAL_GRID;


public void setup(){
	person_map = new HashMap<Integer, Person>();
	person_array = new ArrayList<Person>();
	selected_location_id = new HashSet<String>();

	//test
	selected_location_id.add("32_33");

	load_matrix_x("/Users/Ryo/Desktop/PhD/Year_4/22_VAST_2015/Derived_Data/x_matrix_Fri.csv");
	load_matrix_y("/Users/Ryo/Desktop/PhD/Year_4/22_VAST_2015/Derived_Data/y_matrix_Fri.csv");

	// load_matrix_x("/Users/Ryo/Desktop/PhD/Year_4/22_VAST_2015/Derived_Data/x_matrix_Sat.csv");
	// load_matrix_y("/Users/Ryo/Desktop/PhD/Year_4/22_VAST_2015/Derived_Data/y_matrix_Sat.csv");
	// load_matrix_x("/Users/Ryo/Desktop/PhD/Year_4/22_VAST_2015/Derived_Data/x_matrix_Sun.csv");
	// load_matrix_y("/Users/Ryo/Desktop/PhD/Year_4/22_VAST_2015/Derived_Data/y_matrix_Sun.csv");


	//load_grid
	load_grid("park_grids_area.csv");
	load_custom_grid("/Users/Ryo/Desktop/PhD/Year_4/22_VAST_2015/Derived_Data/custom_grid_1.csv");

	//load locations
	load_location("park_locations.csv");

	area_type_array = new ArrayList<String>(Arrays.asList(area_types));

	//setup stage
	int stage_width = displayWidth;
	_plot_width = stage_width - _left_margin - _right_margin;
	int stage_height = displayHeight  - _menu_bar;
	_plot_height = stage_height - _top_margin - _bottom_margin;

	size(stage_width, stage_height);
	plot_rect = new Rectangle(_left_margin, _top_margin, _plot_width, _plot_height);

	//debug
	float line_y_gap = (float)_plot_height / (float)person_array.size();
	// println("Debug: line_y_gap ="+line_y_gap +"  height = "+_plot_height+"  people="+person_array.size());
	_line_weight = 0.5f;


	font = createFont("SUPERNAT1001.TTF", 10);
	textFont(font, 10);


	//sort Person
	Collections.sort(person_array, new PersonComparator(SORT_FIRST));
	// Collections.sort(person_array, new PersonComparator(SORT_LAST));
	//set dy
	set_y_pos();


	//set data
	// mydata = getData(0, person_array.size()-1, 150, 151);

	//debug
	// for(int i =0; i<mydata.myInstances.numInstances(); i++){
	// 	println("ID: " + mydata.myInstances.instance(i).value(0));
	// 	println("\t" + mydata.myInstances.instance(i).toString());
	// }
	// clusterer = new WekaHierarchicalClusterer();
	//run clustering
	// String output = clusterer.clusterDataGetOrder(mydata);
	// String[] lines = split(output, "\n");
	// ArrayList<String> result = parseNewwick(lines[1].trim());

	//assign new index
	// ArrayList<Person> unselected = new ArrayList<Person>(person_array.subList(102, person_array.size()));

	// int runningScore = 0;
	// for(String id:result){
	// 	Person person = (Person) person_map.get(new Integer(id));
	// 	if(person  == null){
	// 		println("Error: person not found:"+id);
	// 	}else{
	// 		person.order_index = runningScore;
	// 		runningScore ++;
	// 	}
	// }
	// for(Person p:unselected){
	// 	p.order_index = runningScore;
	// 	runningScore ++;
	// }

	// Collections.sort(person_array, new PersonComparator(ORDER_INDEX));
	// set_y_pos();
	// println("Clustering Result! "+result);

	//debug
	// println("Distance Function Tip:");
	// println(((HierarchicalClusterer)clusterer.myClusterer).distanceFunctionTipText());
	// println("Branch height Tip:");
	// println(((HierarchicalClusterer)clusterer.myClusterer).distanceIsBranchLengthTipText());
	// println("Newick Tip:");
	// println(((HierarchicalClusterer)clusterer.myClusterer).printNewickTipText());
	//render 
	pg = createGraphics(stage_width, stage_height);
	render_plot(pg);

	// exit();
	noLoop();
}	

public void draw(){
	background(230);

	if(pg!= null){
		image(pg, 0,0);
	}

	draw_interaction();

	if(draw_counter > draw_max){
		println(" --------- ");
		noLoop();
		draw_counter = 0;
	}else{
		draw_counter ++;
	}
}


public void draw_interaction(){
	//hovering time
	if(hovering_time_index != -1){
		//get Date
		int seconds = min_sec+hovering_time_index*time_interval;
		// Date now = new Date((long)(seconds)*(1000l));
		String time_label  = hourFormat.format(hovering_now);
		float label_width = textWidth(time_label) + _margin;
		float dx = map(seconds, min_sec, max_sec, plot_rect.x, plot_rect.x+plot_rect.width);

		if(mousePressed){
			if(m_pressed!=null && m_dragged != null){
				//line
				strokeWeight(1);
				stroke(0);
				line(dx, m_pressed.y, dx, m_dragged.y);
				line(dx - 2, m_pressed.y, dx+2, m_pressed.y);
				line(dx - 2, m_dragged.y, dx+2, m_dragged.y);
			}
		}else{
			//background
			fill(230, 230);
			noStroke();
			rect(dx - (label_width/2), plot_rect.y -15, label_width, 15);
			textAlign(CENTER, BOTTOM);
			fill(0);
			text(time_label, dx, plot_rect.y-2);
			//line
			strokeWeight(1);
			stroke(0);
			line(dx, plot_rect.y, dx, plot_rect.y+plot_rect.height);
		}
	}
}

//grid
public void load_grid(String url){
	grid_array = new ArrayList<Grid>();
	grid_map = new HashMap<String, Grid>();
	HashSet<String> unique_type = new HashSet<String>();

	String[] lines = loadStrings(url);
	for(String l: lines){
		if(l.startsWith("id")){
			//header
		}else{
			String[] s = split(l, ",");
			String id = s[0];
			int gx = Integer.parseInt(s[1]);
			int gy = Integer.parseInt(s[2]);
			String area_type = s[3].trim();

			Grid g = new Grid(id, gx, gy, area_type);
			grid_array.add(g);
			grid_map.put(id, g);
			unique_type.add(area_type);
		}
	}

	println("Debug:total number of girds ="+grid_array.size());
	println("Debug:unique type ="+unique_type);
}

public void load_custom_grid(String url){
	String[] lines = loadStrings(url);
	for(String l: lines){
		if(l.startsWith("id")){
			//header
		}else{
			String[] s = split(l, ",");
			String id = s[0];
			int gx = Integer.parseInt(s[1]);
			int gy = Integer.parseInt(s[2]);
			String area_type = s[3].trim();

			Grid g = (Grid) grid_map.get(id);
			if(g == null){
				println("Error: custom grid: cannot find:"+id);
			}else{
				g.custom_type = area_type;
			}
		}
	}
	//debug
	// for(Grid g: grid_array){
	// 	if(g.custom_type == ""){
	// 		println("Error: missing type: "+g.id);
	// 	}
	// }
}


public void load_location(String url){
	location_array = new ArrayList<Location>();
	location_map = new HashMap<String, Location>();
	unique_location_id = new HashSet<String>();
	String[] lines = loadStrings(url);
	for(String l : lines){
		if(l.startsWith("id")){
			//header
		}else{
			String[] s = split(l, ",");
			int index = Integer.parseInt(s[0]);
			String name = s[1].trim();
			String type = s[2].trim();
			String area = s[3].trim();
			int lx = Integer.parseInt(s[4]);
			int ly = Integer.parseInt(s[5]);

			String label = lx+"_"+ly;
			Location loc = new Location(label, index, lx, ly, name, type, area);
			location_array.add(loc);
			location_map.put(label, loc);
			unique_location_id.add(label);
		}
	}
	println("Debug:"+location_array.size()+" locations loaded");
}

public void load_matrix_x(String url){
	int counter = 0;
	File file = new File(url);
	int array_size = 0;
	try {
		FileInputStream fstream = new FileInputStream(file);
		BufferedReader br = new BufferedReader(new InputStreamReader(fstream));
		String line;
		//Read File Line By Line
		while ((line = br.readLine()) != null){
			if(counter%100000 == 0){
				println("loading data:"+counter);
			}
			if(counter == 0){
				println("header:"+line);
				String header = line.substring(1);
				String[] s = split(header,",");
				int start_sec = Integer.parseInt(s[0]);
				int end_sec = Integer.parseInt(s[1]);
				int interval = Integer.parseInt(s[2]);
				array_size = (end_sec - start_sec)/interval;
				println("array_size ="+array_size);
				time_interval_count = array_size;
				time_interval = interval;

				if(min_time == null){
					min_time = new Date((long)start_sec * 1000l);
					max_time = new Date((long)end_sec * 1000l);
					min_sec = start_sec;
					max_sec = end_sec;

					println("Debug: start:\t"+min_time);
					println("Debug: end:\t"+max_time);
				}
			}else{
				String[] s = split(line, ",");
				int id = Integer.parseInt(s[0]);
				int[] x_pos = new int[array_size];
				for(int i = 1; i<s.length; i++){
					x_pos[i-1] = Integer.parseInt(s[i]);
				}
				//find Person
				Person p = (Person) person_map.get(new Integer(id));
				if(p == null){
					p = new Person(id);
					person_array.add(p);
					person_map.put(new Integer(id), p);
				}
				p.x_pos = x_pos;
				//find first and the last index
				for(int i = 0; i<p.x_pos.length; i++){
					if(p.x_pos[i] != -1){
						p.first_index = i;
						break;
					}
				}
				for(int i = p.x_pos.length-1; i >= 0; i--){
					if(p.x_pos[i] != -1){
						p.last_index = i;
						break;
					}
				}
				// println("Debug: "+id+" first:"+p.first_index+" last:"+p.last_index);

			}
		  	counter ++;
		}
		//Close the input stream
		br.close();
	}catch(Exception e){
		e.printStackTrace();
	}
	println("Debug: total number of people:"+person_array.size());
}

public void load_matrix_y(String url){
	int counter = 0;
	File file = new File(url);
	int array_size = 0;
	try {
		FileInputStream fstream = new FileInputStream(file);
		BufferedReader br = new BufferedReader(new InputStreamReader(fstream));
		String line;
		//Read File Line By Line
		while ((line = br.readLine()) != null){
			if(counter%100000 == 0){
				println("loading data:"+counter);
			}
			if(counter == 0){
				println("header:"+line);
				String header = line.substring(1);
				String[] s = split(header,",");
				int start_sec = Integer.parseInt(s[0]);
				int end_sec = Integer.parseInt(s[1]);
				int interval = Integer.parseInt(s[2]);
				array_size = (end_sec - start_sec)/interval;
				println("array_size ="+array_size);

				if(min_time == null){
					min_time = new Date((long)start_sec * 1000l);
					max_time = new Date((long)end_sec * 1000l);

					println("Debug: start:\t"+min_time);
					println("Debug: end:\t"+max_time);
				}
			}else{
				String[] s = split(line, ",");
				int id = Integer.parseInt(s[0]);
				int[] y_pos = new int[array_size];
				for(int i = 1; i<s.length; i++){
					y_pos[i-1] = Integer.parseInt(s[i]);
				}
				//find Person
				Person p = (Person) person_map.get(new Integer(id));
				if(p == null){
					p = new Person(id);
					person_array.add(p);
					person_map.put(new Integer(id), p);
				}
				p.y_pos = y_pos;
				// println("Debug: "+id+" first:"+p.first_index+" last:"+p.last_index);
			}
		  	counter ++;
		}
		//Close the input stream
		br.close();
	}catch(Exception e){
		e.printStackTrace();
	}
	println("Debug: total number of people:"+person_array.size());
}

public void set_y_pos(){
	for(int i = 0; i<person_array.size(); i++){
		Person p = person_array.get(i);
		p.dy = map(i, 0, person_array.size()-1, plot_rect.y, plot_rect.y+plot_rect.height);
	}
}


//both inclusive
public WekaData getData(int p_from, int p_to, int t_from, int t_to){
	int row_length = 1 + (t_to - t_from +1)*2;
	int row_count = p_to - p_from +1;
	WekaData data = new WekaData(row_length, row_count);
	//add attribute 
	data.AddStringAttribute("id");
	for(int i = t_from; i <= t_to; i++){
		data.AddAttribute("x"+i);
		data.AddAttribute("y"+i);
	}
	//add data point
	for(int i = p_from; i <= p_to; i++){
		Person p = person_array.get(i);
		Object[]row = new Object[row_length];
		row[0] = ""+p.id;
		int runningIndex = 1;
		for(int j = t_from; j <= t_to; j++){
			row[runningIndex] = p.x_pos[j];
			runningIndex++;
			row[runningIndex] = p.y_pos[j];
			runningIndex++;
		}
		Object[][] tableRow = new Object[][]{row};
		data.InsertData(tableRow);
	}
	return data;
}



public void render_plot(PGraphics p){
	println("*** start rendering ***");
	p.beginDraw();
	p.textFont(font, 10);
	p.fill(255);
	p.noStroke();
	p.rect(plot_rect.x, plot_rect.y, plot_rect.width, plot_rect.height);

	//day of the week
	p.fill(0);
	p.textAlign(LEFT, TOP);
	p.text(weekFormat.format(min_time), _left_margin, 5);

	//draw time intervals
	for(Date d = new Date(min_time.getTime()); d.before(max_time); d = increment_minute(d, 30)){
		int seconds = getSeconds(d);
		String label = hourFormat.format(d);
		float dx = map(seconds, min_sec, max_sec, plot_rect.x, plot_rect.x+plot_rect.width);
		//label
		p.fill(0);
		p.textAlign(CENTER, BOTTOM);
		p.text(label, dx, plot_rect.y -2);
		p.textAlign(CENTER, TOP);
		p.text(label, dx, plot_rect.y + _plot_height);
		//line
		p.stroke(230);
		p.strokeWeight(1);
		p.noFill();
		p.line(dx, plot_rect.y-2, dx, plot_rect.y+plot_rect.height);
	}
	//draw person
	for(int i = 0; i<person_array.size(); i++){
		Person per = person_array.get(i);
		if(current_grid == ORIGINAL_GRID){
			per.render(p);
		}else if(current_grid == CUSTOM_GRID){
			per.render_custom_grid(p);
		}

		// per.render_locations(p);


		// println("Debug:last:"+per.last_index);
	}

	p.endDraw();

	println("*** end rendering ***");
}




/// utility
//// Date
public int getYear(Date d){
	Calendar cal = GregorianCalendar.getInstance();
	cal.setTime(d);
	return cal.get(Calendar.YEAR);
}
public int getMonth(Date d){
	Calendar cal = Calendar.getInstance();
	cal.setTime(d);
	return cal.get(Calendar.MONTH);
}
public int getDay(Date d){
	Calendar cal = Calendar.getInstance();
	cal.setTime(d);
	return cal.get(Calendar.DAY_OF_MONTH);
}
public int getHour(Date d){
	Calendar cal = GregorianCalendar.getInstance();
	cal.setTime(d);
	return cal.get(Calendar.HOUR_OF_DAY);
}
public int getMin(Date d){
	Calendar cal = GregorianCalendar.getInstance();
	cal.setTime(d);
	return cal.get(Calendar.MINUTE);
}
public int getSec(Date d){
	Calendar cal = GregorianCalendar.getInstance();
	cal.setTime(d);
	return cal.get(Calendar.SECOND);
}
public int getSeconds(Date d){
	// return (int)(d.getTime()/1000);
	long l = d.getTime();
	int result = (int) (l/1000l);
	return result;
}
public Date increment_sec(Date d){
	long t = d.getTime(); //millisecond
	t =  t+1000;
	return new Date(t);
}
public Date increment_sec(Date d, int n){
	long t = d.getTime(); //millisecond
	t =  t+1000*n;
	return new Date(t);
}
public Date increment_minute(Date d, int m){
	long t = d.getTime();
	t = t+ (1000l)*m*60l;
	return new Date(t);
}

public float log10 (float x) {
  return (log(x) / log(10));
}
public ArrayList<String> parseNewwick (String s){
	// println(" --- parseNewick() "+s);
	ArrayList<String> result = new ArrayList<String>();
	StringTokenizer st = new StringTokenizer(s, "(,)", true);
	String token = st.nextToken();
	if (token.equals("(")) {
		// Inner node
		ArrayList<String> left = parseNewick(st);
		String comma = st.nextToken();
		ArrayList<String> right = parseNewick(st);	
		String close = st.nextToken();
		String label = "";
		if(st.hasMoreElements()){
			label = st.nextToken();
			String[] pieces = label.split(":");
			String piece = pieces[0].trim();
			if(!piece.equals("")){
				result.add(piece);
			}
		}
		if(left.size()> 0){
			result.addAll(left); 
		}
		if(right.size()> 0){
			result.addAll(right); 
		}
		return result;
	}
	else {
		// Leaf
		String[] pieces = token.split(":");
		String piece = pieces[0].trim();
		if(!piece.equals("")){
			result.add(piece);
		}
		return  result;
	}
}

public ArrayList<String> parseNewick(StringTokenizer st) {
	// println("parseNewick() -- token "+st);
	String token = st.nextToken();
	ArrayList<String> result = new ArrayList<String>();
	if (token.equals("(")) {
		// Inner node
		ArrayList<String> left = parseNewick(st);
		String comma = st.nextToken();
		ArrayList<String> right = parseNewick(st);	
		String close = st.nextToken();
		String label = "";
		if(st.hasMoreElements()){
			label = st.nextToken();
			String[] pieces = label.split(":");
			String piece = pieces[0].trim();
			if(!piece.equals("")){
				result.add(piece);
			}
		}
		if(left.size()> 0){
			result.addAll(left); 
		}
		if(right.size()> 0){
			result.addAll(right); 
		}
		return result;
	}
	else {
		// Leaf
		String[] pieces = token.split(":");
		String piece = pieces[0].trim();
		if(!piece.equals("")){
			result.add(piece);
		}
		return  result;
	}
}
PVector m_pressed = null;
PVector m_released = null;
PVector m_dragged = null;


public void keyPressed(){
	if(key == '1'){
		//sort by beginning
		Collections.sort(person_array, new PersonComparator(SORT_FIRST));
		set_y_pos();
		pg.clear();
		render_plot(pg);
		loop();
	}else if(key == '2'){
		//sort by exit
		Collections.sort(person_array, new PersonComparator(SORT_LAST));
		set_y_pos();
		pg.clear();
		render_plot(pg);
		loop();
	}else if(key == '3'){
		//sort by entrace point then time
		//update person order index
		for(int i = 0; i < person_array.size(); i++){
			//check the first area type
			Person p = person_array.get(i);
			p.update_order_by_area(p.first_index);
		}
		Collections.sort(person_array, new PersonComparator(SORT_FIRST));
		Collections.sort(person_array, new PersonComparator(ORDER_INDEX));
		set_y_pos();
		pg.clear();
		render_plot(pg);
		loop();
	}else if(key == '4'){
		//sort by exit time then exit point area
		//update person order index
		for(int i = 0; i < person_array.size(); i++){
			//check the first area type
			Person p = person_array.get(i);
			p.update_order_by_area(p.last_index);
		}
		Collections.sort(person_array, new PersonComparator(SORT_LAST));
		Collections.sort(person_array, new PersonComparator(ORDER_INDEX));
		set_y_pos();
		pg.clear();
		render_plot(pg);
		loop();
	}else if(key == '5'){
		if(hovering_time_index != -1){
			//sort by area at that particular time of the day
			//update person order index
			for(int i = 0; i < person_array.size(); i++){
				//check the first area type
				Person p = person_array.get(i);
				p.update_order_by_area(hovering_time_index);
			}
			Collections.sort(person_array, new PersonComparator(ORDER_INDEX));
			set_y_pos();
			pg.clear();
			render_plot(pg);
			loop();
		}
	}else if(key == 'g'){
		//swap grid
		if(current_grid == ORIGINAL_GRID){
			current_grid = CUSTOM_GRID;
		}else if(current_grid == CUSTOM_GRID){
			current_grid = ORIGINAL_GRID;
		}
		pg.clear();
		render_plot(pg);
		loop();
	}
}

public void mouseMoved(){
	cursor(ARROW);
	hovering_time_index = -1;
	hovering_now = null;

	if(plot_rect.contains(mouseX, mouseY)){
		cursor(CROSS);
		if(mousePressed){
			//hovering time index
			hovering_time_index = constrain(round(map(m_pressed.x, plot_rect.x, plot_rect.x+plot_rect.width, 0, time_interval_count)), 0 , time_interval_count-1);
		}else{
			//hovering time index
			hovering_time_index = constrain(round(map(mouseX, plot_rect.x, plot_rect.x+plot_rect.width, 0, time_interval_count)), 0 , time_interval_count-1);
		}
		int seconds = min_sec+hovering_time_index*time_interval;
		hovering_now = new Date((long)(seconds)*(1000l));
	}
	loop();
}

public void mousePressed(){
	m_pressed = new PVector(mouseX, mouseY);
	m_released = null;
	m_dragged = null;
	hovering_time_index = constrain(round(map(m_pressed.x, plot_rect.x, plot_rect.x+plot_rect.width, 0, time_interval_count)), 0 , time_interval_count-1);
	int seconds = min_sec+hovering_time_index*time_interval;
	hovering_now = new Date((long)(seconds)*(1000l));
	loop();
}

public void mouseDragged(){
	m_dragged = new PVector(mouseX, mouseY);
	loop();
}

public void mouseReleased(){
	m_released = new PVector(mouseX, mouseY);

	
	if(m_dragged != null && m_pressed != null){
		float y_from = m_pressed.y;
		float y_to = m_released.y;
		if(m_pressed.y > m_released.y){
			y_from = m_released.y;
			y_to = m_pressed.y;
		}
		ArrayList<Person> unaffected_top = new ArrayList<Person>();
		ArrayList<Person> affected = null;
		ArrayList<Person> unaffected_bottom = null; 

		//adjust order index for the rage
		for(int i = 0; i < person_array.size(); i++){
			Person p = person_array.get(i);
			if(unaffected_bottom == null){
				if(affected == null){
					if(y_from <= p.dy && p.dy <= y_to){
						//in the range
						affected = new ArrayList<Person>();
						p.update_order_by_area(hovering_time_index);
						affected.add(p);
					}else{
						unaffected_top.add(p);
					}
				}else{
					if(y_from <= p.dy && p.dy <= y_to){
						//in the range
						p.update_order_by_area(hovering_time_index);
						affected.add(p);
					}else{
						unaffected_bottom = new ArrayList<Person>();
						unaffected_bottom.add(p);
					}
				}
			}else{
				unaffected_bottom.add(p);
			}
		}
		if(affected == null){
			affected = new ArrayList<Person>();
		}
		if(unaffected_bottom == null){
			unaffected_bottom = new ArrayList<Person>();
		}
		println("Debug: top = "+unaffected_top.size()+"\taffected = "+affected.size()+ "\tbottom = "+unaffected_bottom.size());

		//sort affected
		Collections.sort(affected, new PersonComparator(ORDER_INDEX));
		//combine
		unaffected_top.addAll(affected);
		unaffected_top.addAll(unaffected_bottom);
		person_array = unaffected_top;
		//re-render
		set_y_pos();
		pg.clear();
		render_plot(pg);
	}
	//reset
	m_released = null;
	m_dragged = null;
	m_pressed = null;
	hovering_time_index = -1;
	//loop
	loop();
}
class Grid{
	String id;
	int x, y;
	String type ="";
	Location location = null; //if there is correspoinding location
	String custom_type = "";


	Grid(String id, int x, int y, String type){
		this.id = id;
		this.x = x;
		this.y = y;
		this.type = type;
	}
}
class Location{
	int index = -1;
	String name = "";
	String type = "" ;
	String area = "";
	String id;
	int x, y;


	//display position
	int dy;


	Location(String id, int index, int x, int y, String name, String type, String area){
		this.id = id;
		this.index = index;
		this.x = x;
		this.y = y;
		this.name = name;
		this.type = type;
		this.area = area;
	}
}
class Person{
	int id;
	int[] x_pos, y_pos;
	int first_index = 0; 
	int last_index = 0;
	int order_index = 0;

	float dy = 0; //display position

	Person(int id){
		this.id = id;
	}

	public void render(PGraphics p){
		int prev_x = -1;
		int prev_y = -1;
		float prev_dx = plot_rect.x;
		int prev_type = -1;

		p.strokeCap(SQUARE);
		p.strokeWeight(_line_weight);
		// for(int i = first_index; i <= last_index; i++){
		// for(int i = 0; i < x_pos.length; i++){
		for(int i = 0; i <= last_index; i++){
			int px = x_pos[i];
			int py = y_pos[i];
			if(px == prev_x && py == prev_y){
				//same position as before
				if(i == last_index){
					float new_dx = map(i, 0, x_pos.length, plot_rect.x, plot_rect.x+plot_rect.width);
					p.stroke(area_colors[prev_type]);
					p.line(prev_dx, dy, new_dx, dy);
				}
			}else{
				//different position
				Grid grid = (Grid) grid_map.get(px+"_"+py);
				if(grid == null){
					println("Error: grid is null: "+px+"_"+py +" index ="+i);
				}else{
					String type = grid.type;
					int type_index = area_type_array.indexOf(type);
					float  new_dx = map(i, 0, x_pos.length, plot_rect.x, plot_rect.x+plot_rect.width);
					int opacity = 255;
					if(prev_x == -1){
						//first time
						prev_dx = new_dx;
						prev_type = type_index;						
					}else if(px == -1){
						//last time
						p.stroke(area_colors[prev_type],opacity);
						p.line(prev_dx, dy, new_dx, dy);
						prev_dx = new_dx;
						prev_type = type_index;
						// println("px == -1 :"+i);

					}else{
						//check if same type
						if(prev_type == type_index){
							//move on
							//unless its last move
							if(i == last_index){
								p.stroke(area_colors[prev_type],opacity);
								p.line(prev_dx, dy, new_dx, dy);
							}
						}else{
							//draw
							p.stroke(area_colors[prev_type],opacity);
							p.line(prev_dx, dy, new_dx, dy);
							prev_dx = new_dx;
							prev_type = type_index;
							if(i == last_index){
								p.stroke(area_colors[prev_type],opacity);
								p.line(prev_dx, dy, new_dx, dy);
							}
						}
					}
				}
				prev_x = px;
				prev_y = py;
			}
		}

		//debug
		// float last_dx = map(last_index, 0, x_pos.length, plot_rect.x, plot_rect.x+plot_rect.width);	
		// p.stroke(0);
		// p.strokeCap(PROJECT);
		// p.point(last_dx, dy);
	}

	public void render_locations(PGraphics p){
		int prev_x = -1;
		int prev_y = -1;
		float prev_dx = plot_rect.x;
		int prev_type = -1;
		boolean prevIsLocation = false;

		p.strokeCap(SQUARE);
		p.strokeWeight(_line_weight);
		// for(int i = first_index; i <= last_index; i++){
		// for(int i = 0; i < x_pos.length; i++){
		for(int i = 0; i <= last_index; i++){
			int px = x_pos[i];
			int py = y_pos[i];
			if(px == prev_x && py == prev_y){
				//same position as before
				if(i == last_index && prevIsLocation){
					float new_dx = map(i, 0, x_pos.length, plot_rect.x, plot_rect.x+plot_rect.width);
					p.stroke(area_colors[prev_type]);
					p.line(prev_dx, dy, new_dx, dy);
				}
			}else{
				//different position
				Grid grid = (Grid) grid_map.get(px+"_"+py);
				boolean isLocation = (unique_location_id.contains(grid.id)? true:false);

				if(grid == null){
					println("Error: grid is null: "+px+"_"+py +" index ="+i);
				}else{
					String type = grid.type;
					int type_index = area_type_array.indexOf(type);
					float  new_dx = map(i, 0, x_pos.length, plot_rect.x, plot_rect.x+plot_rect.width);
					int opacity = 255;

					if(prev_x == -1){
						//first time
						prev_dx = new_dx;
						prev_type = type_index;						
					}else if(px == -1 && prevIsLocation){
						//last time
						p.stroke(area_colors[prev_type],opacity);
						p.line(prev_dx, dy, new_dx, dy);
						prev_dx = new_dx;
						prev_type = type_index;
						// println("px == -1 :"+i);

					}else{
						//check if same type
						if(prev_type == type_index){
							//move on
							//unless its last move
							if(i == last_index && prevIsLocation){
								p.stroke(area_colors[prev_type],opacity);
								p.line(prev_dx, dy, new_dx, dy);
							}
						}else{
							//draw
							if(prevIsLocation){
								p.stroke(area_colors[prev_type],opacity);
								p.line(prev_dx, dy, new_dx, dy);
							}
							prev_dx = new_dx;
							prev_type = type_index;
							if(i == last_index){
								p.stroke(area_colors[prev_type],opacity);
								p.line(prev_dx, dy, new_dx, dy);
							}
						}
					}
				}
				prev_x = px;
				prev_y = py;
				prevIsLocation = isLocation;
			}
		}

		//debug
		// float last_dx = map(last_index, 0, x_pos.length, plot_rect.x, plot_rect.x+plot_rect.width);	
		// p.stroke(0);
		// p.strokeCap(PROJECT);
		// p.point(last_dx, dy);
	}

	

	public void render_only_selected_location(PGraphics p){
		int prev_x = -1;
		int prev_y = -1;
		float prev_dx = plot_rect.x;
		int prev_type = -1;

		p.strokeCap(SQUARE);
		p.strokeWeight(_line_weight);
		// for(int i = first_index; i <= last_index; i++){
		// for(int i = 0; i < x_pos.length; i++){
		for(int i = 0; i <= last_index; i++){
			int px = x_pos[i];
			int py = y_pos[i];
			if(px == prev_x && py == prev_y){
				//same position as before
			}else{
				//different position
				Grid grid = (Grid) grid_map.get(px+"_"+py);
				if(grid == null){
					println("Error: grid is null: "+px+"_"+py +" index ="+i);
				}else{
					String type = grid.type;
					int type_index = area_type_array.indexOf(type);
					float  new_dx = map(i, 0, x_pos.length, plot_rect.x, plot_rect.x+plot_rect.width);
					
					int opacity = (selected_location_id.contains(grid.id)?255: 0);

					if(prev_x == -1){
						//first time
						prev_dx = new_dx;
						prev_type = type_index;						
					}else if(px == -1){
						//last time
						p.stroke(area_colors[prev_type],opacity);
						p.line(prev_dx, dy, new_dx, dy);
						prev_dx = new_dx;
						prev_type = type_index;
						// println("px == -1 :"+i);

					}else{
						//check if same type
						if(prev_type == type_index){
							//move on
							//unless its last move
							if(i == last_index){
								p.stroke(area_colors[prev_type],opacity);
								p.line(prev_dx, dy, new_dx, dy);
							}
						}else{
							//draw
							p.stroke(area_colors[prev_type],opacity);
							p.line(prev_dx, dy, new_dx, dy);
							prev_dx = new_dx;
							prev_type = type_index;
						}
					}
				}
				prev_x = px;
				prev_y = py;
			}
		}
	}

	public void render_custom_grid(PGraphics p){
		int prev_x = -1;
		int prev_y = -1;
		float prev_dx = plot_rect.x;
		int prev_type = -1;

		p.strokeCap(SQUARE);
		// p.strokeCap(PROJECT);
		p.strokeWeight(_line_weight);
		// for(int i = first_index; i <= last_index; i++){
		// for(int i = 0; i < x_pos.length; i++){
		for(int i = 0; i <= last_index; i++){
			int px = x_pos[i];
			int py = y_pos[i];
			if(px == prev_x && py == prev_y){
				//same position as before
				if(i == last_index){
					float new_dx = map(i, 0, x_pos.length, plot_rect.x, plot_rect.x+plot_rect.width);
					p.stroke(color_array[prev_type]);
					p.line(prev_dx, dy, new_dx, dy);
				}
			}else{
				//different position
				Grid grid = (Grid) grid_map.get(px+"_"+py);
				if(grid == null){
					println("Error: grid is null: "+px+"_"+py +" index ="+i);
				}else{
					String type = grid.custom_type; /////////////////
					int type_index = Integer.parseInt(type);
					float new_dx = map(i, 0, x_pos.length, plot_rect.x, plot_rect.x+plot_rect.width);
					int opacity = 255;
					// int opacity = (selected_location_id.contains(grid.id)?255: 0);

					if(prev_x == -1){
						//first time
						prev_dx = new_dx;
						prev_type = type_index;		
					}else if(px == -1){
						//this never happens
						println("ERROR: px == -1");

					}else{
						//check if same type
						if(prev_type == type_index){
							//move on
							//unless its last move
							if(i == last_index){
								p.stroke(color_array[prev_type],opacity);
								p.line(prev_dx, dy, new_dx, dy);
								// p.line(prev_dx, dy, new_dx+20, dy);
							}
						}else{
							//draw
							p.stroke(color_array[prev_type],opacity);
							p.line(prev_dx, dy, new_dx, dy);
							prev_dx = new_dx;
							prev_type = type_index;

							if(i == last_index){
								p.stroke(color_array[prev_type],opacity);
								p.line(new_dx, dy, new_dx+1, dy);
							}
						}
					}
				}
				prev_x = px;
				prev_y = py;
			}
		}
	}
	


	public void update_order_by_area(int t){
		int px = x_pos[t];
		int py = y_pos[t];
		Grid grid = (Grid) grid_map.get(px+"_"+py);
		int type_index = -1;
		if(grid == null){
			// println("Error: grid is null: "+px+"_"+py +" index ="+id);
		}else{
			if(current_grid == ORIGINAL_GRID){
				String type = grid.type;
			 	type_index = area_type_array.indexOf(type);
			}else if(current_grid == CUSTOM_GRID){
				type_index = Integer.parseInt(grid.custom_type);
			}
		}
		order_index = type_index;
	}
}
class PersonComparator implements Comparator<Person>{
	int type = 0;

	PersonComparator(int type){
		this.type = type;
	}


	@Override
	public int compare(Person a, Person b){	
		if(type == SORT_FIRST){
			if(a.first_index > b.first_index){
				return 1;
			}else if(a.first_index < b.first_index){
				return -1;
			}
		}else if(type == SORT_LAST){
			if(a.last_index > b.last_index){
				return 1;
			}else if(a.last_index < b.last_index){
				return -1;
			}
		}else if(type == ORDER_INDEX){
			if(a.order_index > b.order_index){
				return 1;
			}else if(a.order_index < b.order_index){
				return -1;
			}
		}

		return 0;
	}	
}
  static public void main(String[] passedArgs) {
    String[] appletArgs = new String[] { "InteractiveSequenceView" };
    if (passedArgs != null) {
      PApplet.main(concat(appletArgs, passedArgs));
    } else {
      PApplet.main(appletArgs);
    }
  }
}
