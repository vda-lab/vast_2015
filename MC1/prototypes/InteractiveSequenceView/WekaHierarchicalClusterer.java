import weka.clusterers.AbstractClusterer;
import weka.clusterers.HierarchicalClusterer;
import weka.clusterers.ClusterEvaluation;
import weka.core.Instance;
import weka.core.EuclideanDistance;
import weka.core.SelectedTag;
import java.util.Arrays;

public class WekaHierarchicalClusterer {
	public static final int EUCLIDEAN	= 0;
	
	public int numClusters;
	
	public AbstractClusterer myClusterer;
	
	/**
	 * The constructor needed for initialization
	 * 
	 */
	public WekaHierarchicalClusterer() {
		myClusterer = new HierarchicalClusterer();
	    ((HierarchicalClusterer)myClusterer).setDistanceIsBranchLength(true);
	    ((HierarchicalClusterer)myClusterer).setLinkType(new SelectedTag(2, HierarchicalClusterer.TAGS_LINK_TYPE));
	    ((HierarchicalClusterer)myClusterer).setNumClusters(1);
	    ((HierarchicalClusterer)myClusterer).setDistanceFunction(new EuclideanDistance()); 
		((HierarchicalClusterer)myClusterer).setPrintNewick(true);

	}
	
	/**
	 * The constructor with a specific type
	 * @param type Clusterer type
	 */
	public WekaHierarchicalClusterer(int type) {
		switch (type) {
		case EUCLIDEAN:
			myClusterer = new HierarchicalClusterer();
			((HierarchicalClusterer)myClusterer).setDistanceFunction(new EuclideanDistance()); 

			break;
		}
	}
	
	//cluster and get linear order=
	public String clusterDataGetOrder(WekaData data) {
		System.out.println("clusterDataGetOrder() --- ");

		int dataLength = data.myInstances.numInstances();
		// System.out.println("clusterDataGetOrder() --- "+dataLength);
		// int[] clusters = new int[dataLength];
		// int[] order = new int[dataLength];

		try {
			data.myInstances.setClassIndex(-1);
			myClusterer.buildClusterer(data.GetData());	//build the clusterer
		} catch (Exception e1) {
			System.out.println("Cluster build unsuccessful");
			System.out.println(e1.getMessage());
		}

		// System.out.println("done clustering");

		// System.out.println("Newwick -------------- ");
		
		// System.out.println(((HierarchicalClusterer)myClusterer).toString());
		String newick = ((HierarchicalClusterer)myClusterer).toString();
		// String[] lines = split(output, "\n");
		//((((a:0.17495,b:0.17495):0.22153,c:0.22153):0.40337,d:0.40337):0.97418,(((e:0.18782,f:0.18782):0.27679,g:0.27679):0.28945,h:0.28945):0.97418)
		// println("Parsed:"+parseNewwick(lines[1].trim()));
		// ArrayList<String> result = parseNewwick(lines[1].trim());
		// for (int i = 0; i < dataLength; i++) {
		// 	Instance inst = data.getInstance(i);
		// 	System.out.println(i+":"+inst);
		// }

		return newick;
	}
	
	
	/**
	 * For advanced users, allows custom options for the clusterer in Weka
	 * Refer to weka doc and specific clusterer doc for details
	 * 
	 * @param options String array of options
	 */
	public void setOptions(String[] options) {
		try {
			((HierarchicalClusterer)myClusterer).setOptions(options);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}









}
