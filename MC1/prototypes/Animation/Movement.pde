class Movement implements Comparable<Movement>{
	int x, y;
	boolean isMovement = false;
	Date t;


	Movement(int x, int y, boolean isMovement, Date t){
		this.x = x;
		this.y = y;
		this.isMovement = isMovement;
		this.t = t;
	}


	int getIndex(){
		return x + y*_grid_size ;
	}

	public int compareTo(Movement that){
		if(this.t.after(that.t)){
			return 1;
		}else if(this.t.before(that.t)){
			return -1;
		}else{
			return 0;
		}
	}
	
}