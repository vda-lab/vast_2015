import processing.core.*; 
import processing.data.*; 
import processing.event.*; 
import processing.opengl.*; 

import java.io.FileInputStream; 
import java.io.BufferedReader; 
import java.io.InputStreamReader; 
import java.util.*; 
import java.awt.*; 
import java.awt.event.*; 
import java.text.*; 

import java.util.HashMap; 
import java.util.ArrayList; 
import java.io.File; 
import java.io.BufferedReader; 
import java.io.PrintWriter; 
import java.io.InputStream; 
import java.io.OutputStream; 
import java.io.IOException; 

public class Animation extends PApplet {









int _margin = 10;
int _left_margin = _margin*3;
int _right_margin = _margin*3;
int _top_margin = _margin*3;
int _bottom_margin = _margin*3;
int _grid_size = 100;


int _cell_width = 8;
int _cell_height = 8;
int _cell_gap = 0;

int _min_dot_size = 3;
int _max_dot_size = 20;


int plot_width, plot_height;
Rectangle plot_rect;
int stage_width, stage_height;

PFont font;


ArrayList<Person> people;
HashMap<Integer, Person> person_map;
Date min_time, max_time;
Date current_time;

Grid grid;

int color_magenta = color(231,41,138);
int color_blue = color(117,112,179);

int img_counter = 0;
boolean is_export_mode = false;


PGraphics pg_map;


public void setup(){
	font = createFont("SUPERNAT1001.TTF", 10);
	textFont(font, 10);
	smooth();

	people = new ArrayList<Person>();
	person_map = new HashMap<Integer, Person>();

	load_data("/Users/Ryo/Desktop/PhD/Year_4/22_VAST_2015/MC1 2015 Data/park-movement-Sun.csv");

	// load_data("/Users/Ryo/Desktop/PhD/Year_4/22_VAST_2015/MC1 2015 Data/park-movement-Fri.csv");
	// load_data("/Users/Ryo/Desktop/PhD/Year_4/22_VAST_2015/MC1 2015 Data/sample.csv"); 

	current_time = min_time;

	//sort data
	for(Person p: people){
		Collections.sort(p.moves);
	}

	println("Debug: number of unique person:"+people.size());
	println("Debug: time range: "+min_time+ " to " +max_time);

	//setup stage
	plot_width = (_grid_size*_cell_width) + (_grid_size-1)*_cell_gap;
	plot_height = (_grid_size*_cell_height) + (_grid_size-1)*_cell_gap;
	plot_rect = new Rectangle(_left_margin, _top_margin, plot_width, plot_height);
	stage_width = _left_margin + _right_margin + plot_width;
	stage_height = _top_margin + _bottom_margin + plot_height;
	size(stage_width, stage_height);


	//setup grid
	grid = new Grid();
	grid.update(current_time);
	grid.isReady = true;

	noLoop();
	// exit();


	//make background
	for(Person p : people){
		for(Movement m: p.moves){
			int index = m.getIndex();
			if(m.isMovement){
				grid.cells[index].isPath = true;
			}else{
				grid.cells[index].isAttraction = true;
			}
		}
	}
	// pg_map = createGraphics(stage_width, stage_height);
	// pg_map.beginDraw();
	// pg_map.smooth();
	// pg_map.ellipseMode(CENTER);
	// pg_map.rectMode(CENTER);

	// for(Cell c:grid.cells){
	// 	if(c.isPath && !c.isAttraction){
	// 		//path
	// 		pg_map.fill(240);
	// 		pg_map.noStroke();
	// 		pg_map.rect((float)c.cell_rect.getCenterX(), (float)c.cell_rect.getCenterY(),8, 8);
	// 	}else if(c.isAttraction){
	// 		//attraction
	// 		pg_map.fill(240);
	// 		pg_map.noStroke();
	// 		pg_map.ellipse((float)c.cell_rect.getCenterX(), (float)c.cell_rect.getCenterY(),20,20);
	// 	}
	// }
	// for(Cell c: grid.cells){
	// 	if(c.isAttraction){
	// 		//attraction
	// 		pg_map.noFill();
	// 		pg_map.stroke(220);
	// 		pg_map.ellipse((float)c.cell_rect.getCenterX(), (float)c.cell_rect.getCenterY(),20,20);
	// 	}
	// }
	// pg_map.endDraw();




	//debug
	println("Debug: plot rect ="+plot_rect);
}

public void load_data(String url){
	SimpleDateFormat ft = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

	int counter = 0;
	File file = new File(url);
	try {
		FileInputStream fstream = new FileInputStream(file);
		BufferedReader br = new BufferedReader(new InputStreamReader(fstream));
		String line;
		//Read File Line By Line
		while ((line = br.readLine()) != null)   {
			if(counter%100000 == 0){
				println("\tloading data:"+counter);
			}
			if(counter == 0){
				println("header:"+line);
			}else{
				String[] s = split(line, ",");
				try{
					Date t = ft.parse(s[0]);
					int id = Integer.parseInt(s[1]);
					String type = s[2].trim();
					boolean isMovement = type.startsWith("movement")?true:false;
					int x_pos = Integer.parseInt(s[3]);
					int y_pos = Integer.parseInt(s[4]);
					// int index = getIndex(x_pos, y_pos);
					
					//find Person
					Person p = (Person) person_map.get(new Integer(id));
					if(p == null){
						p = new Person(id);
						people.add(p);
						person_map.put(new Integer(id), p);
					}
					//make Movement
					Movement m = new Movement(x_pos, y_pos, isMovement, t);
					//add to person
					p.moves.add(m);
					p.move_map.put(t, m);

					//min and max
					if(min_time == null){
						min_time = t;
						max_time = t;
					}else{
						min_time = t.before(min_time)?t:min_time;
						max_time = t.after(max_time)?t:max_time;
					}


				}catch(ParseException e){
					println("Error: parsing:"+line);
				}
			}
		  	counter ++;
		}
		//Close the input stream
		br.close();
	}catch(Exception e){
		e.printStackTrace();
	}
}



public void draw(){
	//fading effect
	// 
	background(240);
	fill(255);
	noStroke();
	rect(plot_rect.x, plot_rect.y, plot_rect.width, plot_rect.height);

	// image(pg_map, 0, 0);
	ellipseMode(CENTER);
	rectMode(CENTER);
	strokeWeight(1);
	for(Cell c:grid.cells){
		if(c.isPath && !c.isAttraction){
			//path
			fill(240);
			noStroke();
			rect((float)c.cell_rect.getCenterX(), (float)c.cell_rect.getCenterY(),8, 8);
		}else if(c.isAttraction){
			//attraction
			fill(240);
			noStroke();
			ellipse((float)c.cell_rect.getCenterX(), (float)c.cell_rect.getCenterY(),20,20);
		}
	}
	for(Cell c: grid.cells){
		if(c.isAttraction){
			//attraction
			noFill();
			stroke(220);
			ellipse((float)c.cell_rect.getCenterX(), (float)c.cell_rect.getCenterY(),20,20);
		}
	}
	rectMode(CORNER);



	if(grid != null && grid.isReady){
		for(Cell c: grid.cells){
			c.render_stationary();
		}
		for(Cell c: grid.cells){
			c.render_checkIn();
		}
		for(Cell c: grid.cells){
			c.render_movement();
		}
	}


	fill(0);
	textAlign(LEFT, TOP);
	text(current_time.toString(), plot_rect.x, 10);


	if(is_export_mode){
		if(current_time.after(max_time)){
			noLoop();
			is_export_mode = false;
		}else{
			saveFrame("animation/animation_####.png");
			current_time = increment_sec(current_time);
			grid.reset();
			grid.update(current_time);
		}
	}else{
		noLoop();
	}

}

public void keyPressed(){
	if(keyCode == LEFT){

	}else if(keyCode == RIGHT){
		// println("RIGHT!");
		current_time = increment_sec(current_time);
		if(current_time.after(max_time)){
			println("stop!");
		}else{
			saveFrame("animation/animation_"+nf(img_counter, 4, 0)+".png");
			grid.reset();
			grid.update(current_time);
			redraw();
			img_counter++;
		}
	}else if(key=='e'){
		//export
		is_export_mode = !is_export_mode;
		if(is_export_mode){
			println("Debug: start exporting");
			loop();
		}else{
			println("Debug: stop exporting");
			noLoop();
		}


	}
}





//// utility
//// Date
public int getHour(Date d){
	Calendar cal = GregorianCalendar.getInstance();
	cal.setTime(d);
	return cal.get(Calendar.HOUR);
}
public int getMin(Date d){
	Calendar cal = GregorianCalendar.getInstance();
	cal.setTime(d);
	return cal.get(Calendar.MINUTE);
}
public int getSec(Date d){
	Calendar cal = GregorianCalendar.getInstance();
	cal.setTime(d);
	return cal.get(Calendar.SECOND);
}

public Date increment_sec(Date d){
	long t = d.getTime(); //millisecond
	t =  t+1000;
	return new Date(t);
}

class Cell{
	int x, y;
	int index;
	HashMap<Integer, ArrayList<Person>> moving_people_map;
	ArrayList<Person> checkIn_people;
	ArrayList<Person> stationary_people;

	Rectangle cell_rect;


	boolean isPath = false;
	boolean isAttraction = false;


	Cell(int x, int y){
		this.x = x;
		this.y = y;
		this.cell_rect = new Rectangle(x, y, _cell_width, _cell_height);


		this.checkIn_people = new ArrayList<Person>();
		this.stationary_people = new ArrayList<Person>();
		this.moving_people_map = new HashMap<Integer, ArrayList<Person>>();
	}


	

	//add moving people
	public void moving(Person p, int prev){
		//get ArrayList
		ArrayList<Person> pple = (ArrayList)moving_people_map.get(new Integer(prev));
		if(pple == null){
			pple = new ArrayList<Person>();
			moving_people_map.put(new Integer(prev), pple);
		}
		pple.add(p);
	}

	//add check_in people
	public void checkingIn(Person p, int prev){
		if(!checkIn_people.contains(p)){
			checkIn_people.add(p);
		}
		p.prev_index = prev;
	}


	public void addStationary(Person p, int prev){
		if(!this.stationary_people.contains(p)){
			this.stationary_people.add(p);
		}
		p.prev_index = prev;
	}

	//reset the hashmap after each move
	public void reset_moving(){
		Iterator it = moving_people_map.entrySet().iterator();
		int new_index = getIndex();
	    while (it.hasNext()) {
	        Map.Entry pair = (Map.Entry)it.next();
	        // System.out.println(pair.getKey() + " = " + pair.getValue());
	        ArrayList<Person> array = (ArrayList)pair.getValue();
	        for(Person p : array){
	        	this.stationary_people.add(p);
	        	p.prev_index = new_index;
	        	p.isMoving = false;
	        	p.isStationary = true;
	        	p.isCheckedIn = false;
	        }
	        it.remove(); // avoids a ConcurrentModificationException
	    }
		moving_people_map.clear();
	}

	// find the person and remove from array
	public void remove(Person p){
		if(checkIn_people.contains(p)){
			checkIn_people.remove(p);
			return;
		}
		if(stationary_people.contains(p)){
			stationary_people.remove(p);
		}
	}





	public void render(){
		// int running_index = 0;
		// for(Person p : assigned_people){
		// 	PVector pos = getPosition(x, y, running_index);
		// 	if(p.isCheckedIn){
		// 		stroke(color_magenta);
		// 	}else{
		// 		stroke(color_blue);
		// 	}
		// 	point(pos.x, pos.y);
		// 	running_index ++;
		// }
	}


	//draw stationary 
	public void render_stationary(){
		int stationary_count = stationary_people.size();
		if(stationary_count != 0){
			//20 max
			float pointWeight = constrain(map(stationary_count, 1, 20, _min_dot_size, _max_dot_size), _min_dot_size, _max_dot_size);
			strokeWeight(pointWeight);
			strokeCap(PROJECT);
			stroke(color_blue, 120);
			point((float)cell_rect.getCenterX(), (float)cell_rect.getCenterY());
			// println("debug:stationary:"+getIndex()+":"+stationary_count);

			if(stationary_count > 20){
				fill(0);
				textAlign(CENTER, CENTER);
				text(""+stationary_count, (float)cell_rect.getCenterX(), (float)cell_rect.getCenterY());
			}
		}

	}

	public void render_checkIn(){
		int checkIn_count = checkIn_people.size();
		if(checkIn_count != 0){
			//100 max
			float pointWeight = constrain(map(checkIn_count, 1, 20, _min_dot_size, _max_dot_size), _min_dot_size, _max_dot_size);
			strokeWeight(1);
			stroke(color_magenta);
			noFill();
			ellipseMode(CENTER);
			ellipse((float)cell_rect.getCenterX(), (float)cell_rect.getCenterY(), pointWeight, pointWeight);
			
			// println("debug:render_checkIn:"+getX()+","+getY()+" index="+index);

			//draw count
			fill(0);
			textAlign(CENTER, BOTTOM);
			text(""+checkIn_count, (float)cell_rect.getCenterX(), (float)cell_rect.getCenterY());
		}
	}


	public void render_movement(){
		Iterator it = moving_people_map.entrySet().iterator();
		int new_index = getIndex();
	    while (it.hasNext()) {
	        Map.Entry pair = (Map.Entry)it.next();
	        ArrayList<Person> array = (ArrayList)pair.getValue();
	        Integer integer = (Integer)pair.getKey();
	        int prev_index = integer.intValue();
	        Cell prev_cell = grid.cells[prev_index];
	        float from_x = (float)prev_cell.cell_rect.getCenterX();
	        float from_y = (float)prev_cell.cell_rect.getCenterY();
	        float to_x = (float) this.cell_rect.getCenterX();
	        float to_y = (float)this.cell_rect.getCenterY();

	        int count = array.size();
	        float weight = constrain(map(count, 1, 20, _min_dot_size, _max_dot_size), _min_dot_size, _max_dot_size);

	        strokeWeight(weight);
	        strokeCap(ROUND);
	        stroke(color_blue, 200);
	        line(from_x, from_y, to_x, to_y);

	    }
	}

	public int getX(){
		return index % _grid_size;
	}
	public int getY(){
		return index / _grid_size;
	}

	public int getIndex(){
		return index;
	}
}
class Grid{
	Cell[]  cells;
	boolean isReady = false;

	Grid(){
		int total_cell_count = _grid_size*_grid_size;
		cells = new Cell[total_cell_count];
		//set cell positions
		int runningX = _left_margin;
		int runningY = _top_margin + plot_rect.height;
		int x_counter = 0;
		for(int i = 0; i < total_cell_count; i++){
			int cx = runningX;
			int cy = runningY - _cell_height;

			// println("debug: "+i+"\t"+ cx+"\t"+cy);
			cells[i] = new Cell(cx, cy);
			cells[i].index = i;
			x_counter ++;
			if(x_counter > (_grid_size-1)){
				//next row
				runningX = _left_margin;
				runningY -= (_cell_height + _cell_gap);
				x_counter = 0;
			}else{
				runningX  += _cell_width + _cell_gap;
			}
		}
	}



	//update Grid based on Date
	public void update(Date t){
		for(Person p: people){
			//check if the person has record at given time
			Movement m = (Movement) p.move_map.get(t);
			if(m != null){
				int index = m.getIndex();
				if(m.isMovement){
					//making a movement
					int prev_index = p.prev_index;
					if(prev_index == -1){
						println("ERROR: this should not happen: prev_index = -1 for "+p.id);
					}else{
						//remove from previous cell
						// println("Debug: update(): prev_index="+prev_index);
						Cell prev_cell = cells[prev_index];
						prev_cell.remove(p);
					}
					//index to move to
					p.isCheckedIn = false;
					p.isMoving = true;
					p.isStationary = false;
					p.isOnMap = true;
					cells[index].moving(p, prev_index);
				}else{
					//check_in event
					int prev_index = p.prev_index;
					if(prev_index == -1){
						//first time check in
						p.isCheckedIn = true;
						p.isMoving = false;
						p.isStationary = false;
						p.isOnMap = true;
						cells[index].checkingIn(p, index);
					}else{
						//remove from previous cell
						Cell prev_cell = cells[prev_index];
						prev_cell.remove(p);
						p.isCheckedIn = true;
						p.isMoving = false;
						p.isStationary = false;
						p.isOnMap = true;
						cells[index].checkingIn(p, index);
					}
				}
			}else{
				if(p.isOnMap){
					if(p.isCheckedIn){
						//checked in
						// println("debug: checkedIn:"+p.id);
					}else if(p.isMoving){		
						// println("Error: debug: Moving:"+p.id);				
					}else{
						//still not moving
						// println("debug: stationary:"+p.id);
					}

				}
			}
		}
	}


	//clear assigned people
	public void reset(){
		for(Cell cell:cells){
			//interate through moving_people_map to update to stationay
			cell.reset_moving();

		}
	}

}
class Movement implements Comparable<Movement>{
	int x, y;
	boolean isMovement = false;
	Date t;


	Movement(int x, int y, boolean isMovement, Date t){
		this.x = x;
		this.y = y;
		this.isMovement = isMovement;
		this.t = t;
	}


	public int getIndex(){
		return x + y*_grid_size ;
	}

	public int compareTo(Movement that){
		if(this.t.after(that.t)){
			return 1;
		}else if(this.t.before(that.t)){
			return -1;
		}else{
			return 0;
		}
	}
	
}
class Person{
	int id;
	ArrayList<Movement> moves;
	HashMap<Date, Movement> move_map;
	boolean isCheckedIn = false;
	boolean isStationary = false;
	boolean isMoving = false;
	boolean isOnMap = false;

	

	int prev_index = -1;

	Person(int id){
		this.id = id;
		this.moves = new ArrayList<Movement>();
		this.move_map = new HashMap<Date, Movement>();
	}

}
  static public void main(String[] passedArgs) {
    String[] appletArgs = new String[] { "Animation" };
    if (passedArgs != null) {
      PApplet.main(concat(appletArgs, passedArgs));
    } else {
      PApplet.main(appletArgs);
    }
  }
}
