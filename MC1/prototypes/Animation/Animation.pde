import java.io.FileInputStream;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.*;
import java.awt.*;
import java.awt.event.*;
import java.text.*;

int _margin = 10;
int _left_margin = _margin*3;
int _right_margin = _margin*3;
int _top_margin = _margin*3;
int _bottom_margin = _margin*3;
int _grid_size = 100;


int _cell_width = 8;
int _cell_height = 8;
int _cell_gap = 0;

int _min_dot_size = 3;
int _max_dot_size = 20;


int plot_width, plot_height;
Rectangle plot_rect;
int stage_width, stage_height;

PFont font;


ArrayList<Person> people;
HashMap<Integer, Person> person_map;
Date min_time, max_time;
Date current_time;

Grid grid;

int color_magenta = color(231,41,138);
int color_blue = color(117,112,179);

int img_counter = 0;
boolean is_export_mode = false;


PGraphics pg_map;


void setup(){
	font = createFont("SUPERNAT1001.TTF", 10);
	textFont(font, 10);
	smooth();

	people = new ArrayList<Person>();
	person_map = new HashMap<Integer, Person>();

	load_data("/Users/Ryo/Desktop/PhD/Year_4/22_VAST_2015/MC1 2015 Data/park-movement-Sun.csv");

	// load_data("/Users/Ryo/Desktop/PhD/Year_4/22_VAST_2015/MC1 2015 Data/park-movement-Fri.csv");
	// load_data("/Users/Ryo/Desktop/PhD/Year_4/22_VAST_2015/MC1 2015 Data/sample.csv"); 

	current_time = min_time;

	//sort data
	for(Person p: people){
		Collections.sort(p.moves);
	}

	println("Debug: number of unique person:"+people.size());
	println("Debug: time range: "+min_time+ " to " +max_time);

	//setup stage
	plot_width = (_grid_size*_cell_width) + (_grid_size-1)*_cell_gap;
	plot_height = (_grid_size*_cell_height) + (_grid_size-1)*_cell_gap;
	plot_rect = new Rectangle(_left_margin, _top_margin, plot_width, plot_height);
	stage_width = _left_margin + _right_margin + plot_width;
	stage_height = _top_margin + _bottom_margin + plot_height;
	size(stage_width, stage_height);


	//setup grid
	grid = new Grid();
	grid.update(current_time);
	grid.isReady = true;

	noLoop();
	// exit();


	//make background
	for(Person p : people){
		for(Movement m: p.moves){
			int index = m.getIndex();
			if(m.isMovement){
				grid.cells[index].isPath = true;
			}else{
				grid.cells[index].isAttraction = true;
			}
		}
	}
	// pg_map = createGraphics(stage_width, stage_height);
	// pg_map.beginDraw();
	// pg_map.smooth();
	// pg_map.ellipseMode(CENTER);
	// pg_map.rectMode(CENTER);

	// for(Cell c:grid.cells){
	// 	if(c.isPath && !c.isAttraction){
	// 		//path
	// 		pg_map.fill(240);
	// 		pg_map.noStroke();
	// 		pg_map.rect((float)c.cell_rect.getCenterX(), (float)c.cell_rect.getCenterY(),8, 8);
	// 	}else if(c.isAttraction){
	// 		//attraction
	// 		pg_map.fill(240);
	// 		pg_map.noStroke();
	// 		pg_map.ellipse((float)c.cell_rect.getCenterX(), (float)c.cell_rect.getCenterY(),20,20);
	// 	}
	// }
	// for(Cell c: grid.cells){
	// 	if(c.isAttraction){
	// 		//attraction
	// 		pg_map.noFill();
	// 		pg_map.stroke(220);
	// 		pg_map.ellipse((float)c.cell_rect.getCenterX(), (float)c.cell_rect.getCenterY(),20,20);
	// 	}
	// }
	// pg_map.endDraw();




	//debug
	println("Debug: plot rect ="+plot_rect);
}

void load_data(String url){
	SimpleDateFormat ft = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

	int counter = 0;
	File file = new File(url);
	try {
		FileInputStream fstream = new FileInputStream(file);
		BufferedReader br = new BufferedReader(new InputStreamReader(fstream));
		String line;
		//Read File Line By Line
		while ((line = br.readLine()) != null)   {
			if(counter%100000 == 0){
				println("\tloading data:"+counter);
			}
			if(counter == 0){
				println("header:"+line);
			}else{
				String[] s = split(line, ",");
				try{
					Date t = ft.parse(s[0]);
					int id = Integer.parseInt(s[1]);
					String type = s[2].trim();
					boolean isMovement = type.startsWith("movement")?true:false;
					int x_pos = Integer.parseInt(s[3]);
					int y_pos = Integer.parseInt(s[4]);
					// int index = getIndex(x_pos, y_pos);
					
					//find Person
					Person p = (Person) person_map.get(new Integer(id));
					if(p == null){
						p = new Person(id);
						people.add(p);
						person_map.put(new Integer(id), p);
					}
					//make Movement
					Movement m = new Movement(x_pos, y_pos, isMovement, t);
					//add to person
					p.moves.add(m);
					p.move_map.put(t, m);

					//min and max
					if(min_time == null){
						min_time = t;
						max_time = t;
					}else{
						min_time = t.before(min_time)?t:min_time;
						max_time = t.after(max_time)?t:max_time;
					}


				}catch(ParseException e){
					println("Error: parsing:"+line);
				}
			}
		  	counter ++;
		}
		//Close the input stream
		br.close();
	}catch(Exception e){
		e.printStackTrace();
	}
}



void draw(){
	//fading effect
	// 
	background(240);
	fill(255);
	noStroke();
	rect(plot_rect.x, plot_rect.y, plot_rect.width, plot_rect.height);

	// image(pg_map, 0, 0);
	ellipseMode(CENTER);
	rectMode(CENTER);
	strokeWeight(1);
	for(Cell c:grid.cells){
		if(c.isPath && !c.isAttraction){
			//path
			fill(240);
			noStroke();
			rect((float)c.cell_rect.getCenterX(), (float)c.cell_rect.getCenterY(),8, 8);
		}else if(c.isAttraction){
			//attraction
			fill(240);
			noStroke();
			ellipse((float)c.cell_rect.getCenterX(), (float)c.cell_rect.getCenterY(),20,20);
		}
	}
	for(Cell c: grid.cells){
		if(c.isAttraction){
			//attraction
			noFill();
			stroke(220);
			ellipse((float)c.cell_rect.getCenterX(), (float)c.cell_rect.getCenterY(),20,20);
		}
	}
	rectMode(CORNER);



	if(grid != null && grid.isReady){
		for(Cell c: grid.cells){
			c.render_stationary();
		}
		for(Cell c: grid.cells){
			c.render_checkIn();
		}
		for(Cell c: grid.cells){
			c.render_movement();
		}
	}


	fill(0);
	textAlign(LEFT, TOP);
	text(current_time.toString(), plot_rect.x, 10);


	if(is_export_mode){
		if(current_time.after(max_time)){
			noLoop();
			is_export_mode = false;
		}else{
			saveFrame("animation/animation_####.png");
			current_time = increment_sec(current_time);
			grid.reset();
			grid.update(current_time);
		}
	}else{
		noLoop();
	}

}

void keyPressed(){
	if(keyCode == LEFT){

	}else if(keyCode == RIGHT){
		// println("RIGHT!");
		current_time = increment_sec(current_time);
		if(current_time.after(max_time)){
			println("stop!");
		}else{
			saveFrame("animation/animation_"+nf(img_counter, 4, 0)+".png");
			grid.reset();
			grid.update(current_time);
			redraw();
			img_counter++;
		}
	}else if(key=='e'){
		//export
		is_export_mode = !is_export_mode;
		if(is_export_mode){
			println("Debug: start exporting");
			loop();
		}else{
			println("Debug: stop exporting");
			noLoop();
		}


	}
}





//// utility
//// Date
int getHour(Date d){
	Calendar cal = GregorianCalendar.getInstance();
	cal.setTime(d);
	return cal.get(Calendar.HOUR);
}
int getMin(Date d){
	Calendar cal = GregorianCalendar.getInstance();
	cal.setTime(d);
	return cal.get(Calendar.MINUTE);
}
int getSec(Date d){
	Calendar cal = GregorianCalendar.getInstance();
	cal.setTime(d);
	return cal.get(Calendar.SECOND);
}

Date increment_sec(Date d){
	long t = d.getTime(); //millisecond
	t =  t+1000;
	return new Date(t);
}

