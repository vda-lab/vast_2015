class Movement implements Comparable<Movement>{
	int x, y;
	boolean isMovement = false;
	Date t;


	Movement(int x, int y, boolean isMovement, Date t){
		this.x = x;
		this.y = y;
		this.isMovement = isMovement;
		this.t = t;
	}


	int getIndex(){
		return x + y*_grid_size ;
	}

	public int compareTo(Movement that){
		if(this.t.after(that.t)){
			return 1;
		}else if(this.t.before(that.t)){
			return -1;
		}else{
			if(this.isMovement && !that.isMovement){
				return -1;
			}else if(!this.isMovement && that.isMovement){
				return 1;
			}
			return 0;
		}
	}

	String toString(){
		return "("+x+","+y+") "+(isMovement?"move":"check-in")+" : "+t.toString();
	}
	String toStringPos(){
		return "("+x+","+y+")";
	}

	int getSeconds(){
		long l = t.getTime();
		int result = (int) (l/1000l);
		return result;
	}

	long getSeconds_long(){
		// return (int)(d.getTime()/1000);
		long l = t.getTime() / 1000l;
		return l;
	}

	
}
