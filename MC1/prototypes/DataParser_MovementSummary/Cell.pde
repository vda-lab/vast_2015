class Cell{
	int x, y;
	int index;
	HashMap<Integer, ArrayList<Person>> moving_people_map;
	ArrayList<Person> checkIn_people;
	ArrayList<Person> stationary_people;

	Rectangle cell_rect;


	boolean isPath = false;
	boolean isAttraction = false;


	Cell(int x, int y){
		this.x = x;
		this.y = y;
		this.cell_rect = new Rectangle(x, y, _cell_width, _cell_height);


		this.checkIn_people = new ArrayList<Person>();
		this.stationary_people = new ArrayList<Person>();
		this.moving_people_map = new HashMap<Integer, ArrayList<Person>>();
	}


	

	//add moving people
	void moving(Person p, int prev){
		//get ArrayList
		ArrayList<Person> pple = (ArrayList)moving_people_map.get(new Integer(prev));
		if(pple == null){
			pple = new ArrayList<Person>();
			moving_people_map.put(new Integer(prev), pple);
		}
		pple.add(p);
	}

	//add check_in people
	void checkingIn(Person p, int prev){
		if(!checkIn_people.contains(p)){
			checkIn_people.add(p);
		}
		p.prev_index = prev;
		// println("debug: Check in :"+p.id);
	}


	void addStationary(Person p, int prev){
		if(!this.stationary_people.contains(p)){
			this.stationary_people.add(p);
		}
		p.prev_index = prev;
	}

	//reset the hashmap after each move
	void reset_moving(){
		Iterator it = moving_people_map.entrySet().iterator();
		int new_index = getIndex();
	    while (it.hasNext()) {
	        Map.Entry pair = (Map.Entry)it.next();
	        // System.out.println(pair.getKey() + " = " + pair.getValue());
	        ArrayList<Person> array = (ArrayList)pair.getValue();
	        for(Person p : array){
	        	this.stationary_people.add(p);
	        	p.prev_index = new_index;
	        	p.isMoving = false;
	        	p.isStationary = true;
	        	p.isCheckedIn = false;
	        }
	        it.remove(); // avoids a ConcurrentModificationException
	    }
		moving_people_map.clear();
	}

	// find the person and remove from array
	void remove(Person p){
		if(checkIn_people.contains(p)){
			checkIn_people.remove(p);
			return;
		}
		if(stationary_people.contains(p)){
			stationary_people.remove(p);
		}
	}

	void render_stationary(PGraphics pg, int c, boolean isLast){
		int stationary_count = stationary_people.size();
		if(stationary_count != 0){
			//20 max
			float pointWeight = constrain(map(stationary_count, 1, 20, _min_dot_size, _max_dot_size), _min_dot_size, _max_dot_size);
			pg.strokeWeight(pointWeight);
			pg.strokeCap(PROJECT);
			pg.stroke(c, 120);
			pg.point((float)cell_rect.getCenterX(), (float)cell_rect.getCenterY());
			// println("debug:stationary:"+getIndex()+":"+stationary_count);
			if(stationary_count > 20 && isLast){
				pg.fill(255);
				pg.textAlign(CENTER, CENTER);
				pg.text(""+stationary_count, (float)cell_rect.getCenterX(), (float)cell_rect.getCenterY());
			}
		}
	}
	void render_checkIn(PGraphics pg, boolean isLast){
		int checkIn_count = checkIn_people.size();
		if(checkIn_count != 0){
			//100 max
			float pointWeight = constrain(map(checkIn_count, 1, 20, _min_dot_size, _max_dot_size), _min_dot_size, _max_dot_size);
			pg.strokeWeight(1);
			pg.stroke(color_magenta);
			pg.noFill();
			pg.ellipseMode(CENTER);
			pg.ellipse((float)cell_rect.getCenterX(), (float)cell_rect.getCenterY(), pointWeight, pointWeight);
			//draw count
			if(isLast){
				pg.fill(0);
				pg.textAlign(CENTER, BOTTOM);
				pg.text(""+checkIn_count, (float)cell_rect.getCenterX(), (float)cell_rect.getCenterY());
			}
		}	
	}
	void render_movement(PGraphics pg, int c){
		Iterator it = moving_people_map.entrySet().iterator();
		int new_index = getIndex();
	    while (it.hasNext()) {
	        Map.Entry pair = (Map.Entry)it.next();
	        ArrayList<Person> array = (ArrayList)pair.getValue();
	        Integer integer = (Integer)pair.getKey();
	        int prev_index = integer.intValue();
	        Cell prev_cell = grid.cells[prev_index];
	        float from_x = (float)prev_cell.cell_rect.getCenterX();
	        float from_y = (float)prev_cell.cell_rect.getCenterY();
	        float to_x = (float) this.cell_rect.getCenterX();
	        float to_y = (float)this.cell_rect.getCenterY();

	        int count = array.size();
	        float weight = constrain(map(count, 1, 20, _min_dot_size, _max_dot_size), _min_dot_size, _max_dot_size);

	        pg.strokeWeight(weight);
	        pg.strokeCap(ROUND);
	        pg.stroke(c, 200);
	        pg.line(from_x, from_y, to_x, to_y);
	    }
	}



	int getX(){
		return index % _grid_size;
	}
	int getY(){
		return index / _grid_size;
	}

	int getIndex(){
		return index;
	}
}
