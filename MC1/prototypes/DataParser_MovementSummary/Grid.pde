class Grid{
	Cell[]  cells;
	boolean isReady = false;

	Grid(){
		int total_cell_count = _grid_size*_grid_size;
		cells = new Cell[total_cell_count];
		//set cell positions
		int runningX = _left_margin;
		int runningY = _top_margin + plot_rect.height;
		int x_counter = 0;
		for(int i = 0; i < total_cell_count; i++){
			int cx = runningX;
			int cy = runningY - _cell_height;

			// println("debug: "+i+"\t"+ cx+"\t"+cy);
			cells[i] = new Cell(cx, cy);
			cells[i].index = i;
			x_counter ++;
			if(x_counter > (_grid_size-1)){
				//next row
				runningX = _left_margin;
				runningY -= (_cell_height + _cell_gap);
				x_counter = 0;
			}else{
				runningX  += _cell_width + _cell_gap;
			}
		}
	}



	// update Grid based on Date
	void update(Date t){
		// println("debug: Grid.update():"+t.toString());
		for(Person p: people){
			//check if the person has record at given time
			Movement m = (Movement) p.move_map.get(t);
			if(m != null){
				int index = m.getIndex();
				if(m.isMovement){
					// println("\tupdate() : movement:"+p.id+ " index ="+index);
					//making a movement
					int prev_index = p.prev_index;
					if(prev_index == -1){
						println("ERROR: this should not happen: prev_index = -1 for "+p.id + " : "+m.t.toString());
					}else{
						//remove from previous cell
						// println("Debug: update(): prev_index="+prev_index);
						Cell prev_cell = cells[prev_index];
						prev_cell.remove(p);
					}
					//index to move to
					p.isCheckedIn = false;
					p.isMoving = true;
					p.isStationary = false;
					p.isOnMap = true;
					cells[index].moving(p, prev_index);
					p.lastMove_date = t;
				}else{
					// println("\tupdate() : checkIn:"+p.id+ " index ="+index);
					//check_in event
					int prev_index = p.prev_index;
					if(prev_index == -1){
						//first time check in
						p.isCheckedIn = true;
						p.isMoving = false;
						p.isStationary = false;
						p.isOnMap = true;
						cells[index].checkingIn(p, index);
						p.checkIn_date = t;
					}else{
						//remove from previous cell
						Cell prev_cell = cells[prev_index];
						prev_cell.remove(p);
						p.isCheckedIn = true;
						p.isMoving = false;
						p.isStationary = false;
						p.isOnMap = true;
						cells[index].checkingIn(p, index);
						p.checkIn_date = t;
					}
				}
			}else{
				if(p.isOnMap){
					if(p.isCheckedIn){
						//checked in
						// println("debug: checkedIn:"+p.id);
					}else if(p.isMoving){		
						// println("Error: debug: Moving:"+p.id);				
					}else{
						//still not moving
						// println("debug: stationary:"+p.id);
					}

				}
			}
		}
	}


	//clear assigned people
	void reset(){
		for(Cell cell:cells){
			//interate through moving_people_map to update to stationay
			cell.reset_moving();

		}
	}

	void time_reset(Date t){
		for(Cell cell:cells){
			cell.moving_people_map.clear();

			ArrayList<Person> toRemove = new ArrayList<Person>();
			for(Person p: cell.checkIn_people){
				if(p.checkIn_date.after(t)){
					toRemove.add(p);
				}
			}
			cell.checkIn_people.removeAll(toRemove);

			toRemove.clear();
			for(Person p: cell.stationary_people){
				if(p.lastMove_date.after(t)){
					toRemove.add(p);
				}
			}
			cell.stationary_people.removeAll(toRemove);
		}
	}

}
