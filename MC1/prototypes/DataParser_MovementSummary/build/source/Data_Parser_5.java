import processing.core.*; 
import processing.data.*; 
import processing.event.*; 
import processing.opengl.*; 

import java.io.FileInputStream; 
import java.io.BufferedReader; 
import java.io.InputStreamReader; 
import java.util.*; 
import java.awt.*; 
import java.awt.event.*; 
import java.text.*; 

import java.util.HashMap; 
import java.util.ArrayList; 
import java.io.File; 
import java.io.BufferedReader; 
import java.io.PrintWriter; 
import java.io.InputStream; 
import java.io.OutputStream; 
import java.io.IOException; 

public class Data_Parser_5 extends PApplet {

//Movement summary









int _margin = 10;
int _left_margin = _margin*3;
int _right_margin = _margin*3;
int _top_margin = _margin*3;
int _bottom_margin = _margin*3;
int _grid_size = 100;


int _cell_width = 8;
int _cell_height = 8;
int _cell_gap = 0;

int _min_dot_size = 3;
int _max_dot_size = 20;


int plot_width, plot_height;
Rectangle plot_rect;
int stage_width, stage_height;

PFont font;


ArrayList<Person> people;
HashMap<Integer, Person> person_map;
Date min_time, max_time;
Date current_time;
Date seq_start, seq_end;

Grid grid;

int color_magenta = color(231,41,138);
int color_blue = color(117,112,179);
// int[] color_blue_array = {color(255,247,251),color(236,231,242),color(208,209,230),color(166,189,219),color(116,169,207),color(54,144,192),color(5,112,176),color(4,90,141),color(2,56,88)};
int[] color_blue_array = {color(222,235,247),color(222,235,247),color(222,235,247),color(222,235,247),color(222,235,247),color(222,235,247),
						color(198,219,239),color(198,219,239),color(198,219,239),color(198,219,239),color(198,219,239),
						color(158,202,225),color(158,202,225),color(158,202,225),color(158,202,225),
						color(107,174,214),color(107,174,214),
						color(66,146,198),color(66,146,198),
						color(33,113,181),
						color(8,81,156)};


int img_counter = 0;
boolean is_export_mode = false;


PGraphics pg_map;
PGraphics[] pg_array;
PGraphics[] pg_next_half;
int sequence_length = 60; //seconds


int[][] x_pos_matrix, y_pos_matrix;
long start_m_sec, end_m_sec;  //milliseconds
int interval = 60*1000; // one minute
int total_minutes;

int time_window = 12; //10 seonds  ////////////////// *************** values to adjust

String data_day = "Fri";
// String data_day = "Sat";
// String data_day = "Sun";




HashMap<String, Spot> spot_map;
SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

public void setup(){
	font = createFont("SUPERNAT1001.TTF", 10);
	textFont(font, 10);
	smooth();

	people = new ArrayList<Person>();
	person_map = new HashMap<Integer, Person>();

	// load_data("/Users/Ryo/Desktop/PhD/Year_4/22_VAST_2015/MC1 2015 Data/park-movement-"+data_day+".csv");
	// load_data("/Users/Ryo/Desktop/PhD/Year_4/22_VAST_2015/MC1 2015 Data/park-movement-Fri.csv");
	// load_data("/Users/Ryo/Desktop/PhD/Year_4/22_VAST_2015/MC1 2015 Data/park-movement-Sat.csv");
	// load_data("/Users/Ryo/Desktop/PhD/Year_4/22_VAST_2015/MC1 2015 Data/park-movement-Sun.csv");
	load_data("/Users/Ryo/Desktop/PhD/Year_4/22_VAST_2015/MC1 Data June 2015 V3/park-movement-Fri-FIXED-2.0.csv");
	//smaller test data
	// load_data("/Users/Ryo/Desktop/PhD/Year_4/22_VAST_2015/Derived_Data/sample_friday.csv");
	// load_data("/Users/Ryo/Desktop/PhD/Year_4/22_VAST_2015/Derived_Data/ten_sample_friday.csv");

	// load_data(dataPath("one_sample_friday.csv"));
	println("Debug: number of unique person: "+people.size());

	//find min_time and max_time
	min_time = people.get(0).moves.get(0).t;
	max_time = min_time;
	for(Person p: people){
		int move_counts = p.moves.size();
		Date p_min = p.moves.get(0).t;
		Date p_max = p.moves.get(move_counts-1).t;

		min_time = (min_time.after(p_min)? p_min:min_time);
		max_time = (max_time.before(p_max)? p_max:max_time);
	}

	println("Debug: min_time ="+min_time);
	println("Debug: max_time ="+max_time);

	// clean start date
	GregorianCalendar min_cal = new GregorianCalendar(getYear(min_time), getMonth(min_time),
		getDay(min_time), getHour(min_time), getMin(min_time));
	GregorianCalendar max_cal = new GregorianCalendar(getYear(max_time), getMonth(max_time),
		getDay(max_time), getHour(max_time), getMin(max_time)+1);

	// long min_sec = min_cal.getTime();
	println("Debug: clean min:"+ min_cal.getTime());
	println("Debug: clean max:"+ max_cal.getTime());

	start_m_sec = min_cal.getTimeInMillis();
	end_m_sec = max_cal.getTimeInMillis();
	total_minutes = ((int)(end_m_sec - start_m_sec)/interval) +1;
	println("Debug: total munites = "+total_minutes);


	for(int i = 0; i<people.size(); i++){
		Person p = people.get(i);
		p.move_output = new ArrayList<String>(); // to store output text
		Collections.sort(p.moves);

		Movement first_move = p.moves.get(0);
		Movement prev_move = p.moves.get(0);
		float sum_distance = 0;
		float sum_sec = 0;
		//perse by person
		for(int j = 1; j<p.moves.size(); j++){
			Movement m = p.moves.get(j);
			float distance = dist(prev_move.x, prev_move.y, m.x, m.y);
			float seconds = m.getSeconds() - prev_move.getSeconds();
				
			if(first_move != prev_move){
				if(seconds <= time_window){
					if(m.isMovement){
						//move on
						sum_distance += distance;
						sum_sec += seconds;
					}else{
						//checkin
						//first to m
						float speed = sum_distance/sum_sec;
						if(sum_sec ==0f){
							speed = 0;
						}
						String result = p.id+","+first_move.x+","+first_move.y+","+m.x+","+m.y+","+first_move.getSeconds_long()+","+m.getSeconds_long()+","+sum_distance+","+sum_sec+","+(speed)+",movement";
						p.move_output.add(result);
						//reset
						sum_distance = 0;
						sum_sec = 0;
						first_move = m;
					}
				}else{
					//long travel
					//save first to prev
					float speed = sum_distance/sum_sec;
					if(sum_sec ==0f){
						speed = 0;
					}
					String result = p.id+","+first_move.x+","+first_move.y+","+prev_move.x+","+prev_move.y+","+first_move.getSeconds_long()+","+prev_move.getSeconds_long()+","+sum_distance+","+sum_sec+","+(speed)+",movement";
					p.move_output.add(result);
					sum_distance = 0;
					sum_sec = 0;

					sum_distance += distance;
					sum_sec += seconds;
					//save prev to m  // slow movement
					speed = sum_distance/sum_sec;
					if(sum_sec ==0f){
						speed = 0;
					}
					result = p.id+","+prev_move.x+","+prev_move.y+","+m.x+","+m.y+","+prev_move.getSeconds_long()+","+m.getSeconds_long()+","+sum_distance+","+sum_sec+","+(speed)+",movement";
					p.move_output.add(result);
					//reset
					sum_distance = 0;
					sum_sec = 0;
					first_move = m;
				}
			}else{
				if(prev_move.isMovement){
					//no previous move
					if(seconds <= time_window){
						if(m.isMovement){
							//move on
							sum_distance += distance;
							sum_sec += seconds;
						}else{
							//checkin
							//prev to m
							float speed = sum_distance/sum_sec;
							if(sum_sec ==0f){
								speed = 0;
							}
							String result = p.id+","+prev_move.x+","+prev_move.y+","+m.x+","+m.y+","+prev_move.getSeconds_long()+","+m.getSeconds_long()+","+sum_distance+","+sum_sec+","+(speed)+",movement";
							p.move_output.add(result);
							//reset
							sum_distance = 0;
							sum_sec = 0;
							first_move = m;
						}
					}else{
						//long travel
						sum_distance += distance;
						sum_sec += seconds;
						float speed = sum_distance/sum_sec;
						if(sum_sec ==0f){
							speed = 0;
						}

						//save prev to m  // slow movement
						String result = p.id+","+prev_move.x+","+prev_move.y+","+m.x+","+m.y+","+prev_move.getSeconds_long()+","+m.getSeconds_long()+","+sum_distance+","+sum_sec+","+speed+",movement";
						p.move_output.add(result);
						//reset
						sum_distance = 0;
						sum_sec = 0;
						first_move = m;
					}
				}else{
					//checkin
					sum_distance += distance;
					sum_sec += seconds;
					//save prev to m  // slow movement
					String result = p.id+","+prev_move.x+","+prev_move.y+","+m.x+","+m.y+","+prev_move.getSeconds_long()+","+m.getSeconds_long()+","+sum_distance+","+sum_sec+","+0+",checkin";
					p.move_output.add(result);
					//reset
					sum_distance = 0;
					sum_sec = 0;
					first_move = m;
				}
			}
			prev_move = m;
		}

		println("Debug:"+p.id+" has "+p.move_output.size()+" moves");
		// for(String out :p.move_output){
		// 	println("\t"+out.replaceAll(",", "\t"));
		// }
		//	179386	44	25	45	26	1402053694	1402055998	1.4142135	2304.0	6.1380793E-4	movement

	}

	//write a file
	PrintWriter output = createWriter("movement_summary_"+data_day+".csv");
	// PrintWriter output = createWriter("movement_summary_sample.csv");

	for(Person p: people){
		for(String out :p.move_output){
			// println("\t"+out.replaceAll(",", "\t"));
			output.println(out);
		}
	}
	output.flush();
	output.close();




	//setup stage
	plot_width = (_grid_size*_cell_width) + (_grid_size-1)*_cell_gap;
	plot_height = (_grid_size*_cell_height) + (_grid_size-1)*_cell_gap;
	plot_rect = new Rectangle(_left_margin, _top_margin, plot_width, plot_height);
	stage_width = _left_margin + _right_margin + plot_width;
	stage_height = _top_margin + _bottom_margin + plot_height;
	size(stage_width, stage_height);


	current_time = min_time;
	
	noLoop();
	exit();
}

public void load_data(String url){
	SimpleDateFormat ft = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
	// ft.setTimeZone(TimeZone.getTimeZone("GMT"));


	int counter = 0;
	File file = new File(url);
	try {
		FileInputStream fstream = new FileInputStream(file);
		BufferedReader br = new BufferedReader(new InputStreamReader(fstream));
		String line;
		//Read File Line By Line
		while ((line = br.readLine()) != null)   {
			if(counter%100000 == 0){
				println("\tloading data:"+counter);
			}
			if(counter == 0){
				println("header:"+line);
			}else{
				String[] s = split(line, ",");
				try{
					Date t = ft.parse(s[0]);

					int id = Integer.parseInt(s[1]);
					String type = s[2].trim();
					boolean isMovement = type.startsWith("movement")?true:false;
					int x_pos = Integer.parseInt(s[3]);
					int y_pos = Integer.parseInt(s[4]);
					// int index = getIndex(x_pos, y_pos);
					
					// if(counter <100){
					// 	println("Date:"+t+"\t"+id+"\t"+t.getTime()+"\t"+getSeconds(t));
					// }


					//find Person
					Person p = (Person) person_map.get(new Integer(id));
					if(p == null){
						p = new Person(id);
						people.add(p);
						person_map.put(new Integer(id), p);
					}
					//make Movement
					Movement m = new Movement(x_pos, y_pos, isMovement, t);
					//add to person
					p.moves.add(m);
					p.move_map.put(t, m);
				}catch(ParseException e){
					println("Error: parsing:"+line);
				}
			}
		  	counter ++;
		}
		//Close the input stream
		br.close();
	}catch(Exception e){
		e.printStackTrace();
	}
}




public void draw(){

}






public void keyPressed(){
	if(keyCode == LEFT){

	}else if(keyCode == RIGHT){
		println("RIGHT!");
		
	}else if(key=='e'){
		


	}
}





//// utility
//// Date
public int getYear(Date d){
	Calendar cal = GregorianCalendar.getInstance();
	cal.setTime(d);
	return cal.get(Calendar.YEAR);
}
public int getMonth(Date d){
	Calendar cal = Calendar.getInstance();
	cal.setTime(d);
	return cal.get(Calendar.MONTH);
}
public int getDay(Date d){
	Calendar cal = Calendar.getInstance();
	cal.setTime(d);
	return cal.get(Calendar.DAY_OF_MONTH);
}

public int getHour(Date d){
	Calendar cal = GregorianCalendar.getInstance();
	cal.setTime(d);
	return cal.get(Calendar.HOUR_OF_DAY);
}
public int getMin(Date d){
	Calendar cal = GregorianCalendar.getInstance();
	cal.setTime(d);
	return cal.get(Calendar.MINUTE);
}
public int getSec(Date d){
	Calendar cal = GregorianCalendar.getInstance();
	cal.setTime(d);
	return cal.get(Calendar.SECOND);
}

public int getSeconds(Date d){
	// return (int)(d.getTime()/1000);
	long l = d.getTime();
	int result = (int) (l/1000l);
	return result;
}

public long getSeconds_long(Date d){
	// return (int)(d.getTime()/1000);
	long l = d.getTime() / 1000l;
	return l;
}

//2147483647
//1402034432


public Date increment_sec(Date d){
	long t = d.getTime(); //millisecond
	t =  t+1000;
	return new Date(t);
}

public Date increment_sec(Date d, int n){
	long t = d.getTime(); //millisecond
	t =  t+1000*n;
	return new Date(t);
}

public int getDifference(Movement m1, Movement m2){
	return PApplet.parseInt(abs(m1.t.getTime() - m2.t.getTime())/1000l);
}

public float log10 (float x) {
  return (log(x) / log(10));
}

class Cell{
	int x, y;
	int index;
	HashMap<Integer, ArrayList<Person>> moving_people_map;
	ArrayList<Person> checkIn_people;
	ArrayList<Person> stationary_people;

	Rectangle cell_rect;


	boolean isPath = false;
	boolean isAttraction = false;


	Cell(int x, int y){
		this.x = x;
		this.y = y;
		this.cell_rect = new Rectangle(x, y, _cell_width, _cell_height);


		this.checkIn_people = new ArrayList<Person>();
		this.stationary_people = new ArrayList<Person>();
		this.moving_people_map = new HashMap<Integer, ArrayList<Person>>();
	}


	

	//add moving people
	public void moving(Person p, int prev){
		//get ArrayList
		ArrayList<Person> pple = (ArrayList)moving_people_map.get(new Integer(prev));
		if(pple == null){
			pple = new ArrayList<Person>();
			moving_people_map.put(new Integer(prev), pple);
		}
		pple.add(p);
	}

	//add check_in people
	public void checkingIn(Person p, int prev){
		if(!checkIn_people.contains(p)){
			checkIn_people.add(p);
		}
		p.prev_index = prev;
		// println("debug: Check in :"+p.id);
	}


	public void addStationary(Person p, int prev){
		if(!this.stationary_people.contains(p)){
			this.stationary_people.add(p);
		}
		p.prev_index = prev;
	}

	//reset the hashmap after each move
	public void reset_moving(){
		Iterator it = moving_people_map.entrySet().iterator();
		int new_index = getIndex();
	    while (it.hasNext()) {
	        Map.Entry pair = (Map.Entry)it.next();
	        // System.out.println(pair.getKey() + " = " + pair.getValue());
	        ArrayList<Person> array = (ArrayList)pair.getValue();
	        for(Person p : array){
	        	this.stationary_people.add(p);
	        	p.prev_index = new_index;
	        	p.isMoving = false;
	        	p.isStationary = true;
	        	p.isCheckedIn = false;
	        }
	        it.remove(); // avoids a ConcurrentModificationException
	    }
		moving_people_map.clear();
	}

	// find the person and remove from array
	public void remove(Person p){
		if(checkIn_people.contains(p)){
			checkIn_people.remove(p);
			return;
		}
		if(stationary_people.contains(p)){
			stationary_people.remove(p);
		}
	}

	public void render_stationary(PGraphics pg, int c, boolean isLast){
		int stationary_count = stationary_people.size();
		if(stationary_count != 0){
			//20 max
			float pointWeight = constrain(map(stationary_count, 1, 20, _min_dot_size, _max_dot_size), _min_dot_size, _max_dot_size);
			pg.strokeWeight(pointWeight);
			pg.strokeCap(PROJECT);
			pg.stroke(c, 120);
			pg.point((float)cell_rect.getCenterX(), (float)cell_rect.getCenterY());
			// println("debug:stationary:"+getIndex()+":"+stationary_count);
			if(stationary_count > 20 && isLast){
				pg.fill(255);
				pg.textAlign(CENTER, CENTER);
				pg.text(""+stationary_count, (float)cell_rect.getCenterX(), (float)cell_rect.getCenterY());
			}
		}
	}
	public void render_checkIn(PGraphics pg, boolean isLast){
		int checkIn_count = checkIn_people.size();
		if(checkIn_count != 0){
			//100 max
			float pointWeight = constrain(map(checkIn_count, 1, 20, _min_dot_size, _max_dot_size), _min_dot_size, _max_dot_size);
			pg.strokeWeight(1);
			pg.stroke(color_magenta);
			pg.noFill();
			pg.ellipseMode(CENTER);
			pg.ellipse((float)cell_rect.getCenterX(), (float)cell_rect.getCenterY(), pointWeight, pointWeight);
			//draw count
			if(isLast){
				pg.fill(0);
				pg.textAlign(CENTER, BOTTOM);
				pg.text(""+checkIn_count, (float)cell_rect.getCenterX(), (float)cell_rect.getCenterY());
			}
		}	
	}
	public void render_movement(PGraphics pg, int c){
		Iterator it = moving_people_map.entrySet().iterator();
		int new_index = getIndex();
	    while (it.hasNext()) {
	        Map.Entry pair = (Map.Entry)it.next();
	        ArrayList<Person> array = (ArrayList)pair.getValue();
	        Integer integer = (Integer)pair.getKey();
	        int prev_index = integer.intValue();
	        Cell prev_cell = grid.cells[prev_index];
	        float from_x = (float)prev_cell.cell_rect.getCenterX();
	        float from_y = (float)prev_cell.cell_rect.getCenterY();
	        float to_x = (float) this.cell_rect.getCenterX();
	        float to_y = (float)this.cell_rect.getCenterY();

	        int count = array.size();
	        float weight = constrain(map(count, 1, 20, _min_dot_size, _max_dot_size), _min_dot_size, _max_dot_size);

	        pg.strokeWeight(weight);
	        pg.strokeCap(ROUND);
	        pg.stroke(c, 200);
	        pg.line(from_x, from_y, to_x, to_y);
	    }
	}



	public int getX(){
		return index % _grid_size;
	}
	public int getY(){
		return index / _grid_size;
	}

	public int getIndex(){
		return index;
	}
}
class Grid{
	Cell[]  cells;
	boolean isReady = false;

	Grid(){
		int total_cell_count = _grid_size*_grid_size;
		cells = new Cell[total_cell_count];
		//set cell positions
		int runningX = _left_margin;
		int runningY = _top_margin + plot_rect.height;
		int x_counter = 0;
		for(int i = 0; i < total_cell_count; i++){
			int cx = runningX;
			int cy = runningY - _cell_height;

			// println("debug: "+i+"\t"+ cx+"\t"+cy);
			cells[i] = new Cell(cx, cy);
			cells[i].index = i;
			x_counter ++;
			if(x_counter > (_grid_size-1)){
				//next row
				runningX = _left_margin;
				runningY -= (_cell_height + _cell_gap);
				x_counter = 0;
			}else{
				runningX  += _cell_width + _cell_gap;
			}
		}
	}



	// update Grid based on Date
	public void update(Date t){
		// println("debug: Grid.update():"+t.toString());
		for(Person p: people){
			//check if the person has record at given time
			Movement m = (Movement) p.move_map.get(t);
			if(m != null){
				int index = m.getIndex();
				if(m.isMovement){
					// println("\tupdate() : movement:"+p.id+ " index ="+index);
					//making a movement
					int prev_index = p.prev_index;
					if(prev_index == -1){
						println("ERROR: this should not happen: prev_index = -1 for "+p.id + " : "+m.t.toString());
					}else{
						//remove from previous cell
						// println("Debug: update(): prev_index="+prev_index);
						Cell prev_cell = cells[prev_index];
						prev_cell.remove(p);
					}
					//index to move to
					p.isCheckedIn = false;
					p.isMoving = true;
					p.isStationary = false;
					p.isOnMap = true;
					cells[index].moving(p, prev_index);
					p.lastMove_date = t;
				}else{
					// println("\tupdate() : checkIn:"+p.id+ " index ="+index);
					//check_in event
					int prev_index = p.prev_index;
					if(prev_index == -1){
						//first time check in
						p.isCheckedIn = true;
						p.isMoving = false;
						p.isStationary = false;
						p.isOnMap = true;
						cells[index].checkingIn(p, index);
						p.checkIn_date = t;
					}else{
						//remove from previous cell
						Cell prev_cell = cells[prev_index];
						prev_cell.remove(p);
						p.isCheckedIn = true;
						p.isMoving = false;
						p.isStationary = false;
						p.isOnMap = true;
						cells[index].checkingIn(p, index);
						p.checkIn_date = t;
					}
				}
			}else{
				if(p.isOnMap){
					if(p.isCheckedIn){
						//checked in
						// println("debug: checkedIn:"+p.id);
					}else if(p.isMoving){		
						// println("Error: debug: Moving:"+p.id);				
					}else{
						//still not moving
						// println("debug: stationary:"+p.id);
					}

				}
			}
		}
	}


	//clear assigned people
	public void reset(){
		for(Cell cell:cells){
			//interate through moving_people_map to update to stationay
			cell.reset_moving();

		}
	}

	public void time_reset(Date t){
		for(Cell cell:cells){
			cell.moving_people_map.clear();

			ArrayList<Person> toRemove = new ArrayList<Person>();
			for(Person p: cell.checkIn_people){
				if(p.checkIn_date.after(t)){
					toRemove.add(p);
				}
			}
			cell.checkIn_people.removeAll(toRemove);

			toRemove.clear();
			for(Person p: cell.stationary_people){
				if(p.lastMove_date.after(t)){
					toRemove.add(p);
				}
			}
			cell.stationary_people.removeAll(toRemove);
		}
	}

}
class Movement implements Comparable<Movement>{
	int x, y;
	boolean isMovement = false;
	Date t;


	Movement(int x, int y, boolean isMovement, Date t){
		this.x = x;
		this.y = y;
		this.isMovement = isMovement;
		this.t = t;
	}


	public int getIndex(){
		return x + y*_grid_size ;
	}

	public int compareTo(Movement that){
		if(this.t.after(that.t)){
			return 1;
		}else if(this.t.before(that.t)){
			return -1;
		}else{
			if(this.isMovement && !that.isMovement){
				return -1;
			}else if(!this.isMovement && that.isMovement){
				return 1;
			}
			return 0;
		}
	}

	public String toString(){
		return "("+x+","+y+") "+(isMovement?"move":"check-in")+" : "+t.toString();
	}
	public String toStringPos(){
		return "("+x+","+y+")";
	}

	public int getSeconds(){
		long l = t.getTime();
		int result = (int) (l/1000l);
		return result;
	}

	public long getSeconds_long(){
		// return (int)(d.getTime()/1000);
		long l = t.getTime() / 1000l;
		return l;
	}

	
}
class Person{
	int id;
	ArrayList<Movement> moves;
	HashMap<Date, Movement> move_map;
	boolean isCheckedIn = false;
	boolean isStationary = false;
	boolean isMoving = false;
	boolean isOnMap = false;

	Date checkIn_date = null;
	Date lastMove_date = null;

	int prev_index = -1;

	ArrayList<String> move_output;

	Person(int id){
		this.id = id;
		this.moves = new ArrayList<Movement>();
		this.move_map = new HashMap<Date, Movement>();
	}

	

	public int getDifference(Movement m1, Movement m2){
		return PApplet.parseInt(abs(m1.t.getTime() - m2.t.getTime())/1000);
	}

}
class Spot{
	int x, y;
	int total_min = 0;
	float ave_min = 0f;
	HashSet<Person> unique_people;
	HashSet<Person> unique_people_stationary;
	int check_in_count = 0;
	int movement_count = 0;

	Spot(int x, int y){
		this.x = x;
		this.y = y;
		this.unique_people = new HashSet<Person>();
		this.unique_people_stationary = new HashSet<Person>();
	}
}
  static public void main(String[] passedArgs) {
    String[] appletArgs = new String[] { "Data_Parser_5" };
    if (passedArgs != null) {
      PApplet.main(concat(appletArgs, passedArgs));
    } else {
      PApplet.main(appletArgs);
    }
  }
}
