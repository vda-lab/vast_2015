//Movement summary

import java.io.FileInputStream;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.*;
import java.awt.*;
import java.awt.event.*;
import java.text.*;

int _margin = 10;
int _left_margin = _margin*3;
int _right_margin = _margin*3;
int _top_margin = _margin*3;
int _bottom_margin = _margin*3;
int _grid_size = 100;


int _cell_width = 8;
int _cell_height = 8;
int _cell_gap = 0;

int _min_dot_size = 3;
int _max_dot_size = 20;


int plot_width, plot_height;
Rectangle plot_rect;
int stage_width, stage_height;

PFont font;


ArrayList<Person> people;
HashMap<Integer, Person> person_map;
Date min_time, max_time;
Date current_time;
Date seq_start, seq_end;

Grid grid;

int color_magenta = color(231,41,138);
int color_blue = color(117,112,179);
// int[] color_blue_array = {color(255,247,251),color(236,231,242),color(208,209,230),color(166,189,219),color(116,169,207),color(54,144,192),color(5,112,176),color(4,90,141),color(2,56,88)};
int[] color_blue_array = {color(222,235,247),color(222,235,247),color(222,235,247),color(222,235,247),color(222,235,247),color(222,235,247),
						color(198,219,239),color(198,219,239),color(198,219,239),color(198,219,239),color(198,219,239),
						color(158,202,225),color(158,202,225),color(158,202,225),color(158,202,225),
						color(107,174,214),color(107,174,214),
						color(66,146,198),color(66,146,198),
						color(33,113,181),
						color(8,81,156)};


int img_counter = 0;
boolean is_export_mode = false;


PGraphics pg_map;
PGraphics[] pg_array;
PGraphics[] pg_next_half;
int sequence_length = 60; //seconds


int[][] x_pos_matrix, y_pos_matrix;
long start_m_sec, end_m_sec;  //milliseconds
int interval = 60*1000; // one minute
int total_minutes;

int time_window = 12; //10 seonds  ////////////////// *************** values to adjust

String data_day = "Fri";
// String data_day = "Sat";
// String data_day = "Sun";




HashMap<String, Spot> spot_map;
SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

void setup(){
	font = createFont("SUPERNAT1001.TTF", 10);
	textFont(font, 10);
	smooth();

	people = new ArrayList<Person>();
	person_map = new HashMap<Integer, Person>();

 	//Change the path to the data
	load_data("/Path/to/data/park-movement-Fri-FIXED-2.0.csv");

	println("Debug: number of unique person: "+people.size());

	//find min_time and max_time
	min_time = people.get(0).moves.get(0).t;
	max_time = min_time;
	for(Person p: people){
		int move_counts = p.moves.size();
		Date p_min = p.moves.get(0).t;
		Date p_max = p.moves.get(move_counts-1).t;

		min_time = (min_time.after(p_min)? p_min:min_time);
		max_time = (max_time.before(p_max)? p_max:max_time);
	}

	println("Debug: min_time ="+min_time);
	println("Debug: max_time ="+max_time);

	// clean start date
	GregorianCalendar min_cal = new GregorianCalendar(getYear(min_time), getMonth(min_time),
		getDay(min_time), getHour(min_time), getMin(min_time));
	GregorianCalendar max_cal = new GregorianCalendar(getYear(max_time), getMonth(max_time),
		getDay(max_time), getHour(max_time), getMin(max_time)+1);

	// long min_sec = min_cal.getTime();
	println("Debug: clean min:"+ min_cal.getTime());
	println("Debug: clean max:"+ max_cal.getTime());

	start_m_sec = min_cal.getTimeInMillis();
	end_m_sec = max_cal.getTimeInMillis();
	total_minutes = ((int)(end_m_sec - start_m_sec)/interval) +1;
	println("Debug: total munites = "+total_minutes);


	for(int i = 0; i<people.size(); i++){
		Person p = people.get(i);
		p.move_output = new ArrayList<String>(); // to store output text
		Collections.sort(p.moves);

		Movement first_move = p.moves.get(0);
		Movement prev_move = p.moves.get(0);
		float sum_distance = 0;
		float sum_sec = 0;
		//perse by person
		for(int j = 1; j<p.moves.size(); j++){
			Movement m = p.moves.get(j);
			float distance = dist(prev_move.x, prev_move.y, m.x, m.y);
			float seconds = m.getSeconds() - prev_move.getSeconds();
				
			if(first_move != prev_move){
				if(seconds <= time_window){
					if(m.isMovement){
						//move on
						sum_distance += distance;
						sum_sec += seconds;
					}else{
						//checkin
						//first to m
						float speed = sum_distance/sum_sec;
						if(sum_sec ==0f){
							speed = 0;
						}
						String result = p.id+","+first_move.x+","+first_move.y+","+m.x+","+m.y+","+first_move.getSeconds_long()+","+m.getSeconds_long()+","+sum_distance+","+sum_sec+","+(speed)+",movement";
						p.move_output.add(result);
						//reset
						sum_distance = 0;
						sum_sec = 0;
						first_move = m;
					}
				}else{
					//long travel
					//save first to prev
					float speed = sum_distance/sum_sec;
					if(sum_sec ==0f){
						speed = 0;
					}
					String result = p.id+","+first_move.x+","+first_move.y+","+prev_move.x+","+prev_move.y+","+first_move.getSeconds_long()+","+prev_move.getSeconds_long()+","+sum_distance+","+sum_sec+","+(speed)+",movement";
					p.move_output.add(result);
					sum_distance = 0;
					sum_sec = 0;

					sum_distance += distance;
					sum_sec += seconds;
					//save prev to m  // slow movement
					speed = sum_distance/sum_sec;
					if(sum_sec ==0f){
						speed = 0;
					}
					result = p.id+","+prev_move.x+","+prev_move.y+","+m.x+","+m.y+","+prev_move.getSeconds_long()+","+m.getSeconds_long()+","+sum_distance+","+sum_sec+","+(speed)+",movement";
					p.move_output.add(result);
					//reset
					sum_distance = 0;
					sum_sec = 0;
					first_move = m;
				}
			}else{
				if(prev_move.isMovement){
					//no previous move
					if(seconds <= time_window){
						if(m.isMovement){
							//move on
							sum_distance += distance;
							sum_sec += seconds;
						}else{
							//checkin
							//prev to m
							float speed = sum_distance/sum_sec;
							if(sum_sec ==0f){
								speed = 0;
							}
							String result = p.id+","+prev_move.x+","+prev_move.y+","+m.x+","+m.y+","+prev_move.getSeconds_long()+","+m.getSeconds_long()+","+sum_distance+","+sum_sec+","+(speed)+",movement";
							p.move_output.add(result);
							//reset
							sum_distance = 0;
							sum_sec = 0;
							first_move = m;
						}
					}else{
						//long travel
						sum_distance += distance;
						sum_sec += seconds;
						float speed = sum_distance/sum_sec;
						if(sum_sec ==0f){
							speed = 0;
						}

						//save prev to m  // slow movement
						String result = p.id+","+prev_move.x+","+prev_move.y+","+m.x+","+m.y+","+prev_move.getSeconds_long()+","+m.getSeconds_long()+","+sum_distance+","+sum_sec+","+speed+",movement";
						p.move_output.add(result);
						//reset
						sum_distance = 0;
						sum_sec = 0;
						first_move = m;
					}
				}else{
					//checkin
					sum_distance += distance;
					sum_sec += seconds;
					//save prev to m  // slow movement
					String result = p.id+","+prev_move.x+","+prev_move.y+","+m.x+","+m.y+","+prev_move.getSeconds_long()+","+m.getSeconds_long()+","+sum_distance+","+sum_sec+","+0+",checkin";
					p.move_output.add(result);
					//reset
					sum_distance = 0;
					sum_sec = 0;
					first_move = m;
				}
			}
			prev_move = m;
		}

		println("Debug:"+p.id+" has "+p.move_output.size()+" moves");
		// for(String out :p.move_output){
		// 	println("\t"+out.replaceAll(",", "\t"));
		// }
		//	179386	44	25	45	26	1402053694	1402055998	1.4142135	2304.0	6.1380793E-4	movement

	}

	//write a file
	PrintWriter output = createWriter("movement_summary_"+data_day+".csv");
	// PrintWriter output = createWriter("movement_summary_sample.csv");

	for(Person p: people){
		for(String out :p.move_output){
			// println("\t"+out.replaceAll(",", "\t"));
			output.println(out);
		}
	}
	output.flush();
	output.close();




	//setup stage
	plot_width = (_grid_size*_cell_width) + (_grid_size-1)*_cell_gap;
	plot_height = (_grid_size*_cell_height) + (_grid_size-1)*_cell_gap;
	plot_rect = new Rectangle(_left_margin, _top_margin, plot_width, plot_height);
	stage_width = _left_margin + _right_margin + plot_width;
	stage_height = _top_margin + _bottom_margin + plot_height;
	size(stage_width, stage_height);


	current_time = min_time;
	
	noLoop();
	exit();
}

void load_data(String url){
	SimpleDateFormat ft = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
	// ft.setTimeZone(TimeZone.getTimeZone("GMT"));


	int counter = 0;
	File file = new File(url);
	try {
		FileInputStream fstream = new FileInputStream(file);
		BufferedReader br = new BufferedReader(new InputStreamReader(fstream));
		String line;
		//Read File Line By Line
		while ((line = br.readLine()) != null)   {
			if(counter%100000 == 0){
				println("\tloading data:"+counter);
			}
			if(counter == 0){
				println("header:"+line);
			}else{
				String[] s = split(line, ",");
				try{
					Date t = ft.parse(s[0]);

					int id = Integer.parseInt(s[1]);
					String type = s[2].trim();
					boolean isMovement = type.startsWith("movement")?true:false;
					int x_pos = Integer.parseInt(s[3]);
					int y_pos = Integer.parseInt(s[4]);
					// int index = getIndex(x_pos, y_pos);
					
					// if(counter <100){
					// 	println("Date:"+t+"\t"+id+"\t"+t.getTime()+"\t"+getSeconds(t));
					// }


					//find Person
					Person p = (Person) person_map.get(new Integer(id));
					if(p == null){
						p = new Person(id);
						people.add(p);
						person_map.put(new Integer(id), p);
					}
					//make Movement
					Movement m = new Movement(x_pos, y_pos, isMovement, t);
					//add to person
					p.moves.add(m);
					p.move_map.put(t, m);
				}catch(ParseException e){
					println("Error: parsing:"+line);
				}
			}
		  	counter ++;
		}
		//Close the input stream
		br.close();
	}catch(Exception e){
		e.printStackTrace();
	}
}




void draw(){

}






void keyPressed(){
	if(keyCode == LEFT){

	}else if(keyCode == RIGHT){
		println("RIGHT!");
		
	}else if(key=='e'){
		


	}
}





//// utility
//// Date
int getYear(Date d){
	Calendar cal = GregorianCalendar.getInstance();
	cal.setTime(d);
	return cal.get(Calendar.YEAR);
}
int getMonth(Date d){
	Calendar cal = Calendar.getInstance();
	cal.setTime(d);
	return cal.get(Calendar.MONTH);
}
int getDay(Date d){
	Calendar cal = Calendar.getInstance();
	cal.setTime(d);
	return cal.get(Calendar.DAY_OF_MONTH);
}

int getHour(Date d){
	Calendar cal = GregorianCalendar.getInstance();
	cal.setTime(d);
	return cal.get(Calendar.HOUR_OF_DAY);
}
int getMin(Date d){
	Calendar cal = GregorianCalendar.getInstance();
	cal.setTime(d);
	return cal.get(Calendar.MINUTE);
}
int getSec(Date d){
	Calendar cal = GregorianCalendar.getInstance();
	cal.setTime(d);
	return cal.get(Calendar.SECOND);
}

int getSeconds(Date d){
	// return (int)(d.getTime()/1000);
	long l = d.getTime();
	int result = (int) (l/1000l);
	return result;
}

long getSeconds_long(Date d){
	// return (int)(d.getTime()/1000);
	long l = d.getTime() / 1000l;
	return l;
}

//2147483647
//1402034432


Date increment_sec(Date d){
	long t = d.getTime(); //millisecond
	t =  t+1000;
	return new Date(t);
}

Date increment_sec(Date d, int n){
	long t = d.getTime(); //millisecond
	t =  t+1000*n;
	return new Date(t);
}

int getDifference(Movement m1, Movement m2){
	return int(abs(m1.t.getTime() - m2.t.getTime())/1000l);
}

float log10 (float x) {
  return (log(x) / log(10));
}

