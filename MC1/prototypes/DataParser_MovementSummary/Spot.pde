class Spot{
	int x, y;
	int total_min = 0;
	float ave_min = 0f;
	HashSet<Person> unique_people;
	HashSet<Person> unique_people_stationary;
	int check_in_count = 0;
	int movement_count = 0;

	Spot(int x, int y){
		this.x = x;
		this.y = y;
		this.unique_people = new HashSet<Person>();
		this.unique_people_stationary = new HashSet<Person>();
	}
}
