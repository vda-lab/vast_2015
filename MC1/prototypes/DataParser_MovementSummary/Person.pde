class Person{
	int id;
	ArrayList<Movement> moves;
	HashMap<Date, Movement> move_map;
	boolean isCheckedIn = false;
	boolean isStationary = false;
	boolean isMoving = false;
	boolean isOnMap = false;

	Date checkIn_date = null;
	Date lastMove_date = null;

	int prev_index = -1;

	ArrayList<String> move_output;

	Person(int id){
		this.id = id;
		this.moves = new ArrayList<Movement>();
		this.move_map = new HashMap<Date, Movement>();
	}

	

	int getDifference(Movement m1, Movement m2){
		return int(abs(m1.t.getTime() - m2.t.getTime())/1000);
	}

}
