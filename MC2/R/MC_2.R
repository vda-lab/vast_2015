library(dplyr)
library(ggplot2)
library(lubridate)
library(igraph)
library(grid)
library(scales)
library(RColorBrewer)
library(tidyr)

#large volume of communications
load("com_fri.RData")
load("com_sat.RData")
load("com_sun.RData")

com_fri <- friday %>%
  select(from, to, location, timestamp)
graph <- graph.data.frame(com_fri, directed = TRUE)
#Hub and Authority score
HS <- hub.score(graph)$vector
AS <- authority.score(graph)$vector
#scatterplot
scores = data.frame(V(graph)$name, AS, HS)
colnames(scores) = c("id", "AS", "HS")
scores$id = as.character(scores$id)
#plot
p_fri <- ggplot(scores, aes(x = AS, y = HS)) +geom_point(shape = 1) + 
  geom_text(data= scores %>% filter(AS > 0.1), aes(label = id), color= "red", hjust=1, vjust=0)+
  ggtitle("Hub and Authority score on Friday")+
  coord_fixed(xlim = c(-0.1, 1.1), ylim= c(-0.1, 1.1))

#saturday
com_sat <- saturday %>%
  select(from, to, location, timestamp)
graph <- graph.data.frame(com_sat, directed = TRUE)
#Hub and Authority score
HS <- hub.score(graph)$vector
AS <- authority.score(graph)$vector
#scatterplot
scores = data.frame(V(graph)$name, AS, HS)
colnames(scores) = c("id", "AS", "HS")
scores$id = as.character(scores$id)
#plot
p_sat <- ggplot(scores, aes(x = AS, y = HS)) +geom_point(shape = 1) + 
  geom_text(data= scores %>% filter(AS > 0.08), aes(label = id), color= "red", hjust=1, vjust=0)+
  ggtitle("Hub and Authority score on Saturday")+
  coord_fixed(xlim = c(-0.1, 1.1), ylim= c(-0.1, 1.1))

#sunday
com_sun <- sunday %>%
  select(from, to, location, timestamp)
graph <- graph.data.frame(com_sun, directed = TRUE)
#Hub and Authority score
HS <- hub.score(graph)$vector
AS <- authority.score(graph)$vector
#scatterplot
scores = data.frame(V(graph)$name, AS, HS)
colnames(scores) = c("id", "AS", "HS")
scores$id = as.character(scores$id)
#plot
p_sun <- ggplot(scores, aes(x = AS, y = HS)) +geom_point(shape = 1) + 
  geom_text(data= scores %>% filter(AS > 0.1), aes(label = id), color= "red", hjust=1, vjust=0)+
  ggtitle("Hub and Authority score on Sunday")+
  coord_fixed(xlim = c(-0.1, 1.1), ylim= c(-0.1, 1.1))


#compose
multiplot(p_fri, p_sat, p_sun, cols=3)

#large communication
ggplot(scores, aes(x = AS, y = HS)) +geom_point(shape = 1) + 
  geom_text(data= scores %>% filter(AS > 0.06), aes(label = id), color= "red", hjust=0, vjust=0)+
  ggtitle("Hub and Authority score on Sunday")+
  coord_fixed(xlim = c(-0.1, 0.3), ylim= c(-0.1, 0.3))


#make a histogram on incoming and out going per day 1278894
sub_fri<-friday %>%
  filter(from == "1278894" | to =="1278894") %>%
  mutate(day = "Friday")
sub_sat <-saturday %>%
  filter(from == "1278894" | to =="1278894") %>%
  mutate(day = "Saturday")
sub_sun <- sunday %>%
  filter(from == "1278894" | to =="1278894") %>%
  mutate(day = "Sunday")
sub_all <- bind_rows(sub_fri, sub_sat, sub_sun)

sub_all<-sub_all %>%
  mutate(type = ifelse(from == "1278894", "outgoing", "incoming" ))
# ggplot(sub_all, aes(x=timestamp)) + geom_histogram(binwidth= 60) + facet_grid(type+day~.)
ggplot(sub_sun, aes(x=timestamp)) + geom_histogram(binwidth= 60)

sub_all %>%
  group_by(location, type) %>%
  summarise(count = n())


p1 = ggplot(sub_all %>% filter(day =="Friday"), aes(timestamp))+ geom_histogram(binwidth = 60)+ 
  coord_cartesian(ylim = c(0, 1600), xlim = c(ymd_hm("2014-06-06 07:30"), ymd_hm("2014-06-06 24:00"))) + 
  facet_grid(type ~.)+ xlab("Friday")

p2 = ggplot(sub_all %>% filter(day =="Saturday"), aes(timestamp))+ geom_histogram(binwidth = 60)+ 
  coord_cartesian(ylim = c(0, 1600), xlim = c(ymd_hm("2014-06-07 07:30"), ymd_hm("2014-06-07 24:00"))) + 
  xlab("Saturday")+ 
  facet_grid(type ~.)

p3 = ggplot(sub_all %>% filter(day =="Sunday"), aes(timestamp))+ geom_histogram(binwidth = 60)+ 
  coord_cartesian(ylim = c(0,1600), xlim = c(ymd_hm("2014-06-08 07:30"), ymd_hm("2014-06-08 24:00"))) + 
  xlab("Sunday")+ 
  facet_grid(type ~.)

multiplot(p1, p2, p3, cols = 1)


#make a histogram on incoming and out going per day 839736
target = "839736"
sub_fri<-friday %>%
  filter(from == target | to ==target) %>%
  mutate(day = "Friday")
sub_sat <-saturday %>%
  filter(from == target | to ==target) %>%
  mutate(day = "Saturday")
sub_sun <- sunday %>%
  filter(from == target | to ==target) %>%
  mutate(day = "Sunday")
sub_all <- bind_rows(sub_fri, sub_sat, sub_sun)
sub_all<-sub_all %>%
  mutate(type = ifelse(from == target, "outgoing", "incoming" ))

sub_all %>%
  group_by(location, type) %>%
  summarise(count = n())

p1 = ggplot(sub_all %>% filter(day =="Friday"), aes(timestamp))+ geom_histogram(binwidth = 60)+ 
  coord_cartesian(xlim = c(ymd_hm("2014-06-06 07:30"), ymd_hm("2014-06-06 24:00"))) + 
  facet_grid(type ~.)+ xlab("Friday")

p2 = ggplot(sub_all %>% filter(day =="Saturday"), aes(timestamp))+ geom_histogram(binwidth = 60)+ 
  coord_cartesian( xlim = c(ymd_hm("2014-06-07 07:30"), ymd_hm("2014-06-07 24:00"))) + 
  xlab("Saturday")+ 
  facet_grid(type ~.)

p3 = ggplot(sub_all %>% filter(day =="Sunday"), aes(timestamp))+ geom_histogram(binwidth = 60)+ 
  coord_cartesian(xlim = c(ymd_hm("2014-06-08 07:30"), ymd_hm("2014-06-08 24:00"))) + 
  xlab("Sunday")+ 
  facet_grid(type ~.)

multiplot(p1, p2, p3, cols = 1)

#make a histogram on incoming and out going per day 839736
target = "0"
sub_fri<-friday %>%
  filter(from == target | to ==target) %>%
  mutate(day = "Friday")
sub_sat <-saturday %>%
  filter(from == target | to ==target) %>%
  mutate(day = "Saturday")
sub_sun <- sunday %>%
  filter(from == target | to ==target) %>%
  mutate(day = "Sunday")
sub_all <- bind_rows(sub_fri, sub_sat, sub_sun)
sub_all<-sub_all %>%
  mutate(type = ifelse(from == target, "outgoing", "incoming" ))

p1 = ggplot(sub_all %>% filter(day =="Friday"), aes(timestamp))+ geom_histogram(binwidth = 60)+ 
  coord_cartesian(xlim = c(ymd_hm("2014-06-06 07:30"), ymd_hm("2014-06-06 24:00"))) + 
  facet_grid(type ~.)+ xlab("Friday")

p2 = ggplot(sub_all %>% filter(day =="Saturday"), aes(timestamp))+ geom_histogram(binwidth = 60)+ 
  coord_cartesian( xlim = c(ymd_hm("2014-06-07 07:30"), ymd_hm("2014-06-07 24:00"))) + 
  xlab("Saturday")+ 
  facet_grid(type ~.)

p3 = ggplot(sub_all %>% filter(day =="Sunday"), aes(timestamp))+ geom_histogram(binwidth = 60)+ 
  coord_cartesian(xlim = c(ymd_hm("2014-06-08 07:30"), ymd_hm("2014-06-08 24:00"))) + 
  xlab("Sunday")+ 
  facet_grid(type ~.)

multiplot(p1, p2, p3, cols = 1)



##### MC2.2 #######
# 10 patterns
# The peak of communication to "external on sunday before 12:00
load("com_pos_sun.RData")
to_external  <- com_sun %>%
  filter(to == 0)

ggplot(to_external, aes(x= timestamp)) +geom_histogram(binwidth = 60)+
  scale_x_datetime(breaks = seq(as.POSIXct(strptime("2014-06-08 08:00:00", "%Y-%m-%d %H:%M:%S")), as.POSIXct(strptime("2014-06-08 24:00:00", "%Y-%m-%d %H:%M:%S")), by = 60*30),
                   labels = date_format("%H:%M"))

peak <-  to_external %>%
  mutate(hour = hour(timestamp)) %>%
  mutate(mins = minute(timestamp)) %>%
  group_by(hour, mins) %>%
  summarise(count = n(), min = min(timestamp)) %>%
  filter(count  > 100) %>%
  arrange(min)
min(peak$min) #"2014-06-08 11:45:00 UTC"
max(peak$min) #"2014-06-08 11:59:00 UTC"

subset <- to_external %>%
  filter(timestamp >= as.POSIXct(strptime("2014-06-08 11:45:00", "%Y-%m-%d %H:%M:%S"), tz="UTC")) %>%
  filter(timestamp < as.POSIXct(strptime("2014-06-08 12:00:00", "%Y-%m-%d %H:%M:%S"), tz="UTC"))

sum_subset <- subset %>%
  group_by(from_x, from_y) %>%
  summarise(count = n())

# where are these comunication being sent from
load("map_pos.RData")
map <- ggplot(all_pos)+ geom_rect(aes(xmin=X, ymin=Y, xmax=X+1, ymax=Y+1), fill="white") + coord_fixed(xlim = c(-2, 102), ylim = c(-2, 102))
map + geom_rect(data = sum_subset, aes(xmin = from_x, ymin = from_y, xmax = from_x+1, ymax = from_y+1, fill = count))+
  scale_fill_gradient(low ="#fff5f0", high="red")

?scale_fill_gradient
?cut
# discrete scale
cut(sum_subset$count, breaks = 500*(0:6), labels=FALSE)
sum_subset$label <- cut(sum_subset$count, breaks = 500*(0:6), labels=c("~500", "~1000", "~1500","~2000", "~2500", "~3000"))
map + geom_rect(data = sum_subset, aes(xmin = from_x, ymin = from_y, xmax = from_x+1, ymax = from_y+1, fill = label))+
  scale_fill_manual(values = brewer.pal(4, "Reds")) + ggtitle("Communication counts to external\nbetween 11:45 and 12:00 on Sunday")+
  annotate("text", x = 32, y=31, label ="(32, 33)\n(31, 34)\n(32,34)", vjust = 1, cex= 4)
  
# sum_subset$label <- cut(sum_subset$count, breaks = c(0, 100, 500*(1:6)), labels=c("~100", "~500", "~1000", "~1500","~2000", "~2500", "~3000"))
# map + geom_rect(data = sum_subset, aes(xmin = from_x, ymin = from_y, xmax = from_x+1, ymax = from_y+1, fill = factor(label)))+
#   scale_fill_manual(values = brewer.pal(6, "Reds")) +
#   ggtitle("Communication origin counts\nbetween 11:45 and 12:00 on Sunday")
sum_subset %>%
  ungroup %>%
  arrange(desc(count))

# who are these people?
who <- subset %>%
  filter(from_x >= 31 & from_x <=32 & from_y >= 33, from_y <= 34) %>%
  group_by(from) %>%
  summarise(count = n()) %>%
  ungroup() %>%
  arrange(desc(count))

#Hub and Authority score
HS <- hub.score(graph)$vector
AS <- authority.score(graph)$vector
#scatterplot
scores = data.frame(V(graph)$name, AS, HS)
colnames(scores) = c("id", "AS", "HS")
scores$id = as.character(scores$id)
#plot
ggplot(scores, aes(x = AS, y = HS)) +geom_point(shape = 1) + 
  geom_text(data= scores %>% top_n(5, HS), aes(label = id), color= "red", hjust=1, vjust=0)+
  ggtitle("Hub and Authority score on Sunday")+
  coord_fixed(xlim = c(-0.1, 1.1), ylim= c(-0.1, 1.1))

ggplot(scores, aes(x = AS, y = HS)) +geom_point(shape = 1) + 
  geom_text(data= scores %>% top_n(1,AS), label = "external", color= "red", hjust=1, vjust=0)+
  ggtitle("Hub and Authority score on Sunday")+
  coord_fixed(xlim = c(-0.1, 1.1), ylim= c(-0.1, 1.1))


scores %>%
  filter(HS >0.25) %>%
  nrow()
who2 <- scores %>%
  filter(HS > 0.25) %>%
  select(id) %>%
  t() %>%
  as.vector()
who2
who3 <- scores %>%
  filter(HS < 0.25) %>%
  select(id) %>%
  t() %>%
  as.vector()



#communication pattern of these individual
com_subset <- com_sun %>%
  filter(from %in%who$from | to %in% who$from) %>%
  select(from, to, timestamp, location, from_x, from_y, to_x, to_y) %>%
  filter(to != 0) %>%
  filter(timestamp >= as.POSIXct(strptime("2014-06-08 09:00:00", "%Y-%m-%d %H:%M:%S"), tz="UTC")) %>%
  filter(timestamp < as.POSIXct(strptime("2014-06-08 12:00:00", "%Y-%m-%d %H:%M:%S"), tz="UTC"))

com_subset <- com_sun %>%
  filter(from %in%who$from & to %in% who$from) %>%
  select(from, to, timestamp, location, from_x, from_y, to_x, to_y) %>%
  filter(to != 0) %>%
  filter(timestamp >= as.POSIXct(strptime("2014-06-08 09:00:00", "%Y-%m-%d %H:%M:%S"), tz="UTC")) %>%
  filter(timestamp < as.POSIXct(strptime("2014-06-08 12:00:00", "%Y-%m-%d %H:%M:%S"), tz="UTC"))
#v3
com_subset <- com_sun %>%
  filter(from %in%who2 & to %in% who2) %>%
  select(from, to, timestamp, location, from_x, from_y, to_x, to_y) %>%
  filter(to != 0) %>%
  filter(timestamp >= as.POSIXct(strptime("2014-06-08 09:00:00", "%Y-%m-%d %H:%M:%S"), tz="UTC")) %>%
  filter(timestamp < as.POSIXct(strptime("2014-06-08 12:00:00", "%Y-%m-%d %H:%M:%S"), tz="UTC"))
#v4
com_subset <- com_sun %>%
  filter(from %in%who3 & to %in% who3) %>%
  select(from, to, timestamp, location, from_x, from_y, to_x, to_y) %>%
  filter(to != 0) %>%
  filter(timestamp >= as.POSIXct(strptime("2014-06-08 09:00:00", "%Y-%m-%d %H:%M:%S"), tz="UTC")) %>%
  filter(timestamp < as.POSIXct(strptime("2014-06-08 12:00:00", "%Y-%m-%d %H:%M:%S"), tz="UTC"))


com_subset_summary <- com_subset %>%
  group_by(from, to) %>%
  summarise(count = n()) %>%
  ungroup() %>%
  filter(count >1)
#graph
graph = graph.data.frame(com_subset_summary, directed = TRUE)
graph = graph.data.frame(com_subset, directed = TRUE)

plot(graph, vertex.size=5, 
     vertex.label.cex = 0.8, vertex.label.color="black", vertex.label.dist =0.4, vertex.label.family ="sans",
     edge.arrow.size = 0.2, 
     main="Communication")

par(mar= c(0,0,3,0))
graph$layout = layout.fruchterman.reingold(graph)
graph$layout = layout.graphopt(graph)
#style graph
V(graph)$color = "white"
# V(graph)$color = palette[match(scores$group_id[match(V(graph)$name, scores$id)], group_labels)]
V(graph)$size = 3
V(graph)$label.color ="black"
V(graph)$label.dist=0.4
V(graph)$label.cex = 0.8
V(graph)$label.family ="sans"
V(graph)$label = ""

E(graph)$shape = "circle"
E(graph)$arrow.size=0.2
E(graph)$color = "grey60"

plot(graph, main="communicaiton among those are at pavilion\nbetween 9 to 12 at the 3 positions")


# vcount(graph)
# clique.number(graph)
graph = graph.data.frame(com_subset_summary, directed = FALSE)
# independence.number(graph)
# largest  <- induced.subgraph(graph, largest.independent.vertex.sets(graph)[[1]])
#### too slow

ebc <- edge.betweenness.community(graph) 
plot(ebc, graph)
clusters <- data.frame(V(graph)$name, ebc$membership)
colnames(clusters) <- c("id", "membership")
table(ebc$membership)
#remove small cluster
keep_mem <- clusters %>%
  group_by(membership) %>%
  summarise(count = n()) %>%
  filter(count >3) %>%
  select(membership) %>%
  t() %>%
  as.vector() %>%
  as.character()
keep_id <- clusters %>%
  filter(membership %in% keep_mem)


sub_graph <- delete.vertices(graph,which(!V(graph)$name %in% keep_id$id))
plot(sub_graph)
ebc2 <- edge.betweenness.community(sub_graph) 
plot(ebc2, sub_graph)


####### decompose graph and select the largest subgraph ##############################
decomp <- decompose.graph(sub_graph)
largest <- decomp[[which.max(sapply(decomp, vcount))]]

plot(largest)
members <- V(largest)$name
dput(members)
c("7478", "20098", "27471", "47327", "64304", "81090", "170456", 
  "174727", "174974", "195725", "211109", "211669", "221553", "230313", 
  "240876", "264321", "328657", "365108", "377275", "408230", "412874", 
  "427170", "542027", "576974", "583339", "584464", "590484", "603369", 
  "634880", "710974", "721817", "746574", "813863", "829943", "871333", 
  "873143", "896617", "910832", "1019339", "1070569", "1120259", 
  "1128580", "1147181", "1148774", "1174266", "1180332", "1196711", 
  "1217112", "1246261", "1264352", "1324567", "1392907", "1399218", 
  "1411980", "1412390", "1430595", "1445696", "1512355", "1559001", 
  "1580679", "1615382", "1623035", "1648751", "1669211", "1687201", 
  "1713404", "1721744", "1725194", "1770473", "1814974", "1882328", 
  "1884134", "1890620", "1906865", "1937540", "1970360", "2002193", 
  "2028377", "2039599", "2040751", "2063740", "2090883", "31755", 
  "536330", "1738952")



com_subset <- com_sun %>%
  filter(from %in%members & to %in% members) %>%
  select(from, to, timestamp, location, from_x, from_y, to_x, to_y) %>%
  filter(to != 0) %>%
  filter(timestamp >= as.POSIXct(strptime("2014-06-08 09:00:00", "%Y-%m-%d %H:%M:%S"), tz="UTC")) %>%
  filter(timestamp < as.POSIXct(strptime("2014-06-08 12:00:00", "%Y-%m-%d %H:%M:%S"), tz="UTC"))

com_subset_summary <- com_subset %>%
  group_by(from, to) %>%
  summarise(count = n()) %>%
  ungroup() %>%
  filter(count >1)
#graph
graph = graph.data.frame(com_subset_summary, directed = TRUE)

graph$layout = layout.fruchterman.reingold(graph)
#style graph
V(graph)$color = "white"
# V(graph)$color = palette[match(scores$group_id[match(V(graph)$name, scores$id)], group_labels)]
V(graph)$size = 3
V(graph)$label.color ="black"
V(graph)$label.dist=0.3
V(graph)$label.cex = 0.6
V(graph)$label.family ="sans"
V(graph)$label = ""
V(graph)$label = V(graph)$name

E(graph)$shape = "circle"
E(graph)$arrow.size=0.2
E(graph)$color = "grey60"

plot(graph, main="communicaiton among those are at pavilion\nbetween 9 to 12 at the 3 positions")
ebc <- edge.betweenness.community(graph) 
plot(ebc, graph)

#### communication location of these individual
load("com_pos_sun.RData")
ids = c("7478", "20098", "27471", "47327", "64304", "81090", "170456", 
        "174727", "174974", "195725", "211109", "211669", "221553", "230313", 
        "240876", "264321", "328657", "365108", "377275", "408230", "412874", 
        "427170", "542027", "576974", "583339", "584464", "590484", "603369", 
        "634880", "710974", "721817", "746574", "813863", "829943", "871333", 
        "873143", "896617", "910832", "1019339", "1070569", "1120259", 
        "1128580", "1147181", "1148774", "1174266", "1180332", "1196711", 
        "1217112", "1246261", "1264352", "1324567", "1392907", "1399218", 
        "1411980", "1412390", "1430595", "1445696", "1512355", "1559001", 
        "1580679", "1615382", "1623035", "1648751", "1669211", "1687201", 
        "1713404", "1721744", "1725194", "1770473", "1814974", "1882328", 
        "1884134", "1890620", "1906865", "1937540", "1970360", "2002193", 
        "2028377", "2039599", "2040751", "2063740", "2090883", "31755", 
        "536330", "1738952")

com_subset <- com_sun %>%
  filter(from %in% ids & to %in%ids) %>%
  mutate(hour_f = (hour(timestamp)*60 + minute(timestamp))/60) %>%
  mutate(hour = hour(timestamp))

### communication spectral map
load("map_pos.RData")
map = ggplot(all_pos)+ geom_rect(aes(xmin=X, ymin=Y, xmax=X+1, ymax=Y+1), fill="white") + coord_fixed(xlim = c(-2, 102), ylim = c(-2, 102))

map + geom_segment(data= com_subset,aes(x = from_x, y = from_y, xend=to_x, yend= to_y, color = hour_f) )+
  scale_color_gradientn(colours = brewer.pal(11, "Spectral"), space="rgb") 
map + geom_segment(data= com_subset,aes(x = from_x, y = from_y, xend=to_x, yend= to_y), color="black", alpha=0.2, size = 0.8)+
  facet_wrap(~hour)

### movement pattern
load("gps_sun.RData")
gps <- sun %>%
  filter(id %in% ids) %>%
  mutate(hour_f = (hour(timestamp)*60 + minute(timestamp))/60) 

ggplot(data = gps, aes(x=X, y=Y, color=hour_f)) +
  geom_path(position = position_jitter(w = 0.5, h=0.5)) +
  scale_color_gradientn(colours = brewer.pal(11, "Spectral"), space="rgb") +
  coord_fixed(xlim = c(0, 110), ylim = c(0, 110)) + 
  geom_point(data = gps %>% filter(type=="check-in"), aes(x=X, y=Y, color=hour_f), size= 3)+
  annotate("point", x=32, y=33, shape=1, size=4, color="gray20")+
  annotate("text", x=32, y=33, label="Pavilion", hjust=1, vjust=1,cex=3, color="gray20")+
  annotate("point", x=76, y=22, shape=1, size=4, color="gray20")+
  annotate("text", x=76, y=22, label="Stage", hjust=1, vjust=1,cex=3, color="gray20") +
  ggtitle("Cluster 2 on Sunday")+
  facet_wrap(~id)


#edit distance clustering
par(mar= c(2,2,2,2))
library(dendsort)
m <- get_string_dataframe(gps)
edit_distance <- as.dist(adist(m$path))
hc <- hclust(edit_distance)
plot(dendsort(as.dendrogram(hc), type = "average"))
heatmap(as.matrix(edit_distance))


#edit * seq length
edit_matrix <- adist(m$path)
edit_matrix <- edit_matrix / max(edit_matrix)

nchar(m$path[1])
seq_matrix <- as.matrix(dist(nchar(m$path)))
seq_matrix <- seq_matrix / max(seq_matrix)

mix <- edit_matrix * seq_matrix
rownames(mix) <- m$id
mix_dist <- as.dist(mix)
hc <- hclust(mix_dist)
plot(as.dendrogram(hc), nodePar = list(pch = NA, lab.cex = 0.6))

#get clusters
clusters <- as.data.frame(cutree(hc, h =0.2))
colnames(clusters) = c("membership")
clusters$id = rownames(clusters)
#assignclusters
gps <- gps %>%
  mutate(cluster_id = clusters$membership[match(id, clusters$id)] )

# clusters$membership[match(gps$id,clusters$id)]
cluster <- gps %>%
  filter(cluster_id == 2)
ggplot(data = cluster, aes(x=X, y=Y, color=hour_f)) +
  geom_path(position = position_jitter(w = 0.5, h=0.5)) +
  scale_color_gradientn(colours = brewer.pal(11, "Spectral"), space="rgb") +
  coord_fixed(xlim = c(0, 110), ylim = c(0, 110)) + 
  geom_point(data = cluster %>% filter(type=="check-in"), aes(x=X, y=Y, color=hour_f), size= 3)+
  annotate("point", x=32, y=33, shape=1, size=4, color="gray20")+
  annotate("text", x=32, y=33, label="Pavilion", hjust=1, vjust=1,cex=3, color="gray20")+
  annotate("point", x=76, y=22, shape=1, size=4, color="gray20")+
  annotate("text", x=76, y=22, label="Stage", hjust=1, vjust=1,cex=3, color="gray20") +
  ggtitle("Cluster  on Sunday")+
  facet_wrap(~ id)

# edit distance is crap

#### check in distance
sunday <- read.csv("Derived_Data/movement_summary_Sun.csv", quote="\"", header = FALSE, nrows = 100)
classes <- sapply(sunday, class)
sunday <- read.csv("Derived_Data//movement_summary_Sun.csv", quote="\"", header = FALSE, colClasses = classes)
sunday <- tbl_df(sunday)
colnames(sunday) <- c("id", "from_x", "from_y", "to_x", "to_y", "from_t", "to_t", "distance", "duration", "speed", "type")
#convert time
sunday <- sunday %>%
  transform(from_t = as.POSIXct(from_t, origin = "1970-01-01", tz = "UTC")) %>%
  transform(to_t = as.POSIXct(to_t, origin = "1970-01-01", tz = "UTC"))
#location data
locations <- read.csv("Derived_Data/park_locations.csv", quote="\"", header = TRUE)
locations <- locations %>%
  mutate(pos = paste(X, Y, sep="_"))

subset <- sunday %>%
  filter(id %in% ids) %>%
  filter(speed < 0.01) %>%
  rename(u_id = id) %>%
  mutate(pos = paste(from_x, from_y, sep="_")) %>%
  left_join(locations[, c("id", "name", "pos")], by="pos")

df <- subset %>%
  count(u_id,pos) %>%
  spread(pos, n) %>%
  replace(is.na(.), 0)
#make a matrix
m <- df[, -1]
rownames(m) <- df$u_id
dist <- dist(m)
hc <- hclust(dist)
dend <- as.dendrogram(hc)
plot(dend,nodePar = list(pch = NA, lab.cex = 0.6))
#get clusters
clusters <- as.data.frame(cutree(hc, h = 2))
colnames(clusters) = c("membership")
clusters$id = rownames(clusters)
#assign clusters
load("gps_sun.RData")
gps <- sun %>%
  filter(id %in% ids)
gps <- gps %>%
  mutate(cluster_id = clusters$membership[match(id, clusters$id)] ) %>%
  mutate(hour_f = (hour(timestamp)*60 + minute(timestamp))/60) %>%
  arrange(id)

ggplot(data = gps, aes(x=X, y=Y, color=hour_f)) +
  geom_path(position = position_jitter(w = 0.5, h=0.5), alpha = 0.4) +
  scale_color_gradientn(colours = brewer.pal(11, "Spectral"), space="rgb") +
  coord_fixed(xlim = c(0, 110), ylim = c(0, 110)) + 
  geom_point(data = gps %>% filter(type=="check-in"), aes(x=X, y=Y, color=hour_f), size= 3)+
  annotate("point", x=32, y=33, shape=1, size=4, color="gray20")+
  annotate("text", x=32, y=33, label="Pavilion", hjust=1, vjust=1,cex=3, color="gray20")+
  annotate("point", x=76, y=22, shape=1, size=4, color="gray20")+
  annotate("text", x=76, y=22, label="Stage", hjust=1, vjust=1,cex=3, color="gray20") +
  ggtitle("Clusters on Sunday")+
  facet_wrap(~ cluster_id)

unique_ids <-clusters %>%
  arrange(membership) %>%
  select(id) %>%
  t() %>%
  as.vector() %>%
  as.character()

#sequence view of these individuals
seq <- gps %>%
  group_by(id) %>%
  mutate(from_t = timestamp,
         to_t = lead(timestamp, default=timestamp[1]),
         pos = paste(X, Y, sep="_"),
         index = match(id, unique_ids),
         diff_t =as.integer(to_t - from_t, units="secs")) %>%
  arrange(id) %>%
  filter(diff_t >8) %>%
  mutate(diff_min = diff_t/60) %>%
  left_join(locations %>% select(pos, name, type, area), by ="pos") %>%
  mutate(isPavilion = ifelse(X==32 & Y==33, TRUE, FALSE))


ggplot(seq, aes(x=from_t,y=index)) + geom_segment(aes(xend=to_t, yend=index, color = area )) + guides(color = guide_legend(ncol=7)) +theme(legend.position="bottom", axis.title.x=element_blank())  
ggplot(seq, aes(x=from_t,y=index)) + geom_segment(aes(xend=to_t, yend=index, color = name )) + guides(color = guide_legend(ncol=7)) +theme(legend.position="bottom", axis.title.x=element_blank())  
ggplot(seq, aes(x=from_t,y=index)) + geom_segment(aes(xend=to_t, yend=index, color = type.y )) + guides(color = guide_legend(ncol=7)) +theme(legend.position="bottom", axis.title.x=element_blank())  
ggplot(seq, aes(x=from_t,y=index)) + geom_segment(aes(xend=to_t, yend=index, color = isPavilion )) + guides(color = guide_legend(ncol=7)) +theme(legend.position="bottom", axis.title.x=element_blank())  
ggplot(seq, aes(x=from_t,y=index)) + geom_segment(aes(xend=to_t, yend=index, color = type.x )) + guides(color = guide_legend(ncol=7)) +theme(legend.position="bottom", axis.title.x=element_blank())  


