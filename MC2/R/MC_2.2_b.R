library(dplyr)
library(ggplot2)
library(lubridate)
library(igraph)
library(grid)
library(scales)
library(RColorBrewer)
library(tidyr)

load("com_pos_sun.RData")

# Those who communicated to external from near pavilion betwen 11:45 adn 12:00
group1 <- com_sun %>%
  filter(to == "0") %>%
  filter(from_x>=31 & from_x<=32 & from_y>=33 & from_y<=34) %>%
  filter(timestamp >= as.POSIXct(strptime("2014-06-08 11:45:00", "%Y-%m-%d %H:%M:%S"), tz="UTC")) %>%
  filter(timestamp < as.POSIXct(strptime("2014-06-08 12:00:00", "%Y-%m-%d %H:%M:%S"), tz="UTC"))

g1_id <- group1 %>%
  group_by(from, to) %>%
  summarise(count = n()) %>%
  ungroup() %>%
  arrange(desc(count)) %>%
  select(from) %>%
  t() %>%
  as.vector() %>%
  as.character() 

# get communication network of group1
g1 <- com_sun %>%
  filter(to != "0") %>%
  filter(from %in% g1_id | to%in% g1_id) %>%
  filter(from != "1278894" & to != "1278894") %>%  
  filter(from != "839736" & to != "839736") %>% 
  select(from, to, timestamp)%>%
  filter(timestamp >= as.POSIXct(strptime("2014-06-08 11:45:00", "%Y-%m-%d %H:%M:%S"), tz="UTC")) %>%
  filter(timestamp < as.POSIXct(strptime("2014-06-08 12:00:00", "%Y-%m-%d %H:%M:%S"), tz="UTC"))

g1_summary <- g1 %>%
  group_by(from, to) %>%
  summarise(count = n()) %>%
  ungroup() %>%  
  filter(count > 1)
  
  top_n(1000, count)

  
  filter(count > 5)

graph <- graph.data.frame(g1_summary, directed = TRUE)
vcount(graph)
par(mar= c(0,0,3,0))
#style graph
V(graph)$color = "white"
V(graph)$color = ifelse(V(graph)$name %in% g1_id, "aquamarine", "white")
# V(graph)$color = ifelse(V(graph)$name %in% group2, "steelblue", V(graph)$color)
# V(graph)$color = ifelse(V(graph)$name %in% group3, "coral", V(graph)$color)
# V(graph)$color = ifelse(V(graph)$name %in% group4, "darkorchid", V(graph)$color)
V(graph)$size = 3

# V(graph)$label.color ="black"
# V(graph)$label.dist=0.4
# V(graph)$label.cex = 0.8
# V(graph)$label.family ="sans"
V(graph)$label = ""
E(graph)$shape = "circle"
E(graph)$arrow.size=0.2
E(graph)$color = "grey60"

graph$layout = layout.fruchterman.reingold(graph)
# graph$layout = layout.graphopt(graph)
# graph$layout = layout.kamada.kawai(graph)

###### plot
plot(graph, main="Communication Pattern on Group 1")

#decompose
decomp <- decompose.graph(graph)
sapply(decomp, vcount)
sub1 <- decomp[[1]]
clusters(sub1)
graph.density(sub1)

sub2 <- decomp[[5]]
vcount(sub2)
graph.density(sub2)


#plot subgraph
plot(sub1, layout = layout.fruchterman.reingold(sub1), vertex.label=V(sub1)$name, 
     vertex.label.color ="black", vertex.label.dist=0.4, vertex.label.family="sans", 
     vertex.label.cex = 0.8, main ="Subgraph 1" )

plot(sub2, layout = layout.fruchterman.reingold(sub2), vertex.label=V(sub1)$name, 
     vertex.label.color ="black", vertex.label.dist=0.2, vertex.label.family="sans", 
     vertex.label.cex = 0.6, main ="Subgraph 2" )


## Where are thy communicateing from?
#subgraph 1
sub1_id <- V(sub1)$name
sub2_id  <-V(sub2)$name

sub1 <- com_sun %>%
  filter(from %in% sub1_id | to%in% sub1_id) %>%
  filter(from != "1278894" & to != "1278894") %>%  
  filter(from != "839736" & to != "839736") %>% 
  filter(from != "0" & to != "0") %>% 
  filter(timestamp >= as.POSIXct(strptime("2014-06-08 08:00:00", "%Y-%m-%d %H:%M:%S"), tz="UTC")) %>%
  filter(timestamp < as.POSIXct(strptime("2014-06-08 12:00:00", "%Y-%m-%d %H:%M:%S"), tz="UTC")) %>%
  mutate(hour = hour(timestamp))

sub2 <- com_sun %>%
  filter(from %in% sub2_id & to%in% sub2_id) %>%
  filter(from != "1278894" & to != "1278894") %>%  
  filter(from != "839736" & to != "839736") %>% 
  filter(from != "0" & to != "0") %>% 
  filter(timestamp >= as.POSIXct(strptime("2014-06-08 08:00:00", "%Y-%m-%d %H:%M:%S"), tz="UTC")) %>%
  filter(timestamp < as.POSIXct(strptime("2014-06-08 12:00:00", "%Y-%m-%d %H:%M:%S"), tz="UTC")) %>%
  mutate(hour = hour(timestamp))

#plot on map
load("map_pos.RData")
map <- ggplot(all_pos)+ geom_rect(aes(xmin=X, ymin=Y, xmax=X+1, ymax=Y+1), fill="white") + coord_fixed(xlim = c(-2, 102), ylim = c(-2, 102))
map + geom_segment(data = sub1, aes(x=from_x, y=from_y, xend=to_x, yend=to_y, color=hour))+
  scale_color_gradientn(colours = brewer.pal(11, "Spectral"), space="rgb") 
map + geom_segment(data = sub2, aes(x=from_x, y=from_y, xend=to_x, yend=to_y),position = position_jitter(w = 0.5, h=0.5),color="black", alpha =0.05)+
   facet_wrap(~hour)

#for sub2 find high communication spot
sub2 %>%
  group_by(from_x, from_y) %>%
  summarise(count = n()) %>%
  ungroup() %>%
  arrange(desc(count))

sub2 %>%
  group_by(to_x, to_y) %>%
  summarise(count = n()) %>%
  ungroup() %>%
  arrange(desc(count))

sub2 %>%
  group_by(from_x, from_y, to_x, to_y) %>%
  summarise(count = n()) %>%
  ungroup() %>%
  arrange(desc(count))

sub1 %>%
  group_by(from_x, from_y, to_x, to_y) %>%
  summarise(count = n()) %>%
  ungroup() %>%
  arrange(desc(count))

########## summarise 
sub1 <- com_sun %>%
  filter(from %in% sub1_id | to%in% sub1_id) %>%
  filter(from != "1278894" & to != "1278894") %>%  
  filter(from != "839736" & to != "839736") %>% 
  filter(from != "0" & to != "0") %>% 
  filter(timestamp >= as.POSIXct(strptime("2014-06-08 08:00:00", "%Y-%m-%d %H:%M:%S"), tz="UTC")) %>%
  filter(timestamp < as.POSIXct(strptime("2014-06-08 24:00:00", "%Y-%m-%d %H:%M:%S"), tz="UTC")) %>%
  mutate(hour = hour(timestamp))

sub2 <- com_sun %>%
  filter(from %in% sub2_id | to%in% sub2_id) %>%
  filter(from != "1278894" & to != "1278894") %>%  
  filter(from != "839736" & to != "839736") %>% 
  filter(from != "0" & to != "0") %>% 
  filter(timestamp >= as.POSIXct(strptime("2014-06-08 08:00:00", "%Y-%m-%d %H:%M:%S"), tz="UTC")) %>%
  filter(timestamp < as.POSIXct(strptime("2014-06-08 24:00:00", "%Y-%m-%d %H:%M:%S"), tz="UTC")) %>%
  mutate(hour = hour(timestamp))

sub1_summary <- sub1 %>%
  group_by(from_x, from_y, to_x, to_y, hour) %>%
  summarise(count = n()) %>%
  top_n(10, count) %>%
  ungroup() %>%
  arrange(desc(count)) %>%
  arrange(hour)
#plot summary
map + geom_segment(data = sub1_summary, aes(x=from_x, y=from_y, xend=to_x, yend=to_y, size=count),lineend="round",color="black", alpha =0.5)+
  facet_wrap(~hour) + theme(legend.position =c(0.9,0.1)) + xlab("")+ylab("") +ggtitle("Communication Pattern of Subgraph1")


sub2_summary <- sub2 %>%
  group_by(from_x, from_y, to_x, to_y, hour) %>%
  summarise(count = n()) %>%
  ungroup() %>%
  group_by(hour) %>%
  top_n(10, count)

#plot summary
map + geom_segment(data = sub2_summary, aes(x=from_x, y=from_y, xend=to_x, yend=to_y, size=count),lineend="round",color="black", alpha =0.5)+
  facet_wrap(~hour) + theme(legend.position =c(0.9,0.1)) + xlab("")+ylab("") +ggtitle("Communication Pattern of Subgraph2")

map + geom_segment(data = sub2_summary, aes(x=from_x, y=from_y, xend=to_x, yend=to_y, size=count),lineend="round",color="black", alpha =0.5, arrow = arrow(length = unit(0.1, "cm"), type="closed"))+
  facet_wrap(~hour) + theme(legend.position =c(0.9,0.1)) + xlab("")+ylab("") +ggtitle("Communication Pattern of Subgraph2")

?arrow()

############################## general patterns
#degree distribution
load("com_fri.RData")
com_fri <- friday %>%
  filter(from != "1278894" & to != "1278894") %>%  
  filter(from != "839736" & to != "839736") %>% 
  filter(from != "0" & to != "0") %>%
  select(from, to)
g = graph.data.frame(com_fri, directed = TRUE)

in_count <- com_fri %>%
  group_by(from) %>%
  summarise(count = n())
ggplot(in_count, aes(x=count))+geom_histogram(binwidth=100)

#community detection
com_fri <- friday %>%
  filter(from != "1278894" & to != "1278894") %>%  
  filter(from != "839736" & to != "839736") %>% 
  filter(from != "0" & to != "0") %>%
  group_by(from, to) %>%
  summarise(count = n()) %>%
  ungroup() %>%
  top_n(1000, count)

g = graph.data.frame(com_fri, directed = TRUE)
vcount(g)
ebc <- edge.betweenness.community(g) 
par(mar= c(0,0,3,0))
plot(ebc, g, vertex.size = 3, edge.arrow.size = 0.2, vertex.label=NA, main=("Friday, top 1000 communication count"))

HS <- hub.score(g)$vector
AS <- authority.score(g)$vector
#scatterplot
scores = data.frame(V(g)$name, AS, HS)
colnames(scores) = c("id", "AS", "HS")
scores$id = as.character(scores$id)
#plot
ggplot(scores, aes(x = AS, y = HS)) +geom_point(shape = 1) +
  geom_text(data=scores %>% filter( HS>0.25), aes(x = AS, y = HS, label = id),  color= "red", hjust=0, vjust=0)

ids <- scores %>%
  filter(HS >0.25) %>%
  select(id) %>%
  t() %>%
  as.vector() %>%
  as.character()
ids
#      vertex.label= ifelse(V(g)$name %in% ids, V(g)$name, ""), 
#      vertex.size= ifelse(V(g)$name %in% ids, 5, 3), 
plot(ebc, g,
     edge.color = "grey30",
     edge.arrow.size = 0.2, 
     vertex.label = NA,
     vertex.size= 2,
     vertex.color = "white",
     vertex.label.color ="black",
     vertex.label.dist = 0.4,
     vertex.label.family = "sans",
     main=("Communities based on Communication on Friday"))

ebc$algorithm

#saturday
load("com_sat.RData")
com_sat <- saturday %>%
  filter(from != "1278894" & to != "1278894") %>%  
  filter(from != "839736" & to != "839736") %>% 
  filter(from != "0" & to != "0") %>%
  group_by(from, to) %>%
  summarise(count = n()) %>%
  ungroup() %>%
  top_n(1000, count)

g2 = graph.data.frame(com_sat, directed = TRUE)
vcount(g2)
ebc2 <- edge.betweenness.community(g2) 
par(mar= c(0,0,3,0))
plot(ebc2, g2,
     edge.color = "grey30",
     edge.arrow.size = 0.2, 
     vertex.label = NA,
     vertex.size= 2,
     vertex.color = "white",
     vertex.label.color ="black",
     vertex.label.dist = 0.4,
     vertex.label.family = "sans",
     main=("Communities based on Communication on Saturday"))


#sunday
load("com_sun.RData")
com_sun <- saturday %>%
  filter(from != "1278894" & to != "1278894") %>%  
  filter(from != "839736" & to != "839736") %>% 
  filter(from != "0" & to != "0") %>%
  group_by(from, to) %>%
  summarise(count = n()) %>%
  ungroup() %>%
  top_n(1000, count)

g3 = graph.data.frame(com_sun, directed = TRUE)
vcount(g3)
ebc3 <- edge.betweenness.community(g3) 
par(mar= c(0,0,3,0))
plot(ebc3, g3,
     edge.color = "grey30",
     edge.arrow.size = 0.2, 
     vertex.label = NA,
     vertex.size= 2,
     vertex.color = "white",
     vertex.label.color ="black",
     vertex.label.dist = 0.4,
     vertex.label.family = "sans",
     main=("Communities based on Communication on Sunday"))


##### heatmaps
load("com_pos_fri.RData")
from_fri <- com_fri %>%
  filter(from != "1278894" & to != "1278894") %>%  
  filter(from != "839736" & to != "839736") %>% 
  filter(from != "0" & to != "0") %>%
  filter(!is.na(from_x)) %>%
  mutate(hour = hour(timestamp)) %>%
  group_by(from_x, from_y, hour) %>%
  summarise(count = n())
  
load("map_pos.RData")
# map <- ggplot(all_pos)+ geom_rect(aes(xmin=X, ymin=Y, xmax=X+1, ymax=Y+1), fill="white") + coord_fixed(xlim = c(-2, 102), ylim = c(-2, 102))
map <- ggplot(all_pos)+ geom_rect(aes(xmin=X, ymin=Y, xmax=X+1, ymax=Y+1), fill="grey90") + coord_fixed(xlim = c(-2, 102), ylim = c(-2, 102)) + theme_bw() 

# map + geom_rect(data = from_fri, aes(xmin = from_x, ymin = from_y, xmax = from_x+1, ymax = from_y+1, fill = count))+
#   scale_fill_gradient(low ="#fff5f0", high="red")+ggtitle("Communication counts by origin on Friday")+ facet_wrap(~hour, nrow = 3)+ theme(legend.position =c(0.9,0.1))

#bubble
# map + geom_point(data = from_fri, aes(x=from_x, y=from_y, size = count, color=count), shape=1)+ scale_size_area() + 
#   scale_color_gradient(low ="#fff5f0", high="red")+
#   facet_wrap(~hour, nrow = 3)+ theme(legend.position =c(0.9,0.1))
# 
# map + geom_point(data = from_fri, aes(x=from_x, y=from_y, size = count, alpha=count), shape=1)+ scale_size_area() + 
#   facet_wrap(~hour, nrow = 3)+ theme(legend.position =c(0.9,0.1))

map + geom_point(data = from_fri, aes(x=from_x, y=from_y, size = count, alpha=count), shape=1, color="red")+ scale_size_area() + 
  facet_wrap(~hour, nrow = 3)+ theme(legend.position =c(0.9,0.1))


#label
locations <- read.csv("Derived_Data/park_locations.csv", quote="\"", header = TRUE)
locations <- locations %>%
  mutate(pos = paste(X, Y, sep="_"))

annotation <- from_fri %>%
  filter(count > 1500) %>%
  mutate(pos = paste(from_x, from_y, sep="_")) %>%
  left_join(locations, by="pos")   
#   mutate(name = ifelse((count >1000 & pos%in%locations$pos), (locations$name[match(pos, locations$pos)]), ""))
map + geom_point(data = from_fri, aes(x=from_x, y=from_y, size = count, alpha=count), shape=1, color="red")+ scale_size_area() + 
  geom_text(data = annotation, aes(x=from_x, y=from_y, label = name), size= 2.5, lineheight=0.8)+
  facet_wrap(~hour, nrow = 3)+ theme(legend.position =c(0.9,0.1)) +xlab("")+ylab("")


### to
to_fri <- com_fri %>%
  filter(from != "1278894" & to != "1278894") %>%  
  filter(from != "839736" & to != "839736") %>% 
  filter(from != "0" & to != "0") %>%
  filter(!is.na(to_x)) %>%
  mutate(hour = hour(timestamp)) %>%
  group_by(to_x, to_y, hour) %>%
  summarise(count = n())
annotation <- to_fri %>%
  filter(count > 1500) %>%
  mutate(pos = paste(to_x, to_y, sep="_")) %>%
  left_join(locations, by="pos")   
#   mutate(name = ifelse((count >1000 & pos%in%locations$pos), (locations$name[match(pos, locations$pos)]), ""))
map + geom_point(data = to_fri, aes(x=to_x, y=to_y, size = count, alpha=count), shape=1, color="blue")+ scale_size_area() + 
  geom_text(data = annotation, aes(x=to_x, y=to_y, label = name), size= 2.5, lineheight=0.8)+
  facet_wrap(~hour, nrow = 3)+ theme(legend.position =c(0.9,0.1)) +xlab("")+ylab("")

############ by arrow most 

