# VAST Challenge 2015 #

This repository contains the following items prepared for the VAST Challenge 2015:

* Submission Entries: MiniChallenge 1 (MC1), MiniChallenge 2 (MC2) and Grand Challenge (GC)
* The source R code
* The R markdown for describing the data processing and visualization process. 
* Processing Sketches
* Images generated in R
* Reviewer's comment

The intention behind this repository is to promote reproducibility in research and to share the analysis process with those who are interested in participating the VAST challenge in the future or learning R. We performed most of the analysis in R and source codes are shared here.  We also included reviewer's comments because we found it very useful to review the strengths and weaknesses of our submissions, and to strategise analysis approach for the future VAST challenges.  If you have any comments or questions, please feel free to contact us. 

### Entry Overview: Videos ###
[![VAST Mini Challenge 1](https://dl.dropboxusercontent.com/u/50368075/VAST_git_images/mc1.png)](https://vimeo.com/142686647 "VAST Mini Challenge 1 - Click to Watch!")

[![VAST Mini Challenge 2](https://dl.dropboxusercontent.com/u/50368075/VAST_git_images/mc2.png)](https://vimeo.com/142687087 "VAST Mini Challenge 2 - Click to Watch!")

[![VAST Grand Challenge](https://dl.dropboxusercontent.com/u/50368075/VAST_git_images/gc.png)](https://vimeo.com/142688070 "VAST Grand Challenge - Click to Watch!")

### Presentation Slides ###
[IEEE VIS VAST - Oct 25, 2015](https://dl.dropboxusercontent.com/u/50368075/VAST_git_images/VAST_Presentation_Slides_RSAKAI.pdf)


### Entries ###
* Mini Challenge 1 [\[Entry\]](https://dl.dropboxusercontent.com/u/50368075/VAST_git_images/mc1/index.htm) [\[Reviews\]](https://dl.dropboxusercontent.com/u/50368075/VAST_git_images/mc1/Review_MC1) [[Rmarkdown]](https://dl.dropboxusercontent.com/u/50368075/VAST_git_images/MC_1_Markdown.html)
* Mini Challenge 2 [\[Entry\]](https://dl.dropboxusercontent.com/u/50368075/VAST_git_images/mc2/index.htm) [\[Reviews\]](https://dl.dropboxusercontent.com/u/50368075/VAST_git_images/mc2/Review_MC2) \[Rmarkdown coming soon\]
* Grand Challenge [\[Entry\]](https://dl.dropboxusercontent.com/u/50368075/VAST_git_images/gc/index.htm) [\[Reviews\]](https://dl.dropboxusercontent.com/u/50368075/VAST_git_images/gc/Review_GC) \[Rmarkdown coming soon\]

** Results/Scores **

|     | Reviewer_1 | Reviewer_2 | Reviewer_3 | Reviewer_4 |
|:---:|:----------:|:----------:|:----------:|:----------:|
| MC1 |      5     |      4     |      4     |      3     |
| MC2 |      4     |      4     |      4     |            |
| GC  |      2     |      3     |      4     |      4     |


### How do I get set up? ###
* Install [R](https://www.r-project.org) and [RStudio](https://www.rstudio.com/products/RStudio/)
* Install [Processing 2.2.1.](https://processing.org)  The codes are not adapted for Processing 3.0 yet. 

### Useful Links ###
* [dplyr cheat sheet](https://www.rstudio.com/wp-content/uploads/2015/02/data-wrangling-cheatsheet.pdf)
* [ggplot2 chat sheet](https://www.rstudio.com/wp-content/uploads/2015/03/ggplot2-cheatsheet.pdf)
* [Introduction to dplyr](https://rpubs.com/justmarkham/dplyr-tutorial)

### Contacts ###

* ryo.sakai@esat.kuleuven.be
* [Visual Data Analysis lab](http://vda-lab.be), KU Leuven, Belgium

